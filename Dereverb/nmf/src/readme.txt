The Gam_Mag_NMF.m is the top MATLAB file for NMF processing.

Please refer to the following paper for algorithm details:
% GAMMATONE SUB-BAND MAGNITUDE-DOMAIN DEREVERBERATION FOR ASR

@inproceedings{NMF10,
title = "Gammatone Sub-Band Magnitude-Domain Dereverberation For ASR",
author =  "K. Kumar and Rita Singh and Bhiksha Raj,  R. M. Stern",
booktitle= "in Proc. IEEE ICASSP",
year = "2011 (submitted)"
}

Abstract - We present an algorithm for dereverberation of speech signals for
automatic speech recognition (ASR) applications. Often ASR systems
are presented with speech that has been recorded in environments
that include noise and reverberation. The performance of
ASR systems degrades with increasing levels of noise and reverberation.
While many algorithms have been proposed for robust ASR in
noisy environments, reverberation is still a challenging problem. In
this paper, we present an approach for dereverberation that models
reverberation as a convolution operation in the speech spectral domain.
Using a least-squares error criterion we decompose reverberated
spectra into clean spectra convolved with a filter. We incorporate
non-negativity and sparsity of the speech spectra as constraints
within an NMF framework to achieve the decomposition. In ASR
experiments where the system is trained with unreverberated and reverberated
speech, we show that the proposed approach can provide
upto 40% and 19% relative reduction respectively in performance


Implementation Details -

The file Gam_Mag_NMF.m takes 2 inputs, an input wavfile (MS-WAV format) to read speech data and an output file to write
the NMF processed speech data. The file lists a number of parameters for speech analysis and synthesis. A Gammatone transformation
matrix is first evaluated in the "gH" parameter. The transformation matrix is applied on Fourier magnitude spectra to obtain Gammatone
magnitude spectra. Currently gH is evaluated at every run of the algorithm - it should ideally be saved and recalled when needed
(similarly "igH" as well).

derev_NMF.m is the key file that applies the NMF processing algorithm.

Feature Extraction - 
The NMF algorithm currently processes an input speech file and outputs a reconstructed (dereverberated) speech. 
For ASR applications conventional MFCC/PLP features can be obtained from the dereverberated speech.
In our experiments we used SPHINX (wave2feat executable) for features extraction
http://cmusphinx.sourceforge.net/html/cmusphinx.php

Test - 
The approach can be tested by running the command
Gam_Mag_NMF('input.wav', 'output.wav'); 
and comparing the data in 'output.wav' file with it's corresponding data in 'output_ref.wav'.