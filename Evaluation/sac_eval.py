# -*- coding: utf-8 -*-

from pylab import *
import glob
import evaluation
import operator

mir_path = '../MIR-1K/Wavfile'
mir_reverb_path = '../MIR-1K reverb'
mir_reverb_folders = glob.glob('%s/air_*' % mir_reverb_path)
mir_dereverb_path = '../MIR-1K dereverb'
mir_dereverb_algorithms = glob.glob('%s/*' % mir_dereverb_path)
mir_dereverb_folders =  [glob.glob('%s/air_*' % f) for f in mir_dereverb_algorithms]

eval_folders = [] # folders to evaluate
#eval_folders += [mir_path]
#eval_folders += mir_reverb_folders
eval_folders += reduce(operator.add, mir_dereverb_folders) # flatten list

for folder in eval_folders:
  evaluation.evaluate_folder(folder, 'sac')
  evaluation.folder_results(folder, 'sac')
  evaluation.folder_results_micro(folder, 'sac')
  
evaluation.merge_folder_results(mir_reverb_folders, 'sac', mir_reverb_path) 
evaluation.merge_folder_results_micro(mir_reverb_folders, 'sac', mir_reverb_path) 
for i in range(0,len(mir_dereverb_algorithms)):
  evaluation.merge_folder_results(mir_dereverb_folders[i], 'sac', mir_dereverb_algorithms[i]) 
  evaluation.merge_folder_results_micro(mir_dereverb_folders[i], 'sac', mir_dereverb_algorithms[i]) 
    
print "Finished!!!!!!!!"