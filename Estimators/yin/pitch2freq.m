function freq = pitch2freq(pitch)
freq = 440*2.^((pitch-69)/12);