# -*- coding: utf-8 -*-

from pylab import *
import imp, glob, csv
common = imp.load_source('common', '../common/common.py')

mir_folder = '../MIR-1K/Wavfile'
mir_reverb_folders = glob.glob('../MIR-1K reverb/air_*')
mir_dereverb_folder = '../MIR-1K dereverb'

algorithms = ['yin', 'twm', 'mel', 'sac']
results_file = {'yin': 'results.yin.json', 'twm': 'results.twm.json', 'mel': 'results.mel.json', 'sac': 'results_micro.sac.json'}
dereverbs = ['itd_micro', 'nmf', 'nml_micro']
attrs = ['Overall Accuracy', 'Raw Pitch Accuracy', 'Voicing False Alarm']

def read_csv(csv_path, delimiter=' '):
  data = []
  with open(csv_path, 'rb') as f:
    try:
      reader = csv.reader(f, delimiter=delimiter)
      for row in reader:
        data.append(row)
    finally:
      f.close()
  return array(data)

def plot2():
  rooms = read_csv('../AIR_1_4/rt60.csv')
  
  for attr in attrs:
    data = zeros((rooms.shape[0]+1, rooms.shape[1]+len(algorithms)), dtype=rooms.dtype)
    data[0,0:3] = ['dry', 0, 0]
    col = rooms.shape[1]
    
    for algorithm in algorithms:
      results = common.read_json('%s/%s' % (mir_folder, results_file[algorithm]))
      data[0, col] = results['excerpt'][attr]

      for folder in mir_reverb_folders:      
        room = common.get_filename(folder)
        room_idx = where(rooms[:,0]==room)[0][0]
        results = common.read_json('%s/%s' % (folder, results_file[algorithm]))
        data[room_idx+1, col] = results['excerpt'][attr]
        data[room_idx+1, 0:rooms.shape[1]] = rooms[room_idx, :]
      
      col += 1
    savetxt('plot2 %s.csv' % attr, data, fmt='%s')   
    
def plot3():
  rooms = read_csv('../AIR_1_4/rt60.csv')
  
  for dereverb in dereverbs:
    for attr in attrs:
      data = zeros((rooms.shape[0], rooms.shape[1]+len(algorithms)), dtype=rooms.dtype)
      col = rooms.shape[1]
      
      for algorithm in algorithms:
        folders = glob.glob('%s/%s/air_*' % (mir_dereverb_folder, dereverb))
        for folder in folders:      
          room = common.get_filename(folder)
          room_idx = where(rooms[:,0]==room)[0][0]
          results = common.read_json('%s/%s' % (folder, results_file[algorithm]))
          data[room_idx, col] = results['excerpt'][attr]
          data[room_idx, 0:rooms.shape[1]] = rooms[room_idx, :]
        
        col += 1
      savetxt('plot3 %s %s.csv' % (dereverb, attr), data, fmt='%s')      
  
plot3()