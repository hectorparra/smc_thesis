This folder contains 15 utterances and corresponding semi-manually labeled ground-truth pitch contours used in:

Jin Z. and Wang D.L. (2011): �HMM-based multipitch tracking for noisy and reverberant speech,� IEEE Transactions on Audio, Speech, and Language Processing, vol. 19, pp. 1091-1102.


"sig" folder contains 10 target utterances + 5 interference utterances drawn from the TIMIT corpus. The sampling frequency is 16000 Hz. The files use following naming format:

	r_._._.val


- The first slot is the sentence id (v3, v5, v7, v8, v9, v13, v14, v15, v16, v18, n8, n16, n20, n21, n22), where "v" means target and "n" interference. 

- The second slot is the reverberation time (0,3,6), corresponding anechoic, t60=0.3s and t60=0.6s, respectively.

- The third slot is the configuration index (0,1,2,3), corresponding anechoic and three configurations (see the paper for details).


"pitch" folder contains the corresponding labeled ground-truth pitch periods for each utterance. They use the same naming format as the utterances.

