package casa;

public class Gammatone {

  private double erbBWCorrection;
  private double[] f, af, bf, tf;
  public int sampFreq;
  private int lowerCF, upperCF, dB;
  private double lowerERB, upperERB, spaceERB;
  private GChannel[] gchan;
  private int numberOfChannel;
  public double[] cFreq;

  private class GChannel {

    public double cf, midEarCoeff;
    private double bw, cr, z, gain, expCoeff;
    private double[] p, q, u, v; //p[5],q[5],u[2],v[2]
    private double cc, qq, ww;
    private InnerHairCells ihc;
    //private float c,q,w;

    GChannel(int chan) {
      InitChannel(chan);
    }

    private void InitChannel(int chan) {
      cr = lowerERB + chan * spaceERB;
      cf = ERBRateToHz(cr);
      midEarCoeff = DBToAmp(Loudness(dB, cf) - dB);
      bw = Erb(cf) * erbBWCorrection;
      z = Math.exp( -2 * Math.PI / sampFreq * bw);
      expCoeff = (cf * 2 * Math.PI / sampFreq);
      gain = midEarCoeff * Math.pow(2 * Math.PI / sampFreq * bw, 4) / 3;

      p = new double[5];
      q = new double[5];
      u = new double[2];
      v = new double[2];

      ihc = new InnerHairCells();
      double kt = ihc.MED_G * ihc.MED_A / (ihc.MED_A + ihc.MED_B);
      cc = ihc.MED_M * ihc.MED_Y * kt /
          (ihc.MED_L * kt + ihc.MED_Y * (ihc.MED_L + ihc.MED_R));
      qq = cc * (ihc.MED_L + ihc.MED_R) / kt;
      ww = cc * ihc.MED_R / ihc.MED_X;
    }
  }

  private class InnerHairCells{

    private double MED_Y, MED_G, MED_L, MED_R, MED_X, MED_A, MED_B, MED_H, MED_M;
    private double ymdt,xdt,ydt,lplusrdt,rdt,gdt,hdt;

    InnerHairCells(){
      MED_Y = 5.05;
      MED_G = 2000.0;
      MED_L = 2500.0;
      MED_R = 6580.0;
      MED_X = 66.31;
      MED_A = 3.0;
      MED_B = 300.0;
      MED_H = 48000.0;
      MED_M = 1.0;

      ymdt = MED_Y * MED_M / sampFreq;
      xdt = MED_X / sampFreq;
      ydt = MED_Y / sampFreq;
      lplusrdt = (MED_L + MED_R) / sampFreq;
      rdt = MED_R / sampFreq;
      gdt = MED_G / sampFreq;
      hdt = MED_H;
    }
  }

  public Gammatone() {
    this(16000, 128);
  }

  public Gammatone(int sF) {
    this(sF, 128);
  }

  public Gammatone(int sF, int numChannel) {
    erbBWCorrection = 1.019;
    dB = 60;
    lowerCF = 80;
    upperCF = 5000;
    numberOfChannel=numChannel;
    gchan = new GChannel[numChannel];
    cFreq = new double[numChannel];

    lowerERB = HzToERBRate(lowerCF);
    upperERB = HzToERBRate(upperCF);
    spaceERB = (upperERB - lowerERB) / (numChannel - 1);
    sampFreq = sF;

    f = new double[29];
    af = new double[29];
    bf = new double[29];
    tf = new double[29];
    f[0]=20.0;     af[0]=2.347;  bf[0]=0.00561;   tf[0]=74.3;
    f[1]=25.0;     af[1]=2.190;  bf[1]=0.00527;   tf[1]=65.0;
    f[2]=31.5;     af[2]=2.050;  bf[2]=0.00481;   tf[2]=56.3;
    f[3]=40.0;     af[3]=1.879;  bf[3]=0.00404;   tf[3]=48.4;
    f[4]=50.0;     af[4]=1.724;  bf[4]=0.00383;   tf[4]=41.7;
    f[5]=63.0;     af[5]=1.579;  bf[5]=0.00286;   tf[5]=35.5;
    f[6]=80.0;     af[6]=1.512;  bf[6]=0.00259;   tf[6]=29.8;
    f[7]=100.0;    af[7]=1.466;  bf[7]=0.00257;   tf[7]=25.1;
    f[8]=125.0;    af[8]=1.426;  bf[8]=0.00256;   tf[8]=20.7;
    f[9]=160.0;    af[9]=1.394;  bf[9]=0.00255;   tf[9]=16.8;
    f[10]=200.0;   af[10]=1.372; bf[10]=0.00254;  tf[10]=13.8;
    f[11]=250.0;   af[11]=1.344; bf[11]=0.00248;  tf[11]=11.2;
    f[12]=315.0;   af[12]=1.304; bf[12]=0.00229;  tf[12]=8.9;
    f[13]=400.0;   af[13]=1.256; bf[13]=0.00201;  tf[13]=7.2;
    f[14]=500.0;   af[14]=1.203; bf[14]=0.00162;  tf[14]=6.0;
    f[15]=630.0;   af[15]=1.135; bf[15]=0.00111;  tf[15]=5.0;
    f[16]=800.0;   af[16]=1.062; bf[16]=0.00052;  tf[16]=4.4;
    f[17]=1000.0;  af[17]=1.000; bf[17]=0.00000;  tf[17]=4.2;
    f[18]=1250.0;  af[18]=0.967; bf[18]=-0.00039; tf[18]=3.7;
    f[19]=1600.0;  af[19]=0.943; bf[19]=-0.00067; tf[19]=2.6;
    f[20]=2000.0;  af[20]=0.932; bf[20]=-0.00092; tf[20]=1.0;
    f[21]=2500.0;  af[21]=0.933; bf[21]=-0.00105; tf[21]=-1.2;
    f[22]=3150.0;  af[22]=0.937; bf[22]=-0.00104; tf[22]=-3.6;
    f[23]=4000.0;  af[23]=0.952; bf[23]=-0.00088; tf[23]=-3.9;
    f[24]=5000.0;  af[24]=0.974; bf[24]=-0.00055; tf[24]=-1.1;
    f[25]=6300.0;  af[25]=1.027; bf[25]=0.00000;  tf[25]=6.6;
    f[26]=8000.0;  af[26]=1.135; bf[26]=0.00089;  tf[26]=15.3;
    f[27]=10000.0; af[27]=1.266; bf[27]=0.00211;  tf[27]=16.4;
    f[28]=12500.0; af[28]=1.501; bf[28]=0.00488;  tf[28]=11.6;

    for (int chan = 0; chan < numChannel; chan++){
      gchan[chan] = new GChannel(chan);
      cFreq[chan] = gchan[chan].cf;
    }
  }

  private double Erb(double f) {
    return (24.7 * (4.37e-3 * f + 1.0));
  }

  private double HzToERBRate(double f) {
    return (21.4 * Math.log(4.37e-3 * f + 1.0) / Math.log(10));
  }

  private double ERBRateToHz(double f) {
    return ((Math.pow(10.0, (f / 21.4)) - 1.0) / 4.37e-3);
  }

  private double DBToAmp(double dB) {
    return Math.pow(10, (dB / 20));
  }

  private double Loudness(double dB, double freq) {
    int i = 0;
    double afy, bfy, tfy;

    if ( (freq < 20.0) | (freq > 12500.0)) {
      System.out.println(
          "Can't compute a outer/middle ear gain for that frequency");
      System.exit(0);
    }

    while (f[i] < freq)
      i++;
    afy = af[i - 1] + (freq - f[i - 1]) * (af[i] - af[i - 1]) / (f[i] - f[i - 1]);
    bfy = bf[i - 1] + (freq - f[i - 1]) * (bf[i] - bf[i - 1]) / (f[i] - f[i - 1]);
    tfy = tf[i - 1] + (freq - f[i - 1]) * (tf[i] - tf[i - 1]) / (f[i] - f[i - 1]);
    return (4.2 + afy * (dB - tfy) / (1.0 + bfy * (dB - tfy)));
  }

  private float UpdateFilter(int chan, float in, int t) {
    double zz = gchan[chan].z;
    double bm;
    gchan[chan].p[0] = in * Math.cos(gchan[chan].expCoeff * t) +
        zz *
        (4 * gchan[chan].p[1] -
         zz *
         (6 * gchan[chan].p[2] -
          zz * (4 * gchan[chan].p[3] - zz * gchan[chan].p[4])));
    gchan[chan].q[0] = -in * Math.sin(gchan[chan].expCoeff * t) +
        zz *
        (4 * gchan[chan].q[1] -
         zz *
         (6 * gchan[chan].q[2] -
          zz * (4 * gchan[chan].q[3] - zz * gchan[chan].q[4])));
    gchan[chan].u[0] = zz *
        (gchan[chan].p[1] + zz * (4 * gchan[chan].p[2] + zz * gchan[chan].p[3]));
    gchan[chan].v[0] = zz *
        (gchan[chan].q[1] + zz * (4 * gchan[chan].q[2] + zz * gchan[chan].q[3]));

    gchan[chan].p[4] = gchan[chan].p[3];
    gchan[chan].p[3] = gchan[chan].p[2];
    gchan[chan].p[2] = gchan[chan].p[1];
    gchan[chan].p[1] = gchan[chan].p[0];

    gchan[chan].q[4] = gchan[chan].q[3];
    gchan[chan].q[3] = gchan[chan].q[2];
    gchan[chan].q[2] = gchan[chan].q[1];
    gchan[chan].q[1] = gchan[chan].q[0];

    gchan[chan].u[1] = gchan[chan].u[0];
    gchan[chan].v[1] = gchan[chan].v[0];

    bm = (gchan[chan].u[0] * Math.cos(gchan[chan].expCoeff * t) -
          gchan[chan].v[0] * Math.sin(gchan[chan].expCoeff * t)) *
        gchan[chan].gain;

    return (float)bm;
  }

  private float UpdateFilterWithInnerCells(int chan, float in, int t) {
    double zz = gchan[chan].z;
    double bm, hc;
    double replenish, eject, reuptakeandloss, reuptake, reprocess, kt;

    gchan[chan].p[0] = in * Math.cos(gchan[chan].expCoeff * t) +
        zz *
        (4 * gchan[chan].p[1] -
         zz *
         (6 * gchan[chan].p[2] -
          zz * (4 * gchan[chan].p[3] - zz * gchan[chan].p[4])));
    gchan[chan].q[0] = -in * Math.sin(gchan[chan].expCoeff * t) +
        zz *
        (4 * gchan[chan].q[1] -
         zz *
         (6 * gchan[chan].q[2] -
          zz * (4 * gchan[chan].q[3] - zz * gchan[chan].q[4])));
    gchan[chan].u[0] = zz *
        (gchan[chan].p[1] + zz * (4 * gchan[chan].p[2] + zz * gchan[chan].p[3]));
    gchan[chan].v[0] = zz *
        (gchan[chan].q[1] + zz * (4 * gchan[chan].q[2] + zz * gchan[chan].q[3]));

    bm = (gchan[chan].u[0] * Math.cos(gchan[chan].expCoeff * t) -
          gchan[chan].v[0] * Math.sin(gchan[chan].expCoeff * t)) *
        gchan[chan].gain;

    if ( (bm + gchan[chan].ihc.MED_A) > 0.0)
      kt = gchan[chan].ihc.gdt * (bm + gchan[chan].ihc.MED_A) /
          (bm + gchan[chan].ihc.MED_A + gchan[chan].ihc.MED_B);
    else
      kt = 0.0;
    if (gchan[chan].qq < gchan[chan].ihc.MED_M)
            replenish=gchan[chan].ihc.ymdt-gchan[chan].ihc.ydt*gchan[chan].qq;
    else
            replenish=0.0;
    eject=kt*gchan[chan].qq;
    reuptakeandloss=gchan[chan].ihc.lplusrdt*gchan[chan].cc;
    reuptake=gchan[chan].ihc.rdt*gchan[chan].cc;
    reprocess=gchan[chan].ihc.xdt*gchan[chan].ww;
    gchan[chan].qq+=replenish-eject+reprocess;
    if (gchan[chan].qq<0.0)
            gchan[chan].qq=0.0;
    gchan[chan].cc+=eject-reuptakeandloss;
    if (gchan[chan].cc<0.0)
            gchan[chan].cc=0.0;
    gchan[chan].ww+=reuptake-reprocess;
    if (gchan[chan].ww<0.0)
            gchan[chan].ww=0.0;
    hc = gchan[chan].ihc.hdt*gchan[chan].cc;

    gchan[chan].p[4] = gchan[chan].p[3];
    gchan[chan].p[3] = gchan[chan].p[2];
    gchan[chan].p[2] = gchan[chan].p[1];
    gchan[chan].p[1] = gchan[chan].p[0];

    gchan[chan].q[4] = gchan[chan].q[3];
    gchan[chan].q[3] = gchan[chan].q[2];
    gchan[chan].q[2] = gchan[chan].q[1];
    gchan[chan].q[1] = gchan[chan].q[0];

    gchan[chan].u[1] = gchan[chan].u[0];
    gchan[chan].v[1] = gchan[chan].v[0];

    return (float) hc;
  }

  public float[][] Filter(int[] sig) {
    float[][] response = new float[numberOfChannel][sig.length];
    System.out.print("Gammatone filtering...");
    for (int chan = 0; chan < numberOfChannel; chan++) {
      System.out.print(Math.round((float)chan/numberOfChannel*100)+"%");
      for (int t = 0; t < sig.length; t++)
        response[chan][t] = UpdateFilter(chan, sig[t], t);
      if (Math.round( (float) chan / numberOfChannel * 100) < 10)
        System.out.print("\b\b");
      else
        System.out.print("\b\b\b");
    }
    System.out.println("Done");
    return response;
  }

  public float[][] FilterWithInnerCells(int[] sig) {
    float[][] response = new float[numberOfChannel][sig.length];
    System.out.print("Gammatone filtering...");
    for (int chan = 0; chan < numberOfChannel; chan++) {
      System.out.print(Math.round( (float) chan / numberOfChannel * 100) + "%");
      for (int t = 0; t < sig.length; t++)
        response[chan][t] = UpdateFilterWithInnerCells(chan, sig[t], t);
      if (Math.round( (float) chan / numberOfChannel * 100) < 10)
        System.out.print("\b\b");
      else
        System.out.print("\b\b\b");
    }
    System.out.println("Done");
    return response;
  }

  public double[] getMidEarCoeffs(){
    double[] midEarCoeffs = new double[numberOfChannel];
    for (int chan = 0; chan<numberOfChannel; chan++)
      midEarCoeffs[chan] = gchan[chan].midEarCoeff;
    return midEarCoeffs;
  }

  public float[] filterByChannel(float[] sig, int chan){
    float[] response = new float[sig.length];
    gchan[chan].InitChannel(chan);
    for (int t = 0; t < sig.length; t++)
      response[t] = UpdateFilter(chan, sig[t], t);

    return response;
  }

}