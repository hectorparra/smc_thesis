# -*- coding: utf-8 -*-
# This script generates two sub dataset from MIR-1K:
#   mir1k_mini.json: has an excerpt for each singer's song (there are 110 songs)
#   mir1k_micro.json: has an excerpt for each singer (there are 19 singers)
#
# The excerpts are selected randomly, so every time the script is executed a different subdataset is created
#
# It also creates a json with the complete dataset: mir1k.json

from pylab import *
import glob
import random
import imp
common = imp.load_source('common', '../common/common.py')

wav_files = glob.glob('Wavfile/*.wav')

singer_past = None
song_past = None
mir1k = []
mir1k_mini = []
mir1k_micro = []
singer_excerpts = {}
song_excerpts = {}

for wav_file in wav_files:
  filename = common.get_filename(wav_file)
  singer, song_num, excerpt_num = filename.split('_')
  song = '%s_%s' % (singer, song_num)
  
  if singer != singer_past: 
    singer_excerpts[singer] = []
    singer_past = singer
  if song != song_past:
    song_excerpts[song] = []
    song_past = song
  
  singer_excerpts[singer] += [filename]
  song_excerpts[song] += [filename]
  mir1k += [filename]
    
for key in singer_excerpts.keys(): mir1k_micro += [random.choice(singer_excerpts[key])]
for key in song_excerpts.keys(): mir1k_mini += [random.choice(song_excerpts[key])]
  
# Even though they will be probably already sorted and there is no need for that,
# we just prefer to save them ordered alphabetically
mir1k_micro = sort(mir1k_micro)  
mir1k_mini = sort(mir1k_mini)

common.save_json(mir1k_micro, 'mir1k_micro.json')
common.save_json(mir1k_mini, 'mir1k_mini.json')
common.save_json(mir1k, 'mir1k.json')
