# -*- coding: utf-8 -*-

from pylab import *
from mpl_toolkits.mplot3d import Axes3D
import imp
common = imp.load_source('common', os.path.join(os.path.dirname(__file__), '../../common/common.py'))

results = common.read_json('gridsearch.json')

#==============================================================================
# A line for every minpeaksalience value
#==============================================================================
plots = {}
for result in results:
  key = result['minpeaksalience']
  if not plots.has_key(key): plots[key] = {'x':[], 'y':[]}
  plots[key]['x'].append(result['voicing'])
  plots[key]['y'].append(result['excerpt']['Overall Accuracy'])  
  
figure()
i = 0
for key in sort(plots.keys()):
  i += 1
  style = '-' if i < 9 else '--'   
  plot(plots[key]['x'], plots[key]['y'], label=key, linestyle=style)

xlabel('voicing')
ylabel('accuracy')
legend(loc='upper left')

#==============================================================================
# A line for every voicing value
#==============================================================================
plots = {}
for result in results:
  key = result['voicing']
  if not plots.has_key(key): plots[key] = {'x':[], 'y':[]}
  plots[key]['x'].append(result['minpeaksalience'])
  plots[key]['y'].append(result['excerpt']['Overall Accuracy'])  
  
figure()
i = 0
for key in sort(plots.keys()):
  i += 1
  style = '-' if i < 9 else '--' 
  plot(plots[key]['x'], plots[key]['y'], label=key, linestyle=style)

xlabel('minpeaksalience')
ylabel('accuracy')
legend(loc='upper left')

#==============================================================================
# Temperature map
#==============================================================================
x, y, z = array([]), array([]), array([])
for result in results:
  x = append(x, result['minpeaksalience'])
  y = append(y, result['voicing'])
  z = append(z, result['excerpt']['Overall Accuracy'])
  
fig = figure()  
ax = fig.add_subplot(111, projection='3d')
ax.plot_trisurf(x,y,z, cmap = cm.jet)
xlabel('minpeaksalience')
ylabel('voicing')
ax.set_zlabel('accuracy')