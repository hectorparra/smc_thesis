The Gam_Mag_ITD.m is the top MATLAB file for ITD processing.

Please refer to the following paper for algorithm details:
% AN ITERATIVE LEAST-SQUARES TECHNIQUE FOR DEREVERBERATION

@inproceedings{NMF10,
title = "An Iterative Least-Squares Technique For Dereverberation",
author =  "K. Kumar and Bhiksha Raj Rita Singh and  R. M. Stern",
booktitle= "in Proc. IEEE ICASSP",
year = "2011 (submitted)"
}

Abstract - Some recent dereverberation approaches that have been effective for
ASR applications, model reverberation as a linear convolution operation
in the spectral domain, and derive a factorization to decompose
spectra of reverberated speech in to those of clean speech and
the room-response filter. Typically, a general NMF framework is
employed for this. In this work1 we present an alternative to NMF
and propose an iterative least-squares deconvolution technique for
spectral factorization. We propose an efficient algorithm for this and
experimentally demonstrate it�s effectiveness in improving ASR performance.
The new method results in 40-50% relative reduction in
word error rates over standard baselines on reverberated speech.

Implementation Details -
The file Gam_Mag_ITD.m takes 2 inputs, an input wavfile (MS-WAV format) to read speech data and an output file to write
the NMF processed speech data. The file lists a number of parameters for speech analysis and synthesis. A Gammatone transformation
matrix is first evaluated in the "gH" parameter. The transformation matrix is applied on Fourier magnitude spectra to obtain Gammatone
magnitude spectra. Currently gH is evaluated at every run of the algorithm - it should ideally be saved and recalled when needed
(similarly "igH" as well).

derev_ITD.m is the key file that applies the ITD processing algorithm. The ITD algorithm is at first initialized with
the output of NMF algorithm. NMF processing is run for a specified number of iterations, from which an initial estimate
of reverberation filter is evaluated. This estimate is further updated by ITD processing. The ITD algorithm uses levinson
recursion to speed up evaluating the least-sqaures solution.

Feature Extraction - 
The ITD algorithm currently processes an input speech file and outputs a reconstructed (dereverberated) speech. 
For ASR applications conventional MFCC/PLP features can be obtained from the dereverberated speech.
In our experiments we used SPHINX (wave2feat executable) for features extraction
http://cmusphinx.sourceforge.net/html/cmusphinx.php

Test - 
The approach can be tested by running the command
Gam_Mag_ITD('input.wav', 'output.wav'); 
and comparing the data in 'output.wav' file with it's corresponding data in 'output_ref.wav'.