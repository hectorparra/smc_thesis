# -*- coding: utf-8 -*-

from pylab import *
import imp, glob
common = imp.load_source('common', '../common/common.py')

mir_folder = '../MIR-1K/Wavfile'
mir_reverb_folders = glob.glob('../MIR-1K reverb/air_*')
mir_dereverb_folders = glob.glob('../MIR-1K dereverb/**/air_*')

algorithms = ['yin', 'twm', 'mel', 'sac']
results_file = {'yin': 'results.yin.json', 'twm': 'results.twm.json', 'mel': 'results.mel.json', 'sac': 'results_micro.sac.json'}
attrs = ['Overall Accuracy', 'Raw Pitch Accuracy', 'Voicing False Alarm']

colors = 'bgrcmyk' #w

#==============================================================================
# PLOT 1
#==============================================================================


plot1 = {}
for attr in attrs: plot1[attr] = []

for algorithm in algorithms:
  results = common.read_json('%s/%s' % (mir_folder, results_file[algorithm])) 
  for attr in attrs: plot1[attr].append(results['excerpt'][attr]*100)

fig = figure()
ax = fig.add_subplot(111)
idx = arange(len(algorithms), dtype=float)
width = 0.2

def autolabel(rects):
  # attach some text labels
  for rect in rects:
    height = rect.get_height()
    ax.text(rect.get_x()+rect.get_width()/2., height+0.5, '%d'%int(height), ha='center', va='bottom')  

for i in range(len(attrs)):  
  rect = ax.bar(idx + width*i, plot1[attrs[i]], width, bottom=0, color=colors[i])
  autolabel(rect)
  
ax.set_ylabel('percentage')
ax.set_title('Algorithm performance results')
ax.set_xticks(idx + len(attrs)*width/2.0)
ax.set_xticklabels(algorithms)
#ax.legend(attrs, loc='upper right', fontsize='xx-small', fancybox=True)  
show()  