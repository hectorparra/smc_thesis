package casa;



public class Correlogram {

	public float[][][] ac, acl, acb, acOdd, acEven;
	public float[][] cc, addf;
	public int timeFrame;
	private int maxDelay;
	public double[] cFreq;
	public int numChannel, numSample, numFrame, emptyInteval, sampFreq;

	public Correlogram(FrontEnd fe) {
		//r = fe.r;
		numChannel = fe.numChannel;
		numSample = fe.numSample;
		numFrame = fe.numFrame;
		sampFreq = fe.sampFreq;
		timeFrame = fe.timeFrame;
		emptyInteval = fe.emptyInteval;
		cFreq = fe.cFreq;
	}

	public void GetNormalizedCorrelogram(float[][] r) {
		maxDelay = 405;
		if (acl == null)
			acl = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						acl[chan][tf][delay] = 0;
		}
		int windowSize = 256;
		System.out.print("Getting correlogram...");
		for (int chan = 0; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
			for (int tf = 0; tf < numFrame; tf++) {
				int base = emptyInteval + tf * timeFrame;
				double lowerLeft = 0;
				for (int t = -windowSize / 2; t < windowSize / 2; t++)
					lowerLeft += r[chan][base + t] * r[chan][base + t];

				for (int delay = 0; delay < maxDelay; delay++) {
					double upper = 0;
					double lowerRight = 0;
					for (int t = -windowSize / 2; t < windowSize / 2; t++) {
						upper += r[chan][base + t] * r[chan][base + t - delay];
						lowerRight += r[chan][base + t - delay] * r[chan][base + t -
						                                                  delay];
					}
					if (Math.abs(lowerLeft) < 1e-6 || Math.abs(lowerRight) < 1e-6)

						//threshold for no-energy T-F units
						acl[chan][tf][delay] = 0;
					else
						acl[chan][tf][delay] = (float) (upper / Math.sqrt(lowerLeft) /
								Math.sqrt(lowerRight));
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
	}

	public void GetNormalizedBigCorrelogram(float[][] r) {
		maxDelay = 405;
		if (acb == null)
			acb = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						acb[chan][tf][delay] = 0;
		}
		int windowSizeBig = 480;
		System.out.print("Getting correlogram...");
		for (int chan = 55; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
			if (cFreq[chan] > 0) {
				for (int tf = 0; tf < numFrame; tf++) {
					int base = emptyInteval + tf * timeFrame;
					double lowerLeft = 0;
					for (int t = -windowSizeBig / 2; t < windowSizeBig / 2; t++)
						lowerLeft += r[chan][base + t] * r[chan][base + t];

					for (int delay = 0; delay < maxDelay; delay++) {
						double upper = 0;
						double lowerRight = 0;
						for (int t = -windowSizeBig / 2; t < windowSizeBig / 2; t++) {
							upper += r[chan][base + t] * r[chan][base + t - delay];
							lowerRight += r[chan][base + t - delay] * r[chan][base + t -
							                                                  delay];
						}
						if (Math.abs(lowerLeft) < 1e-6 || Math.abs(lowerRight) < 1e-6)

							//threshold for no-energy T-F units
							acb[chan][tf][delay] = 0;
						else
							acb[chan][tf][delay] = (float) (upper / Math.sqrt(lowerLeft) /
									Math.sqrt(lowerRight));
					}
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
	}

	public void GetCorrelogram(float[][] r) {
		maxDelay = 200;
		if (ac == null)
			ac = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						ac[chan][tf][delay] = 0;
		}
		int windowSize;
		System.out.print("Getting correlogram...");
		for (int chan = 0; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
			windowSize = (int) (4 * sampFreq / cFreq[chan]);
			if (windowSize < 320)
				windowSize = 320;
			for (int tf = 0; tf < numFrame; tf++) {
				for (int delay = 0; delay < maxDelay; delay++) {
					double tmp = 0;
					for (int t = 0; t < windowSize; t++) {
						int tim = (tf + 2) * timeFrame - (t + 1);
						if ( (tim - delay >= 0) && (tim < numSample))
							tmp += r[chan][tim] * r[chan][tim - delay];
					}
					ac[chan][tf][delay] = (float) (tmp / windowSize);
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
	}

	public void GetCorrelogramOdd(float[][] r) {
		maxDelay = 200;
		if (acOdd == null)
			acOdd = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						acOdd[chan][tf][delay] = 0;
		}
		int windowSize;
		System.out.print("Getting correlogram...");
		for (int chan = 0; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
			windowSize = (int) (4 * sampFreq / cFreq[chan]);
			if (windowSize < 320)
				windowSize = 320;
			for (int tf = 0; tf < numFrame; tf++) {
				for (int delay = 0; delay < maxDelay; delay++) {
					double tmp = 0;
					for (int t = 0; t < windowSize; t++) {
						int tim = (tf + 2) * timeFrame - (t + 1);
						if ( (tim - delay >= 0) && (tim < numSample))
							tmp += r[chan][tim] * r[chan][tim - delay];
					}
					acOdd[chan][tf][delay] = (float) (tmp / windowSize);
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
	}

	public void GetCorrelogramEven(float[][] r) {
		maxDelay = 200;
		if (acEven == null)
			acEven = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						acEven[chan][tf][delay] = 0;
		}
		int windowSize;
		System.out.print("Getting correlogram...");
		for (int chan = 0; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
			windowSize = (int) (4 * sampFreq / cFreq[chan]);
			if (windowSize < 320)
				windowSize = 320;
			for (int tf = 0; tf < numFrame; tf++) {
				for (int delay = 0; delay < maxDelay; delay++) {
					double tmp = 0;
					for (int t = 0; t < windowSize; t++) {
						int tim = (tf + 2) * timeFrame - (t + 1);
						if ( (tim - delay >= 0) && (tim < numSample))
							tmp += r[chan][tim] * r[chan][tim - delay];
					}
					acEven[chan][tf][delay] = (float) (tmp / windowSize);
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
	}

	public void GetCorrelogramN(float[][] r) {
		maxDelay = 200;
		int windowSize, tim;
		double lowerLeft, lowerRight, upper;
		if (acl == null)
			acl = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						acl[chan][tf][delay] = 0;
		}
		System.out.print("Getting correlogram...");
		for (int chan = 0; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
			windowSize = (int) (4 * sampFreq / cFreq[chan]);
			if (windowSize < 320)
				windowSize = 320;

			for (int tf = 0; tf < numFrame; tf++) {
				lowerLeft = 0;
				for (int t = 0; t < windowSize; t++) {
					tim = (tf + 2) * timeFrame - (t + 1);
					if ( (tim >= 0) && (tim < numSample))
						lowerLeft += r[chan][tim] * r[chan][tim];
				}

				for (int delay = 0; delay < maxDelay; delay++) {
					upper = lowerRight = 0;
					for (int t = 0; t < windowSize; t++) {
						tim = (tf + 2) * timeFrame - (t + 1);
						if ( (tim - delay >= 0) && (tim < numSample)) {
							upper += r[chan][tim] * r[chan][tim - delay];
							lowerRight += r[chan][tim - delay] * r[chan][tim - delay];
						}
					}
					if (Math.abs(lowerLeft) < 1e-6 || Math.abs(lowerRight) < 1e-6)
						acl[chan][tf][delay] = 0;
					else
						acl[chan][tf][delay] = (float) (upper / Math.sqrt(lowerLeft) /
								Math.sqrt(lowerRight));
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
		ac = acl;
	}

	public void GetCorrelogramN(int maxDelay,float[][] r) {
		this.maxDelay = maxDelay;
		int windowSize, tim;
		double lowerLeft, lowerRight, upper;
		if (acl == null)
			acl = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						acl[chan][tf][delay] = 0;
		}
		System.out.print("Getting correlogram...");
		for (int chan = 0; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
			windowSize = (int) (4 * sampFreq / cFreq[chan]);
			if (windowSize < 320)
				windowSize = 320;

			for (int tf = 0; tf < numFrame; tf++) {
				lowerLeft = 0;
				for (int t = 0; t < windowSize; t++) {
					tim = (tf + 2) * timeFrame - (t + 1);
					if ( (tim >= 0) && (tim < numSample))
						lowerLeft += r[chan][tim] * r[chan][tim];
				}

				for (int delay = 0; delay < maxDelay; delay++) {
					upper = lowerRight = 0;
					for (int t = 0; t < windowSize; t++) {
						tim = (tf + 2) * timeFrame - (t + 1);
						if ( (tim - delay >= 0) && (tim < numSample)) {
							upper += r[chan][tim] * r[chan][tim - delay];
							lowerRight += r[chan][tim - delay] * r[chan][tim - delay];
						}
					}
					if (Math.abs(lowerLeft) < 1e-6 || Math.abs(lowerRight) < 1e-6)
						acl[chan][tf][delay] = 0;
					else
						acl[chan][tf][delay] = (float) (upper / Math.sqrt(lowerLeft) /
								Math.sqrt(lowerRight));
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
		ac = acl;
	}
	
	public void GetCorrelogramNBig(int maxDelay,float[][] r) {
		int windowSize, tim;
		double lowerLeft, lowerRight, upper;
		if (acb == null)
			acb = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						acb[chan][tf][delay] = 0;
		}
		System.out.print("Getting correlogram...");
		for (int chan = 56; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
			windowSize = 480;

			for (int tf = 0; tf < numFrame; tf++) {
				lowerLeft = 0;
				for (int t = 0; t < windowSize; t++) {
					tim = (tf + 2) * timeFrame - (t + 1);
					if ( (tim >= 0) && (tim < numSample))
						lowerLeft += r[chan][tim] * r[chan][tim];
				}

				for (int delay = 0; delay < maxDelay; delay++) {
					upper = lowerRight = 0;
					for (int t = 0; t < windowSize; t++) {
						tim = (tf + 2) * timeFrame - (t + 1);
						if ( (tim - delay >= 0) && (tim < numSample)) {
							upper += r[chan][tim] * r[chan][tim - delay];
							lowerRight += r[chan][tim - delay] * r[chan][tim - delay];
						}
					}
					if (Math.abs(lowerLeft) < 1e-6 || Math.abs(lowerRight) < 1e-6)
						acb[chan][tf][delay] = 0;
					else
						acb[chan][tf][delay] = (float) (upper / Math.sqrt(lowerLeft) /
								Math.sqrt(lowerRight));
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
	}

	public void GetCorrelogramN(float[][] r, int windowSize) {
		maxDelay = 200;
		int tim;
		double lowerLeft, lowerRight, upper;
		if (acl == null)
			acl = new float[numChannel][numFrame][maxDelay];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					for (int delay = 0; delay < maxDelay; delay++)
						acl[chan][tf][delay] = 0;
		}
		System.out.print("Getting correlogram...");
		for (int chan = 0; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");

			for (int tf = 0; tf < numFrame; tf++) {
				lowerLeft = 0;
				for (int t = 0; t < windowSize; t++) {
					tim = (tf + 2) * timeFrame - (t + 1);
					if ( (tim >= 0) && (tim < numSample))
						lowerLeft += r[chan][tim] * r[chan][tim];
				}

				for (int delay = 0; delay < maxDelay; delay++) {
					upper = lowerRight = 0;
					for (int t = 0; t < windowSize; t++) {
						tim = (tf + 2) * timeFrame - (t + 1);
						if ( (tim - delay >= 0) && (tim < numSample)) {
							upper += r[chan][tim] * r[chan][tim - delay];
							lowerRight += r[chan][tim - delay] * r[chan][tim - delay];
						}
					}
					if (Math.abs(lowerLeft) < 1e-6 || Math.abs(lowerRight) < 1e-6)
						acl[chan][tf][delay] = 0;
					else
						acl[chan][tf][delay] = (float) (upper / Math.sqrt(lowerLeft) /
								Math.sqrt(lowerRight));
				}
			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
		ac = acl;
	}

	public void crossChanCorr() {
		maxDelay = 200;
		if (cc == null)
			cc = new float[numChannel][numFrame];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					cc[chan][tf] = 0;
		}

		double tmp;
		double[][] acf;

		System.out.print("Getting cross channel correlation...");
		for (int tf = 0; tf < numFrame; tf++) {
			acf = new double[numChannel][maxDelay];
			for (int chan = 0; chan < numChannel; chan++) {
				//for (int delay = 0; delay < maxDelay; delay++)
				//acf[chan][delay] = ac[chan][tf][delay];
				tmp = 0;
				for (int delay = 0; delay < maxDelay; delay++)
					tmp += ac[chan][tf][delay];

				tmp /= maxDelay;
				for (int delay = 0; delay < maxDelay; delay++) {
					acf[chan][delay] = ac[chan][tf][delay] - (float) tmp;
				}

				tmp = 0;
				for (int delay = 0; delay < maxDelay; delay++) {
					tmp += acf[chan][delay] * acf[chan][delay];
				}

				tmp = Math.sqrt(tmp / maxDelay);
				if (tmp != 0)
					for (int delay = 0; delay < maxDelay; delay++)
						acf[chan][delay] /= tmp;
			}

			for (int chan = 0; chan < (numChannel - 1); chan++) {
				for (int delay = 0; delay < maxDelay; delay++)
					cc[chan][tf] += acf[chan][delay] * acf[chan + 1][delay];
				cc[chan][tf] /= maxDelay;
			}
			cc[numChannel - 1][tf] = 0;

		}
		System.out.println("Done");
	}

	public void crossChanCorrMix() {
		maxDelay = 200;
		if (cc == null)
			cc = new float[numChannel][numFrame];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					cc[chan][tf] = 0;
		}

		double tmp;
		double[][] acf1, acf2;

		System.out.print("Getting cross channel correlation...");
		for (int tf = 0; tf < numFrame; tf++) {
			acf1 = new double[numChannel][maxDelay];
			for (int chan = 0; chan < numChannel; chan++) {
				//for (int delay = 0; delay < maxDelay; delay++)
				//acf[chan][delay] = ac[chan][tf][delay];
				tmp = 0;
				for (int delay = 0; delay < maxDelay; delay++)
					tmp += acOdd[chan][tf][delay];

				tmp /= maxDelay;
				for (int delay = 0; delay < maxDelay; delay++) {
					acf1[chan][delay] = acOdd[chan][tf][delay] - (float) tmp;
				}

				tmp = 0;
				for (int delay = 0; delay < maxDelay; delay++) {
					tmp += acf1[chan][delay] * acf1[chan][delay];
				}

				tmp = Math.sqrt(tmp / maxDelay);
				if (tmp != 0)
					for (int delay = 0; delay < maxDelay; delay++)
						acf1[chan][delay] /= tmp;

			}
			acf2 = new double[numChannel][maxDelay];
			for (int chan = 0; chan < numChannel; chan++) {
				//for (int delay = 0; delay < maxDelay; delay++)
				//acf[chan][delay] = ac[chan][tf][delay];
				tmp = 0;
				for (int delay = 0; delay < maxDelay; delay++)
					tmp += acEven[chan][tf][delay];

				tmp /= maxDelay;
				for (int delay = 0; delay < maxDelay; delay++) {
					acf2[chan][delay] = acEven[chan][tf][delay] - (float) tmp;
				}

				tmp = 0;
				for (int delay = 0; delay < maxDelay; delay++) {
					tmp += acf2[chan][delay] * acf2[chan][delay];
				}

				tmp = Math.sqrt(tmp / maxDelay);
				if (tmp != 0)
					for (int delay = 0; delay < maxDelay; delay++)
						acf2[chan][delay] /= tmp;

			}

			for (int chan = 0; chan < (numChannel - 1); chan++) {
				for (int delay = 0; delay < maxDelay; delay++)
					if (chan % 2 == 0)
						cc[chan][tf] += acf1[chan][delay] * acf1[chan + 1][delay];
					else
						cc[chan][tf] += acf2[chan][delay] * acf2[chan + 1][delay];
				cc[chan][tf] /= maxDelay;
			}
			cc[numChannel - 1][tf] = 0;

		}
		System.out.println("Done");
	}

	public void crossChanCorrN() {
		maxDelay = 200;
		if (cc == null)
			cc = new float[numChannel][numFrame];
		else {
			for (int chan = 0; chan < numChannel; chan++)
				for (int tf = 0; tf < numFrame; tf++)
					cc[chan][tf] = 0;
		}

		double tmp;
		double[][] acf;

		System.out.print("Getting cross channel correlation...");
		for (int tf = 0; tf < numFrame; tf++) {
			acf = new double[numChannel][maxDelay];
			for (int chan = 0; chan < numChannel; chan++) {
				//for (int delay = 0; delay < maxDelay; delay++)
				//acf[chan][delay] = ac[chan][tf][delay];
				tmp = 0;
				for (int delay = 32; delay < maxDelay; delay++)
					tmp += acl[chan][tf][delay];

				tmp /= (maxDelay - 32);
				for (int delay = 32; delay < maxDelay; delay++) {
					acf[chan][delay] = acl[chan][tf][delay] - (float) tmp;
				}

				tmp = 0;
				for (int delay = 32; delay < maxDelay; delay++) {
					tmp += acf[chan][delay] * acf[chan][delay];
				}

				tmp = Math.sqrt(tmp / (maxDelay - 32));
				if (tmp != 0)
					for (int delay = 32; delay < maxDelay; delay++)
						acf[chan][delay] /= tmp;
			}

			for (int chan = 0; chan < (numChannel - 1); chan++) {
				for (int delay = 32; delay < maxDelay; delay++)
					cc[chan][tf] += acf[chan][delay] * acf[chan + 1][delay];
				cc[chan][tf] /= (maxDelay - 32);
			}
			cc[numChannel - 1][tf] = 0;

		}
		System.out.println("Done");
	}

	public float[][] GetEnergy(float[][] r) {
		float[][] energy = new float[numChannel][numFrame];
		//System.out.println(numSample);
		//System.out.println(numSample / timeFrame);
		int windowSize = 320;
		System.out.print("Getting correlogram...");
		for (int chan = 0; chan < numChannel; chan++) {
			System.out.print(Math.round( (float) chan / numChannel * 100) + "%");

			for (int tf = 0; tf < numFrame; tf++) {
				int delay = 0;
				double tmp = 0;
				for (int t = 0; t < windowSize; t++) {
					int tim = (tf + 2) * timeFrame - (t + 1);
					if ( (tim - delay >= 0) && (tim < numSample))
						tmp += r[chan][tim] * r[chan][tim - delay];
				}
				energy[chan][tf] = (float) tmp / windowSize;

			}
			if (Math.round( (float) chan / numChannel * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");
		return (energy);
	}

	public void GetAddf() {
		addf = new float[numChannel][numFrame];
		double term1, term2, term3;
		for (int chan = 0; chan < numChannel - 1; chan++)
			for (int tf = 0; tf < numFrame; tf++) {
				term1 = term2 = term3 = 0;
				for (int delay = 0; delay < 200; delay++) {
					term1 += ac[chan][tf][delay] * ac[chan][tf][delay];
					term2 += ac[chan + 1][tf][delay] * ac[chan + 1][tf][delay];
					term3 += (ac[chan + 1][tf][delay] - ac[chan][tf][delay]) *
					(ac[chan + 1][tf][delay] - ac[chan][tf][delay]);
				}
				addf[chan][tf] = (float) ( (Math.sqrt(term3) -
						Math.abs(Math.sqrt(term1) -
								Math.sqrt(term2))) /
								(Math.sqrt(term1) + Math.sqrt(term2)));
			}
	}

	public static float GetCCDist(float[] vec1, float[] vec2) {
		int md = vec1.length;

		double tmp;
		float[][] acf = new float[2][md];

		// normalize vec1
		tmp = 0;
		for (int delay = 0; delay < md; delay++)
			tmp += vec1[delay];
		tmp /= md;
		for (int delay = 0; delay < md; delay++)
			acf[0][delay] = vec1[delay] - (float) tmp;

		tmp = 0;
		for (int delay = 0; delay < md; delay++)
			tmp += acf[0][delay] * acf[0][delay];

		tmp = Math.sqrt(tmp / md);
		if (tmp != 0)
			for (int delay = 0; delay < md; delay++)
				acf[0][delay] /= tmp;

		// normalize vec2
		tmp = 0;
		for (int delay = 0; delay < md; delay++)
			tmp += vec2[delay];
		tmp /= md;
		for (int delay = 0; delay < md; delay++)
			acf[1][delay] = vec2[delay] - (float) tmp;

		tmp = 0;
		for (int delay = 0; delay < md; delay++)
			tmp += acf[1][delay] * acf[1][delay];

		tmp = Math.sqrt(tmp / md);
		if (tmp != 0)
			for (int delay = 0; delay < md; delay++)
				acf[1][delay] /= tmp;

		tmp = 0;
		for (int delay = 0; delay < md; delay++)
			tmp += acf[0][delay] * acf[1][delay];
		tmp /= md;

		return (float) tmp;
	}

}