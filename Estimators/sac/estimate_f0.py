# -*- coding: utf-8 -*-

from pylab import *
import glob, os, shutil
from fusha import FushaBar
from subprocess import call
import imp
common = imp.load_source('common', '../../common/common.py')
#from scikits.audiolab import Format, Sndfile 
#from scikits.samplerate import resample
#
#def save_resampled(wav_in, wav_out, fs_out = 44100):
#  if os.path.exists(wav_out): return; 
#  # Read input audio
#  f_in = Sndfile(wav_in, 'r')
#  data = f_in.read_frames(f_in.nframes)
#  # Resample audio  
#  resampled_data = resample(data, fs_out/float(f_in.samplerate), 'sinc_best')
#  # Write audio to output
#  f_out = Sndfile(wav_out, 'w', Format('wav'), 1, fs_out)
#  f_out.write_frames(resampled_data)
#  f_out.close  

mir_path = '../../MIR-1K/Wavfile'
mir_reverb_folders = glob.glob('../../MIR-1K reverb/air_*')
mir_dereverb_folders = glob.glob('../../MIR-1K dereverb/**/air_*')
# folders to evaluate
eval_folders = []
eval_folders += [mir_path] 
eval_folders += mir_reverb_folders
eval_folders += mir_dereverb_folders

binary = "ToolsCMD_TOTEST_Nov2012.exe"
config_file = 'anl_sac.sco'

filenames = common.mir1k_micro()
wav_files = []
for eval_folder in eval_folders:
  #wav_files += glob.glob('%s/*.wav' % eval_folder)
  wav_files += map(lambda f: '%s/%s.wav' % (eval_folder, f), filenames)

with FushaBar(interval=1, bar_len=100) as bar:
  i = 0
  for wav_file in wav_files:
    filename = wav_file[:-4] # remove .wav extension
    csv_file = '%s.sac.f0.csv' % filename
    f0_file = '%s.sac.f0.json' % filename
    resampled_wav_file = '%s.wav.44' % filename
    tmp_resampled = '%s_tmp.wav' % filename
    
#    save_resampled(wav_file, resampled_wav_file)
    
    shutil.move(resampled_wav_file, tmp_resampled)    
    call([binary, '-f0-analysis',  config_file, tmp_resampled, csv_file]) # estimate f0
    shutil.move(tmp_resampled, resampled_wav_file)
    
    csv_data = loadtxt(csv_file) # load estimation 
    # notice how first data point is ommitted since MIR1K starts at 20ms and TWM at 0mss
    data = { 'time': csv_data[1:,0], 'f0': common.freq2pitch(csv_data[1:,1]) }
    
    common.save_json(data, f0_file)
    os.remove('%s_tmp.m' % filename) # get rid of useless matlab file
    os.remove(csv_file) # remove since we saved it on the json file   
    
    i += 1
    bar.update(i*100//len(wav_files))
    
print "Estimation finished!" 
  