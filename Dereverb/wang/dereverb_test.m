% This script is a little test to check how this dereverberation works
clear all;close all;clc
addpath('src');

input = 'heycat_1_07_reverb.wav';
output_inv = 'heycat_1_07_inverse.wav';
output = 'heycat_1_07_dereverb.wav';

rev = double(wavread(input, 'native'));

% inverse filter
inv  = inverse_filter_custom(rev); 

% spectral subtraction
derev = spec_sub_derev(inv, 16000);

% output wav files
wavwrite(inv/(max(abs(inv)))*0.95, 16000, output_inv);
wavwrite(derev/(max(abs(derev)))*0.95, 16000, output);