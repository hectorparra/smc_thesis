addpath('yin_src'); % import yin code
addpath('jsonlab'); % import code to save data as json

mir_dir = '../../MIR-1K/Wavfile';
wav_files = dir(sprintf('%s/*.wav', mir_dir));

% YIN parameters
% f0 boundaries extracted from ground truth + 1 semitone
P.minf0 = 63.1883;
P.maxf0 = 706.3069;
P.hop = 320; % 20ms = 320 samples
P.wsize = 640; % 40ms = 640 samples
%P.draw_plot = true;

% JSON save parameters
opt.ForceRootName = 0; % do not use root element
opt.Inf = '$1Infinity'; % use standard JS symbol
opt.NaN = 'NaN'; % use standard JS symbol

progressbar = waitbar(0,'Initializing...'); tic;

for i = 1:length(wav_files)
  filename = wav_files(i).name;
  soundfile = sprintf('%s/%s', mir_dir, filename);
  
  [x, fs, nbits] = wavread(soundfile); % 16kHz, 16bits
  %x = x(:, 2); % Select only voice, i.e. Right channel
  
  % YIN(NAME,P) uses parameters stored in P:
  %   P.minf0:    Hz - minimum expected F0 (default: 30 Hz)
  %   P.maxf0:    Hz - maximum expected F0 (default: SR/(4*dsratio))
  %   P.thresh:   threshold (default: 0.1)
  %   P.relfag:   if ~0, thresh is relative to min of difference function (default: 1)
  %   P.hop:      s - interval between estimates (default: 32/SR)
  %   P.range:    samples - range of samples ([start stop]) to process
  %   P.bufsize:  samples - size of computation buffer (default: 10000)
  %  	P.sr:		Hz - sampling rate (usually taken from file header)
  % 	P.wsize:	samples - integration window size (defaut: SR/minf0)
  % 	P.lpf:		Hz - intial low-pass filtering (default: SR/4)
  % 	P.shift		0: shift symmetric, 1: shift right, -1: shift left (default: 0)

  % Call yin
  P.sr = fs; % althought it is fixed at 16KHz for MIR-1K 
  R = yinf(x, P);
  R.f0 = freq2pitch(R.f0); % save in semitones (as MIR-1K)  

  % Save results to JSON
  [path, name, ~] = fileparts(soundfile); % get rid off .wav extension
  opt.FileName = sprintf('%s/%s.yin.f0.json', path, name);
  savejson('', R, opt);  
  
  % progress bar
  time = toc;
  perc = i / length(wav_files);
  trem = time/perc-time; %Calculate the time remaining
  hrs = floor(trem/3600);
  min = floor((trem-hrs*3600)/60);
  waitbar(perc, progressbar, ...
    sprintf('%0.1f%% %03.0f:%02.0f:%02.0f ETA', perc*100, hrs, min, rem(trem,60)));
  
end

close(progressbar); 

