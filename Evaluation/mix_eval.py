# -*- coding: utf-8 -*-

from pylab import *
import glob
import evaluation

mir_dereverb_algorithm = '../MIR-1K dereverb/nml_micro' #glob.glob('%s/*' % mir_dereverb_path)
mir_dereverb_folders = glob.glob('%s/air_*' % mir_dereverb_algorithm)

for folder in mir_dereverb_folders:
  evaluation.evaluate_folder(folder, 'mix')
  evaluation.folder_results_micro(folder, 'mix')
  
evaluation.merge_folder_results_micro(mir_dereverb_folders, 'mix', mir_dereverb_algorithm)    

print "Finished!!!!!!!!"