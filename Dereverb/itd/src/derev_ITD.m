function [y] = derev_ITD(x, analsize, reverblen, sparsity, iterations, hp, gH, igH, I_ITD)

% Magnitude domain Gammatone subband NMF processing

% x:  input speech data
% analsize:  FFT size
% reverblen: Length of reverb in frames
% sparsity:  Amount of sparsity.
% iterations:  Number of iterations
% hp: window hop length
% gH: Gammatone transformation matrix
% igH: inverse Gammatone transformation matrix

hwindow = hamming(analsize);

% STFT - Fourier spectra
F = stft(x, analsize, hp, 0, hwindow);
Fa = abs(F);

% Gammatone spectra
Ga = gH' * Fa;

% Z is observed reverberated spectra
Z = Ga;
lm = sparsity*(sum( Z(:)) * 10^-4);

% Initialize H filter
H = linspace( 1, .7, reverblen+1); H = repmat(H, size(Z,1), 1);

% Initialize the spectral component X as just Z
X = Z(:,1:end-reverblen);

for i = 1:iterations
    Y = convlr(X, H);
    
    % The NMF update equations
    Xe = convlr(H, Z, [1 0]) ./ (convlr( H, Y, [1 0]) + lm + eps);
    He = convlr(X, Z, [1 0]) ./ (convlr( X, Y, [1 0]) + eps);
    
    X = X .* Xe(:,1:size(X,2));
    H = H .* He(:,1:size(H,2));
    
    % Normalizing for H is not essential.
    H = H ./ repmat(sum( H, 2), 1, size( H,2) + eps);
    
    %subplot(2,1,1); imagesc(X.^.2); axis xy;    subplot(2,1,2); imagesc(H);
end

for i = 1:I_ITD             
    X = update_T_levinson(H, Z); X(X < eps) = eps;
    H = update_T_levinson(X, Z); H(H < eps) = eps;
    Y = convlr(X, H);
       
    % subplot(2, 1, 1), imagesc(abs(X).^.2), axis xy, ylabel( 'X')    
end

% Resynthesize. Note the addition of zeros to fill out the signal.
eGa = [X zeros(size(X,1), reverblen)];

% Back to Fourier spectrum
eFa = (igH * eGa);

% Back to time
fphase = F./abs(F);
finiteid = isfinite(fphase); fphase(finiteid==0) = 0;
y = stft( eFa .* fphase, analsize, hp, 0, hwindow);