function pitch = freq2pitch(freq)
%	Roger Jang, 20040524

pitch = 69+12*log2(freq/440);