# -*- coding: utf-8 -*-

from pylab import *
import glob, os
from fusha import FushaBar
import imp
common = imp.load_source('common', '../../common/common.py')

wav_files = glob.glob('../../MIR-1K dereverb/nml_micro/**/*.wav')

with FushaBar(interval=1, bar_len=20) as bar:
  i = 0
  for wav_file in wav_files:
    filename, _ = os.path.splitext(wav_file)
    sac_results = common.read_json('%s.sac.f0.json' % filename)
    mel_results = common.read_json('%s.mel.f0.json' % filename)    
        
    sac_f0 = array(sac_results['f0'])
    mel_f0 = array(mel_results['f0'])
    # make both arrays the same length (the shortest one)
    sac_f0 = sac_f0[:len(mel_f0)]
    mel_f0 = mel_f0[:len(sac_f0)]
    
    f0 = sac_f0 # use f0 from sac
    unvoiced = mel_f0 <= 0 # use voicing from melodia
    f0[unvoiced] = -f0[unvoiced]
        
    f0_file = '%s.mix.f0.json' % filename
    common.save_json({'f0': f0}, f0_file)
    
    i += 1
    bar.update(i*100//len(wav_files))
    
print "Estimation finished!"    
  