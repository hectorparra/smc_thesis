# -*- coding: utf-8 -*-
# This file searches the maximum and minimum value of pitch in the MIR-1K database ground truth

from pylab import *
import glob

gt_dir = '../MIR-1K/PitchLabel' # ground truth directory

gt_min = inf
gt_max = -inf

for filename in glob.glob("%s/*.pv" % gt_dir):
    gt = loadtxt(filename)
    file_max = gt.max()
    file_min = gt[gt != 0].min()
    if file_max > gt_max: gt_max = file_max
    if file_min < gt_min: gt_min = file_min
        
print "Maximum: %s" % gt_max
print "Minimum: %s" % gt_min 

# Results
# Maximum: 76.1935 semitones
# Minimum: 36.4027 semitones

# As the posterior evaluation is done using MIREX guidelines, where 1/2 semitone is tolerated,
# we propose to increase f0 boundaries by 1 semitone:
# Proposed maximum: 77.1935 semitones = 706.3069 Hz
# Proposed minimum: 35.4027 semitones = 63.1883 Hz
        