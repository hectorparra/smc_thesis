% This script generates the dereverberated files from already
% reveberverated files
clear all;close all;clc
addpath('src');

input = 'heycat_1_07_reverb.wav';
output = 'heycat_1_07_dereverb.wav';

Gam_Mag_NMF(input, output);