% Compute fundamental frequency estimation 
% using yin for 35 monophonic melodies
% %   R.f0: fundamental frequency in octaves re: 440 Hz
%
% Audio and Music Analysis, SMC Master
% emilia.gomez@upf.edu 
% 23rd of February 2009

filelist = dir('../melodies/*.wav');

for i=1:length(filelist)
    
   display(filelist(i).name)
  % yin(['../melodies/' filelist(i).name]);  to look at the plots
   r=yin(['../melodies/' filelist(i).name]); 
   time_index = (1:length(r.f0))*(r.hop/r.sr);
   
   fid = fopen(['../melodies/' filelist(i).name '.txt'],'w');
   for j=1:length(time_index) 
       fprintf(fid,'%f %f %f %f\n',time_index(j), r.f0(j), r.ap(j), r.pwr(j));
   end
   fclose(fid);
   
%pause
  
 end
