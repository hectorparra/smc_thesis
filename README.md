# Master Thesis

[Hector Parra](https://hector.parra.cat) master thesis for the master [Sound and Music Computing](https://www.upf.edu/web/smc) at [Universitat Pompeu Fabra](https://www.upf.edu/).

This repository contains all the files (python scripts, configuration files, audios files...) used to conduct the study of the thesis.
By the time I did this work I wasn't documenting how to use these files, but I believe in transparent and reproducible research thus I prefer to leave the work openly available.

The thesis can be [freely accesed here](http://mtg.upf.edu/node/2836).