# -*- coding: utf-8 -*-

from pylab import *
from subprocess import check_output
import imp, os
common = imp.load_source('common', os.path.join(os.path.dirname(__file__), '../../common/common.py'))

def evaluate(wav_file, transform = 'melodia_parameters.n3'):
  '''Executes the Vamp MELODIA pluguin using sonic-annotator and returns the results'''
  
  sonic_annotator = '/usr/local/bin/sonic-annotator'
  stdout = check_output([sonic_annotator, '-t', transform,'-w', 'csv', '--csv-basedir', '.', 
                         '--csv-force', '--csv-sample-timing', '--csv-stdout', wav_file])

  split = stdout.split(',')
  # split[0] is the path of the file
  # odd numbers are the frame number
  # even numbers are the pitch in Hz (negative for unvoiced)

  str2num = lambda x: float(x)
  nframe_mel = map(str2num, split[1:len(split):2])
  f0_mel = array(map(str2num, split[2:len(split):2]))
  
  # We need to interpolate data since hop size is fixed (46 instead of 320)
  # Para interpolar: crear nuevo vector con el signo del pitch, interporlar el pitch con valor absoluto, y volver a poner el signo
  nframe = arange(320, nframe_mel[-1] + 743//2, 320) # new sample ponts, MELODIA window size is fixed at 743 frames
  f0_hz = common.interpolate_pitch(nframe, nframe_mel, f0_mel)
  f0 = common.freq2pitch(f0_hz) # we want it in semitones
  
  return {
    'f0': f0, 
    'frame': nframe,
    'hop_size': 320,
    'window_size': 640,
    'max_f0': 77.1935,
    'min_f0': 35.4027,
    'melodia': {
      'f0': f0_mel,
      'frame': nframe_mel,
      'step_size': 46,
      'block_size': 743,
      'maxfqr': 706.3069,
      'minfqr': 63.1883,
      'minpeaksalience': 10,
      'voicing': 3
    }
  }