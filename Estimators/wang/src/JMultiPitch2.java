import java.io.*;
import casa.*;


public class JMultiPitch2 {

	public static void main(String args[]) {

		try {
			BufferedReader r = new BufferedReader(new FileReader(args[0]));

			int numSample = 0;
			String t = r.readLine();
			while (t != null) {
				numSample++;
				t = r.readLine();
			}
			System.out.println(numSample + " samples read.");
			r.close();

			int[] sig = new int[numSample];
			r = new BufferedReader(new FileReader(args[0]));
			for (int i = 0; i < numSample; i++)
				sig[i] = Integer.parseInt(r.readLine());
			r.close();

			Gammatone gt = new Gammatone();
			float[][] gOut = gt.FilterWithInnerCells(sig);
			FrontEnd fe = new FrontEnd(gOut, gt);
			fe.GuoNingFront();
			Correlogram corr = new Correlogram(fe);
			corr.GetCorrelogramN(200,fe.r);
			//corr.GetCorrelogramNBig(405,fe.r);
			corr.crossChanCorrN();
			
			
			/*FileWriter w = new FileWriter("cc",false);
			for (int tf = 0; tf < corr.numFrame; tf++){
				for (int chan = 0; chan < corr.numChannel; chan++)
					w.write(corr.cc[chan][tf]+" ");
				w.write("\n");
			}
			w.flush();
			w.close();
			*/
			
			float[][] energy = corr.GetEnergy(gt.Filter(sig));

			PitchProbModel ppm = new PitchProbModel(corr,energy);
			//ppm.ChannelSelection(gt);
			//ppm.PitchTrackingViterbipass1();
			//ppm.FixAC();
			
			ppm.isWideBand = false;
			ppm.PitchTrackingViterbipass2();
			ppm.WriteOutPitch(args[1], args[2]);
			//ppm.WriteMask();
			//ppm.WriteMaxPP(args[1]);

		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

}
