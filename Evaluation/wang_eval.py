# -*- coding: utf-8 -*-

from pylab import *
import glob
import evaluation

mir_path = '../MIR-1K/Wavfile'
mir_reverb_path = '../MIR-1K reverb'
mir_reverb_folders = glob.glob('%s/air_*' % mir_reverb_path)

eval_folders = [] # folders to evaluate
eval_folders += [mir_path]
eval_folders += mir_reverb_folders

for folder in eval_folders:
  evaluation.evaluate_folder(folder, 'wang')
  evaluation.folder_results_micro(folder, 'wang')
  
evaluation.merge_folder_results_micro(mir_reverb_folders, 'wang', mir_reverb_path) 
    
print "Finished!!!!!!!!"
    