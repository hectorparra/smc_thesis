function [X] = update_T_levinson(H, Y)

% Treat the feature dimensions of Y individually.
% Obtain the least squares solution for Xi, where
% Y = H * X.
% The above is solved via Levinson recursion. See the paper in readme.txt for details.

Nr = size(Y, 2);
Nh = size(H,2);
Nc = Nr + 1 - Nh;

X = zeros(size(Y,1), Nc);

for i = 1:size(Y,1)
    % Obtain a total of Nc values of Phi
    Phi = xcorr(H(i,:), Nc-1); Phi = Phi(Nc:end);
    
    tt = xcorr(Y(i,:), H(i,:), Nc-1); tt = tt(Nc:end);
    X(i,:) = levinson_forback(Phi, tt)';
end