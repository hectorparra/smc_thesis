# -*- coding: utf-8 -*-

from pylab import *
import glob
import json
from fusha import FushaBar


f0_path = '../MIR-1K/Wavfile'
all_ap = array([])

f0_files = glob.glob("%s/*.yin.f0.json" % f0_path)

with FushaBar(interval=1, bar_len=100) as bar:
  total = len(f0_files)
  i = 0
  
  for f0_file in f0_files:
    f0_hash = json.loads(open(f0_file).read())
    
    ap = array(f0_hash['ap0']) # gross aperiodicity measure
    ap[isnan(ap)] = 0
    all_ap = concatenate((all_ap, ap))
    
    i += 1
    bar.update(divide(i*100, total))

#hist(all_ap, 100)
hist(all_ap, 100, (0,0.3))

print 'finish!'
    