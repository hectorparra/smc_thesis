# -*- coding: utf-8 -*-

from __future__ import division # force non-truncating divisions
from pylab import *
import glob
import os
from fusha import FushaBar
import imp
__dir__ = os.path.dirname(__file__)
common = imp.load_source('common', os.path.join(__dir__, '../common/common.py'))

GROUND_TRUTH_DIR = os.path.join(__dir__, '../MIR-1K/PitchLabel')

def read_ground_truth(filename):
  ''' Return the ground truth array inferred from the soundfile name'''
  filename, _ = os.path.splitext(os.path.basename(filename))
  gt_path = os.path.join(GROUND_TRUTH_DIR, '%s.pv' % filename)
  return array(loadtxt(gt_path))

def mirex_evaluation(f0, gt):
  ''' Returns a hash with the results of the MIREX evaluation.
  Parameters:
      f0: one dimensional vector with detected fundamental frequency values in semitones
      gt: one dimensional vector with ground truth fundamental frequency values in semitones
  In contrast with MIREX, this evaluation is done directly in semitones, not Hertz.
  Also remeber that unvoiced frames have to be 0 or have a negative frequency.
  For more info see:
      http://www.music-ir.org/mirex/wiki/2012:Audio_Melody_Extraction
      http://www.music-ir.org/mirex/wiki/2005:Audio_Melody_Extraction_rresults
  '''
  
  # Transform to Numpy ndarray if necessary
  if not isinstance(f0, ndarray): f0 = array(f0)
  if not isinstance(gt, ndarray): gt = array(gt)    
  
  # Make size coincide
  if len(f0) > len(gt):
      f0 = f0[0:len(gt)]
  elif len(gt) > len(f0):
      raise Exception('F0 to be evaluated is smaller than ground truth')
      
  f0_voiced = f0 > 0
  f0_unvoiced = logical_not(f0_voiced)
  gt_voiced = gt > 0
  gt_unvoiced = logical_not(gt_voiced)
      
  TP = gt_voiced & f0_voiced # "true positives", frames where the voicing was correctly detected
  TN = gt_unvoiced & f0_unvoiced # "true negatives", frames which where actually unvoiced and are correctly detected unvoiced
  FP = gt_unvoiced & f0_voiced # "false positives", frames which where actually unvoiced but detected as voiced
  FN = gt_voiced & f0_unvoiced # "false negatives", frames which were actually pitched but detected as unpitched

  # frames that are really pitched and the pitch is correctly detected in a 1/2 semitone range
  abs_f0 = abs(f0)
  pitch_correct = gt_voiced & (abs_f0 <= gt + 0.5) & (abs_f0 >= gt - 0.5)
  
  # the same for chroma
  chroma_f0 = mod(abs_f0, 12)
  chroma_f0_1, chroma_f0_2 = chroma_f0 - 1, chroma_f0 + 1
  chroma_gt = mod(gt, 12)
  chroma_gt_up, chroma_gt_down = chroma_gt + 0.5, chroma_gt - 0.5         
  chroma_correct = gt_voiced & \
      (((chroma_f0 <= chroma_gt_up) & (chroma_f0 >= chroma_gt_down)) |
      ((chroma_f0_1 <= chroma_gt_up) & (chroma_f0_1 >= chroma_gt_down)) |
      ((chroma_f0_2 <= chroma_gt_up) & (chroma_f0_2 >= chroma_gt_down)))

  # TP = TPC + TPI, true positives with pitch correct and pitch incorrect
  TPC = TP & pitch_correct    
  TPCch = TP & chroma_correct # Ignoring octave errors: TP = TPCch + TPIch 
  
  # FN = FNC + FNI, false negatives with pitch correct and pitch incorrect
  FNC = FN & pitch_correct
  FNCch = FN & chroma_correct # Ignoring octave errors: FN = FNCch + FNIch   

  r = {} # hash with results to return
  r['TP'] = count_nonzero(TP)
  r['TN'] = count_nonzero(TN)
  r['FP'] = count_nonzero(FP)
  r['FN'] = count_nonzero(FN)
  
  r['TPC'] = count_nonzero(TPC)
  r['TPI'] = r['TP'] - r['TPC']
  r['TPCch'] = count_nonzero(TPCch)
  r['TPIch'] = r['TP'] - r['TPCch']
  
  r['FNC'] = count_nonzero(FNC)
  r['FNI'] = r['FN'] - r['FNC']
  r['FNCch'] = count_nonzero(FNCch)
  r['FNIch'] = r['FN'] - r['FNCch']    
  
  r['GU'] = r['TN'] + r['FP'] # Ground truth Unvoiced
  r['GV'] = r['TP'] + r['FN'] # Ground truth Voiced
  r['DU'] = r['TN'] + r['FN'] # Detected Unvoiced
  r['DV'] = r['FP'] + r['TP'] # Detected Voiced
  r['TO'] = len(gt) # Total number of frames
  
  r['Voicing Detection'] = r['TP'] / r['GV']
  r['Voicing False Alarm'] = r['FP'] / r['GU']
  
  r['Raw Pitch Accuracy'] = (r['TPC'] + r['FNC']) / r['GV'] # pitch accuracy on pitched frames ignoring voicing
  r['Raw Chroma Accuracy'] = (r['TPCch'] + r['FNCch']) / r['GV'] # chroma accuracy on pitched frames ignoring voicing
  r['Overall Accuracy'] = (r['TPC'] + r['TN']) / r['TO'] # proportion of frames with voicing and pitch right
  
  r['Voiced Pitch Accuracy'] = r['TPC'] / r['GV'] # proportion of truly-voiced frames with voicing and pitch right
  r['Voiced Chroma Accuracy'] = r['TPCch'] / r['GV'] # proportion of truly-voiced frames with voicing and chroma right
  r['Voicing Accuracy'] = (r['TP'] + r['TN']) / r['TO'] # overall voicing detection frame accuracy
  r['Overall Chroma Accuracy'] = (r['TPCch'] + r['TN']) / r['TO'] # proportion of frames with voicing and chroma right
  
  r['Precision'] = (r['TPC'] + r['TN']) / (r['TP'] + r['TN'] + r['FP'])
  r['Recall'] = (r['TPC'] + r['TN']) / (r['TP'] + r['TN'] + r['FN'])
  divisor = r['Precision'] + r['Recall']
  if divisor != 0:   
    r['F-measure'] = 2 * r['Precision'] * r['Recall'] / (r['Precision'] + r['Recall'])
  else:
    r['F-measure'] = 0
    
  r['Precision Chroma'] = (r['TPCch'] + r['TN']) / (r['TP'] + r['TN'] + r['FP'])
  r['Recall Chroma'] = (r['TPCch'] + r['TN']) / (r['TP'] + r['TN'] + r['FN'])
  divisor = r['Precision Chroma'] + r['Recall Chroma']
  if divisor != 0:
    r['F-measure Chroma'] = 2 * r['Precision Chroma'] * r['Recall Chroma'] / divisor   
  else:
    r['F-measure Chroma'] = 0
  
  return r
    
def evaluate_folder(f0_path, algorithm_name, f0_method=None):
  '''Output the evaluation of every file in the folder on another file'''
  print 'Evaluating %s for folder: %s' % (algorithm_name, f0_path)
  f0_files = glob.glob('%s/*.%s.f0.json' % (f0_path, algorithm_name))
  
  with FushaBar(interval=1, bar_len=100) as bar:
    i = 0  
    for f0_file in f0_files:
      # Load ground truth
      filename = common.get_filename(f0_file)[:-(4+len(algorithm_name))]
      gt_file = "%s/%s.pv" % (GROUND_TRUTH_DIR, filename)
      gt = loadtxt(gt_file)
  
      # Load f0 
      f0_hash = common.read_json(f0_file)
      f0 = array(f0_hash['f0'])
      
      if f0_method:
        f0 = f0_method(f0, f0_hash)
      
      # evaluate
      results = mirex_evaluation(f0, gt)
      
      # Save results
      eval_file = '%s/%s.%s.eval.json' % (f0_path, filename, algorithm_name)
      common.save_json(results, eval_file)
        
      i += 1
      bar.update(i*100//len(f0_files))
      
  print('Evaluation finished!')
       
def folder_results(folder, algorithm_name):
  '''Use the files from evalute_folder methods to compute the final results'''
  print 'Computing results for %s in folder: %s' % (algorithm_name, folder)
  eval_files = glob.glob("%s/*.%s.eval.json" % (folder, algorithm_name))
  folder_results_aux(folder, algorithm_name, eval_files)
  
def folder_results_micro(folder, algorithm_name):
  print 'Computing (micro)results for %s in folder: %s' % (algorithm_name, folder)
  eval_files = map(lambda f: "%s/%s.%s.eval.json" % (folder, f, algorithm_name), common.mir1k_micro())
  folder_results_aux(folder, algorithm_name, eval_files, 'results_micro')
  
def folder_results_aux(folder, algorithm_name, eval_files, results_filename = 'results'):
  keys = common.read_json(eval_files[0]).keys()
  keys = filter(lambda x: len(x) > 5, keys) # trick to choose relevant keys
  results = {'excerpt': {}, 'frame': {}}
  for key in keys: # initialize
    results['excerpt'][key] = 0
    results['frame'][key] = 0    
  
  with FushaBar(interval=1, bar_len=100) as bar:
    i = 0  
    total_frames = 0

    for eval_file in eval_files:    
      eval_hash = common.read_json(eval_file)
      total_frames += eval_hash['TO']
      for key in keys:
        value = eval_hash[key]
        results['excerpt'][key] += value / len(eval_files) # results per file
        results['frame'][key] += value * eval_hash['TO'] # results per frame
        
      i += 1
      bar.update(i*100//len(eval_files))  
      
    for key in keys: results['frame'][key] = results['frame'][key] / total_frames
      
  # Save results
  results_file = '%s/%s.%s.json' % (folder, results_filename, algorithm_name)
  common.save_json(results, results_file)
    
  print('Results finished!')  
  
def compute_results(evaluations):
  '''Does the same as folder_results but with already loaded in-memory hashes.
    This is useful for doing grid seaches without saving intermediate files'''
  total_frames = 0
  keys = filter(lambda x: len(x) > 5, evaluations[0].keys()) # trick to choose relevant keys
  results = {'excerpt': {}, 'frame': {}}
  for key in keys: # initialize
    results['excerpt'][key] = 0
    results['frame'][key] = 0    

  for eval_hash in evaluations:    
    total_frames += eval_hash['TO']
    for key in keys:
      value = eval_hash[key]
      results['excerpt'][key] += value / len(evaluations) # results per file
      results['frame'][key] += value * eval_hash['TO'] # results per frame  
      
  for key in keys: results['frame'][key] = results['frame'][key] / total_frames      
      
  return results
  
def merge_folder_results(folders, algorithm_name, folder_to_save_results, results_filename='results'):
  '''Merges all the result file in every folder by doing the mean'''
  keys = common.read_json('%s/%s.%s.json' % (folders[0], results_filename, algorithm_name))['excerpt'].keys()
  results = {'excerpt': {}, 'frame': {}}
  for key in keys: # initialize
    results['excerpt'][key] = []
    results['frame'][key] = []    
    
  for folder in folders:
    folder_results = common.read_json('%s/%s.%s.json' % (folder, results_filename, algorithm_name))
    for rkey in results.keys():
      for key in keys:
        results[rkey][key].append(folder_results[rkey][key])

  std_results = {'excerpt': {}, 'frame': {}}          
  for rkey in results.keys():
    for key in keys:
      std_results[rkey][key] = std(results[rkey][key])
      results[rkey][key] = mean(results[rkey][key])
  results['std'] = std_results
  
  save_file = "%s/%s.%s.json" % (folder_to_save_results, results_filename, algorithm_name)
  common.save_json(results, save_file)  
  
def merge_folder_results_micro(folders, algorithm_name, folder_to_save_results): 
  merge_folder_results(folders, algorithm_name, folder_to_save_results, 'results_micro')
  