function [] = Gam_Mag_ITD(inwavfile, outwavfile)

Spar = 0;    % Sparsity parameter
Nfilt = 40;  % Number of filters in a Gammatone filter-bank (Used for designing Gammatone filter)
analsize = 1024; % window length for processing (64 ms at 16 kHz)
I = 20;      % Number of iterations of ITD processing (used for initialization)
I_ITD = 5;   % Number of iterations of ITD algorithm

hp = fix(analsize/4); % window hop length is 1/4 the analsize
Nframes = fix(10/hp * 256); % Filter length is 10-frames long for hp=256 (16 ms), thus a total of 160 ms
fs = 16000;  % Assuming 16 kHz

%%% Obtain normalized Gammatone filer responses
% The values of gH and igH should be saved in a file and just recalled at
% this stage.
gH = abs(ComputeFilterResponse(Nfilt, analsize+2));
normH = sum(gH.^2, 1).^0.5;
gH = gH./ repmat(normH, size(gH,1),1);
igH = pinv(gH');
%%% 

% Read speech data
input = wavread(inwavfile);

% mean and scale normalization [not essential to algorithm]
input = input - mean(input);
input = norm_wav(input, 16);

% ITD processing
[output] = derev_ITD(input', analsize, Nframes, Spar, I, hp, gH, igH, I_ITD);

% Normalize output
output = output - mean(output);
output = norm_wav(output, 16);

% Write output to file
wavwrite(output, fs, outwavfile);