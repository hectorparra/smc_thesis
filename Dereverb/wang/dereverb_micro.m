% This script generates the dereverberated files from already
% reveberverated files
% This algorithm is so slow we only use the micro dataset
clear all;close all;clc
addpath('src');
addpath('jsonlab');

% folder to save full dereverberated files
dereverb_dir = '../../MIR-1K dereverb/wang_micro';
% folder to save first-stage dereverb, only de-coloration
inverse_dir = '../../MIR-1K dereverb/wang_micro_inv';

reverb_dir = '../../MIR-1K reverb';
room_dirs = dir(sprintf('%s/air_*', reverb_dir));
wav_files = loadjson(sprintf('../../MIR-1K/mir1k_micro.json'));

progressbar = waitbar(0,'Initializing...'); tic;

for i=1:length(room_dirs)
  room_name = room_dirs(i).name;
  input_room_dir = sprintf('%s/%s', reverb_dir, room_name);
  output_room_dir = sprintf('%s/%s', dereverb_dir, room_name);
  output_inv_room_dir = sprintf('%s/%s', inverse_dir, room_name);
  mkdir(output_room_dir);
  mkdir(output_inv_room_dir);
  
  for j=1:length(wav_files)
    input = sprintf('%s/%s.wav', input_room_dir, wav_files{j});
    output = sprintf('%s/%s.wav', output_room_dir, wav_files{j});
    output_inv = sprintf('%s/%s.wav', output_inv_room_dir, wav_files{j});
    
    rev = double(wavread(input, 'native')); % read wav
    inv  = inverse_filter_custom(rev); % inverse filter
    derev = spec_sub_derev(rev, 16000); % spectral subtraction  

    % output wav files
    wavwrite(inv/(max(abs(inv)))*0.95, 16000, output_inv);
    wavwrite(derev/(max(abs(derev)))*0.95, 16000, output);    
    
    % progress bar
    time = toc;
    perc = (j + (i-1)*length(wav_files)) / (length(room_dirs) * length(wav_files));
    trem = time/perc-time; %Calculate the time remaining
    hrs = floor(trem/3600);
    min = floor((trem-hrs*3600)/60);
    waitbar(perc, progressbar, ...
    sprintf('%0.1f%% %03.0f:%02.0f:%02.0f ETA', perc*100, hrs, min, rem(trem,60)));   
  end
  
end

close(progressbar);
disp('Dereverberation finished!')