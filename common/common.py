# -*- coding: utf-8 -*-

from __future__ import division # force non-truncating divisions
from pylab import *
import os, imp
import json

class NumpyAwareJSONEncoder(json.JSONEncoder):
  def default(self, obj):
    if isinstance(obj, ndarray) and obj.ndim == 1: return [x for x in obj]
    return json.JSONEncoder.default(self, obj)

def read_json(path):
  return json.loads(open(path).read())  
  
def save_json(data, path):
    with open(path, 'w') as file: 
      file.write(json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '), cls=NumpyAwareJSONEncoder))
     
     
def pitch2freq(pitch):
  '''Semitones to Hertz'''
  freq = 440 * 2 ** ((abs(pitch)-69)/12)
  negative_pitch = pitch < 0
  freq[negative_pitch] = -freq[negative_pitch]
  return freq

def freq2pitch(freq):
  '''Hertz to semitones'''
  pitch = 69 + 12 * log2(abs(freq)/440)  
  negative_freq = freq < 0
  pitch[negative_freq] = -pitch[negative_freq]
  pitch[freq == 0] = 0 # without this, it would return -inf for 0Hz values
  return pitch
  
def interpolate_pitch(new_time, time, pitch):
  '''Interpolates pitch.
  This function takes into account that unvoiced pitch is negative and 0 means no pitch detected'''
  
  # Interpolate using absolute pitch
  new_pitch = interp(new_time, time, abs(pitch))
  
  # Create array to treat 0s specially (we do not want interpolation for values beside 0)
  zeroed = pitch == 0
  if any(zeroed):
    zeros = empty_like(pitch)
    zeros[zeroed] = -1
    zeros[~zeroed] = 1  
    new_zeros = interp(new_time, time, zeros)
    closest2zero = new_zeros < 0 # True for values beside a zero and closer to it than a pitch
    new_pitch[closest2zero] = 0 # Copy zeros for values beside a 0 and closer to it
    
    closest2pitch = where(~closest2zero & (new_zeros != 1))[0] # indices for values beside a zero but closer to a pitch
    time_matrix = np.tile(time, (len(closest2pitch),1)).transpose() # every column is a copy of pitch
    new_pitch[closest2pitch] = pitch[argmin(abs(time_matrix - new_time[closest2pitch]), 0)] # Copy the closest (pitch) value
  
  # Create array with pitch sign (voiciness)
  voiciness = empty_like(pitch)
  voiced = pitch > 0 # voiced frames
  voiciness[voiced] = 1
  voiciness[~voiced] = -1  
  
  # Recover sign
  new_voiciness = interp(new_time, time, voiciness)
  new_pitch[new_voiciness < 0] *= -1 # mark as unvoiced
  
  return new_pitch
  
def get_filename(path, ext=False):
  '''Returns only the filename from a path.
      ext: includes or not the extensions'''
  filename = os.path.basename(path)
  if ~ext: filename, _ = os.path.splitext(filename)
  return filename
  
def mir1k():
  return read_json(os.path.dirname(__file__) + '/../MIR-1K/mir1k.json')  
  
def mir1k_mini():
  return read_json(os.path.dirname(__file__) + '/../MIR-1K/mir1k_mini.json')
  
def mir1k_micro():
  return read_json(os.path.dirname(__file__) + '/../MIR-1K/mir1k_micro.json')  
  
def relative_import(caller_path, path):
  '''Imports modules directly from source using the relative path of the file that uses the function
    That is a bad coding practice in Python, but is OK for this project'''
  return imp.load_source(get_filename(path), os.path.join(os.path.dirname(caller_path), path))
  