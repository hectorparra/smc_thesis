# -*- coding: utf-8 -*-

from pylab import *
import glob

files = glob.glob('*.val')

maximum = -inf
minimum = inf

for f in files:
    data = loadtxt(files[0])
    
    _max = max(data)
    _min = min(data)
    
    if maximum < _max:
        maximum = _max
        
    if minimum > _min:
        minimum = _min
        
print "Maximum: %s" % maximum # 7516.0
print "Minimum: %s" % minimum # -6127.0
    