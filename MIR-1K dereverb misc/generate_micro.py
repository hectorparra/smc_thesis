# -*- coding: utf-8 -*-

import imp, shutil, os, glob
common = imp.load_source('common', os.path.join(os.path.dirname(__file__), '../common/common.py'))

mir_reverb_folder = '../MIR-1K reverb'
reverb_folders = glob.glob('%s/air_*' % mir_reverb_folder)
wav_files = common.mir1k_micro()

for reverb_folder in reverb_folders:
  destination_folder = common.get_filename(reverb_folder)
  os.mkdir(destination_folder)
  for wav_file in wav_files:
    source = '%s/%s.wav' % (reverb_folder, wav_file)
    
    shutil.copy(source, destination_folder)
    
print "Copy finished!" 
  