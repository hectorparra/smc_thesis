# -*- coding: utf-8 -*-

from pylab import *
import glob, os
from fusha import FushaBar
from subprocess import call
import imp
common = imp.load_source('common', '../../common/common.py')

mir_path = '../../MIR-1K/Wavfile'
mir_reverb_folders = glob.glob('../../MIR-1K reverb/air_*')
mir_dereverb_folders = glob.glob('../../MIR-1K dereverb/**/air_*')
# folders to evaluate
eval_folders = []
eval_folders += [mir_path] 
eval_folders += mir_reverb_folders
eval_folders += mir_dereverb_folders

binary = "ToolsCMD_TOTEST_Nov2012.exe"
config_file = 'anl_twm.sco'

wav_files = []
for eval_folder in eval_folders:
  wav_files += glob.glob('%s/*.wav' % eval_folder)

with FushaBar(interval=1, bar_len=100) as bar:
  i = 0
  for wav_file in wav_files:
    filename = wav_file[:-4] # remove .wav extension
    csv_file = '%s.twm.f0.csv' % filename
    f0_file = '%s.twm.f0.json' % filename
    
    call([binary, '-f0-analysis',  config_file, wav_file, csv_file]) # estimate f0
    csv_data = loadtxt(csv_file) # load estimation 
     # notice how first data point is ommitted since MIR1K starts at 20ms and TWM at 0mss
    data = { 'time': csv_data[1:,0], 'f0': common.freq2pitch(csv_data[1:,1]) }
    
    common.save_json(data, f0_file)
    os.remove('%s.m' % filename) # get rid of useless matlab file
    os.remove(csv_file) # remove since we saved it on the json file   
    
    i += 1
    bar.update(i*100//len(wav_files))
    
print "Estimation finished!"    
    