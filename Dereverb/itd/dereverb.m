% This script generates the dereverberated files from already
% reveberverated files
clear all;close all;clc
addpath('src');

dereverb_dir = '../../MIR-1K dereverb/itd';

reverb_dir = '../../MIR-1K reverb';
room_dirs = dir(sprintf('%s/air_*', reverb_dir));

%%% Obtain normalized Gammatone filer responses
Nfilt = 40;  % Number of filters in a Gammatone filter-bank (Used for designing Gammatone filter)
analsize = 1024; % window length for processing (64 ms at 16 kHz)
gH = abs(ComputeFilterResponse(Nfilt, analsize+2));
normH = sum(gH.^2, 1).^0.5;
gH = gH./ repmat(normH, size(gH,1),1);
igH = pinv(gH');

progressbar = waitbar(0,'Initializing...'); tic;

for i=1:length(room_dirs)
  room_name = room_dirs(i).name;
  input_room_dir = sprintf('%s/%s', reverb_dir, room_name);
  output_room_dir = sprintf('%s/%s', dereverb_dir, room_name);
  mkdir(output_room_dir);
  
  wav_files = dir(sprintf('%s/*.wav', input_room_dir));
  for j=1:length(wav_files)
    input = sprintf('%s/%s', input_room_dir, wav_files(j).name);
    output = sprintf('%s/%s', output_room_dir, wav_files(j).name);
    Gam_Mag_ITD_custom(input, output, gH, igH, analsize);
    
    % progress bar
    time = toc;
    perc = (j + (i-1)*length(wav_files)) / (length(room_dirs) * length(wav_files));
    trem = time/perc-time; %Calculate the time remaining
    hrs = floor(trem/3600);
    min = floor((trem-hrs*3600)/60);
    waitbar(perc, progressbar, ...
    sprintf('%0.1f%% %03.0f:%02.0f:%02.0f ETA', perc*100, hrs, min, rem(trem,60)));   
  end
  
end

close(progressbar);
disp('Dereverberation finished!')
