import glob, os, shutil
from fusha import FushaBar

source_dir = 'J:/thesis/MIR-1K dereverb'
dest_dir = 'C:/Users/hector/Desktop/thesis/MIR-1K dereverb'

source_files = glob.glob('%s/**/**/*.wav.44' % source_dir)

with FushaBar(interval=1, bar_len=100) as bar:
    i = 0
    for source_file in source_files:
        dest_file = '%s%s' % (dest_dir, source_file[len(source_dir):])
        shutil.copy(source_file, dest_file)

        i += 1
        bar.update(i*100//len(source_files))

print 'Finished!!!'
    
