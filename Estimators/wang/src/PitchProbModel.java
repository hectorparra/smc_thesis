import java.io.*;
import java.util.*;

import casa.*;

public class PitchProbModel {
	private int numChannel, numFrame;
	private int maxDelay;
	private float[][][] acl, acb;
	private float[][] energy, cc;
	private int[][] chanSelMask;
	private LinkedList<Integer>[][] storedPeaks;
	private int highFreqThreshold = 800;
	private double[] a1, a2;
	private float[][] offPkLogProb1, offPkLogProb2, onePitch, frameLevelTwoPitches, frameLevelTwoPitchesRatios;
	private float[] zeroPitch, frameLevelOnePitch;
	private float frameLevelZeroPitch;
	public boolean isWideBand;
	public float[][] maxPP;
	
	private int[][] twoPitchLCounts;
	private int onePitchLCounts;

	private LinkedList<PitchStateSeq> oldTrellis, newTrellis;

	private float N_ROOT = 6.0f, P_C0 = -451.0f, P_C2 = -66.0f;
	private float FACTOR = 4.0f, BETA = 5.0f;
	private int ZERO_POINT = 200;

	private float logPDynm0to0, logPDynm0to1, logPDynm1to0, logPDynmPara;
	private float[][] logPDynm1to1;
	private float[][][] logPDynm2to1, logPDynm1to2;
	private float[] logPtchDynmOnly;
	private float[][] logPtchDynm2dOnly;

	private int[][] pitchTrack;


	private class PitchState{
		byte numP;
		short p1, p2;

		PitchState(int numP, int p1, int p2){
			this.numP = (byte) numP;
			this.p1 = (short) p1;
			this.p2 = (short) p2;
		}

		PitchState(PitchState oldState){
			this.numP = oldState.numP;
			this.p1 = oldState.p1;
			this.p2 = oldState.p2;
		}
	}

	private class PitchStateSeq implements Comparable<PitchStateSeq>{
		LinkedList<PitchState> seq;
		float prob;

		PitchStateSeq(){
			seq = new LinkedList<PitchState>();
			seq.add(new PitchState(0,0,0));
			prob = 0.0f;
		}

		PitchStateSeq(PitchStateSeq oldSeq){
			this.seq = new LinkedList<PitchState>();
			//PitchState pstate;
			for (Iterator<PitchState> iter = oldSeq.seq.iterator(); iter.hasNext(); ){
				//pstate = iter.next();
				this.seq.add(iter.next());
			}
			this.prob = oldSeq.prob;
		}

		public int compareTo(PitchStateSeq o) {
			return (o.prob < this.prob ? -1 : (o.prob == this.prob ? 0 : 1));
		}
	}

	private class PeakLocation implements Comparable<PeakLocation>{
		int r,c;
		float prob;

		PeakLocation(int r, int c, float prob){
			this.r = r;
			this.c = c;
			this.prob = prob;
		}

		public int compareTo(PeakLocation o) {
			return (o.prob < this.prob ? -1 : (o.prob == this.prob ? 0 : 1));
		}
	}

	public PitchProbModel(Correlogram corrgram, float[][] energy) {
		numChannel = corrgram.numChannel;
		numFrame = corrgram.numFrame;
		maxDelay = 200;
		acl = corrgram.acl;
		cc = corrgram.cc;
		if (corrgram.acb != null){
			acb = corrgram.acb;
		}
		storedPeaks = new LinkedList[numChannel][numFrame];
		this.energy = energy;
		maxPP = new float[2][numFrame];
	}

	public void ChannelSelection(Gammatone gt){
		chanSelMask = new int[numChannel][numFrame];
		for (int chan=0; chan<numChannel; chan++)
			if (gt.cFreq[chan] < highFreqThreshold)
				for (int tf = 0; tf < numFrame; tf++){
					storedPeaks[chan][tf] = new LinkedList<Integer>();
					double tmpMax = -1;
					for (int delay=2; delay<maxDelay; delay++)		// why 2
						if (acl[chan][tf][delay]>acl[chan][tf][delay-1] && acl[chan][tf][delay]>acl[chan][tf][delay+1] && tmpMax < acl[chan][tf][delay])
							tmpMax = acl[chan][tf][delay];
					if (tmpMax > 0.945)
						for (int delay = 32; delay < maxDelay; delay++)
							if (acl[chan][tf][delay]>acl[chan][tf][delay-1] && acl[chan][tf][delay]>acl[chan][tf][delay+1] && acl[chan][tf][delay]>-10){
								storedPeaks[chan][tf].add(new Integer(delay));
								chanSelMask[chan][tf]++;
							}		
				}
		/*else
				for (int tf = 0; tf < numFrame; tf++){
					//if (chan==80 && tf==41)
					//	System.out.println("a\n");
					storedPeaks[chan][tf] = new LinkedList<Integer>();
					int tmpMaxDelay, tmpMaxDelay2;
					double tmpMax2;
					boolean flag = false, flag1 = false;
					for (int delay = 32+1; delay < maxDelay-1; delay++)
						if (acl[chan][tf][delay]>acl[chan][tf][delay-1] && acl[chan][tf][delay]>acl[chan][tf][delay+1] && acl[chan][tf][delay]>-10){
							flag = true;
							tmpMaxDelay = delay;
							tmpMax2 = -1;
							tmpMaxDelay2 = tmpMaxDelay;
							for (int delay2 = tmpMaxDelay-10; delay2 <= tmpMaxDelay + 10; delay2++)
								if (acb[chan][tf][delay2]>tmpMax2){
									tmpMax2 = acb[chan][tf][delay2];
									tmpMaxDelay2 = delay2;
								}
							if (Math.abs(tmpMaxDelay2-tmpMaxDelay)>2){
								flag1 = true;
								break;
							}
						}
					// when selected
					if (flag && ! flag1){
						boolean validPeak;
						for (int delay = 32; delay < maxDelay; delay++)
							if (acl[chan][tf][delay]>acl[chan][tf][delay-1] && acl[chan][tf][delay]>acl[chan][tf][delay+1] && acl[chan][tf][delay]>0.2){
								validPeak = false;
								for (int delay2 = 2*delay-5; delay2<=LocalMathMin(398,2*delay+5); delay2++)
									if (acl[chan][tf][delay2]>acl[chan][tf][delay2-1] && acl[chan][tf][delay2]>acl[chan][tf][delay2+1] && acl[chan][tf][delay2]>0.2){
										validPeak = true;
										break;
									}
								if (validPeak){
									storedPeaks[chan][tf].add(new Integer(delay));
									chanSelMask[chan][tf]++;
								}
							}
						if (chanSelMask[chan][tf]>0 && acl[chan][tf][storedPeaks[chan][tf].peek().intValue()]>0.6){
							// remove subsequent peaks
							int firstPeak = storedPeaks[chan][tf].peek().intValue();
							storedPeaks[chan][tf].clear();
							storedPeaks[chan][tf].add(new Integer(firstPeak));
							chanSelMask[chan][tf]=1;
						}
					}
				}*/
	}

	public void PitchModeling(){
		//BuildPitchModels(new Gammatone());
		//acl = null;
		//ZeroPitchHypothesis();
		//OnePitchHypothesis();
		InitPitchDynm();
		//TwoPitchHypothesis(5);
		/*try{
			  FileWriter w = new FileWriter("prob0.a", false);
		      for (int tf = 0; tf < numFrame; tf++)
		          w.write(zeroPitch[tf] + " ");

		      w.flush();
		      w.close();

		      w = new FileWriter("prob1.a", false);
		      for (int tf = 0; tf < numFrame; tf++) {
		        for (int delay = 0; delay < maxDelay; delay++)
		          w.write(onePitch[tf][delay] + " ");
		        w.write("\n");
		      }

		      w.flush();
		      w.close();
		}catch (IOException e) {
			      e.printStackTrace();
			      }*/
	}

	private void InitPitchDynm(){
		float p0To1, p1To0, p1To2, p2To1, mean, lamda;
		p0To1 = 0.1f; //0.0750f;
		p1To0 = 0.1f; //0.0079f;
		p1To2 = 0.02f; //0.0184f;
		p2To1 = 0.03f; //0.0323f;
		mean = 0.4f;
		lamda = 2.4f;

		double tmp = 0.0;
		logPtchDynmOnly = new float[ZERO_POINT * 2];
		double[] ptchDynmOnly = new double[ZERO_POINT * 2];
		for (int j = 0; j < ZERO_POINT * 2; j++){
			ptchDynmOnly[j] = Math.exp(-Math.abs((double)(j - ZERO_POINT) - mean) / lamda);
			tmp += ptchDynmOnly[j];
		}
		for (int j = 0; j < ZERO_POINT * 2; j++){
			ptchDynmOnly[j] /= tmp;
			logPtchDynmOnly[j] = (float) Math.log(ptchDynmOnly[j]);
		}

		tmp = 0.0f;
		logPtchDynm2dOnly = new float[ZERO_POINT * 2][ZERO_POINT * 2];
		double[][] ptchDynm2dOnly = new double[ZERO_POINT * 2][ZERO_POINT * 2];
		for (int i = 0; i < ZERO_POINT * 2; i++)
			for (int j = 0; j < ZERO_POINT * 2; j++){
				ptchDynm2dOnly[i][j] = ptchDynmOnly[i]*ptchDynmOnly[j];
				tmp += ptchDynm2dOnly[i][j];
			}
		for (int i = 0; i < ZERO_POINT * 2; i++)
			for (int j = 0; j < ZERO_POINT * 2; j++){
				ptchDynm2dOnly[i][j] /= tmp;
				logPtchDynm2dOnly[i][j] = (float) Math.log(ptchDynm2dOnly[i][j]);
			}

		// 0 to *
		logPDynm0to0 = (float) Math.log(1-p0To1);
		logPDynm0to1 = (float) Math.log(p0To1/(maxDelay-32));

		// 1 to *
		logPDynm1to0 = (float) Math.log(p1To0);

		logPDynm1to1 = new float[maxDelay][maxDelay];
		for (int i = 32; i < maxDelay; i++)
			for (int j = 32; j < maxDelay; j++)
				logPDynm1to1[i][j] = (float) Math.log(ptchDynmOnly[ZERO_POINT + (j - i)]*(1 - p1To0 - p1To2));

		logPDynm1to2 = new float[maxDelay][maxDelay][maxDelay];
		for (int i = 32; i < maxDelay; i++)
			for (int j = 32; j < maxDelay; j++)
				for (int k = 32; k < maxDelay; k++)
					logPDynm1to2[i][j][k] = (float) Math.log(ptchDynmOnly[ZERO_POINT + (j - i)] * p1To2 / (maxDelay - 32));

		// 2 to *
		logPDynm2to1 = new float[maxDelay][maxDelay][maxDelay];
		for (int i = 32; i < maxDelay; i++)
			for (int j = 32; j < maxDelay; j++)
				for (int k = 32; k < maxDelay; k++)
					logPDynm2to1[i][j][k] = (float) Math.log(LocalMathMax(ptchDynmOnly[ZERO_POINT + (k - i)], ptchDynmOnly[ZERO_POINT + (k - j)]) * p2To1);	

		logPDynmPara = (float) Math.log(1 - p2To1);
	}

	private void InitPitchDynm2(){
		float p0To1, p1To0, p1To2, p2To1, mean, lamda;
		p0To1 = 0.1f; //0.0750f;
		p1To0 = 0.1f; //0.0079f;
		p1To2 = 0.02f; //0.0184f;
		p2To1 = 0.03f; //0.0323f;
		mean = 0.4f;
		lamda = 2.4f;

		double tmp = 0.0;
		logPtchDynmOnly = new float[ZERO_POINT * 2];
		double[] ptchDynmOnly = new double[ZERO_POINT * 2];
		for (int j = 0; j < ZERO_POINT * 2; j++){
			ptchDynmOnly[j] = Math.exp(-Math.abs((double)(j - ZERO_POINT) - mean) / lamda);
			tmp += ptchDynmOnly[j];
		}
		for (int j = 0; j < ZERO_POINT * 2; j++){
			ptchDynmOnly[j] /= tmp;
			logPtchDynmOnly[j] = (float) Math.log(ptchDynmOnly[j]);
		}

		tmp = 0.0f;
		logPtchDynm2dOnly = new float[ZERO_POINT * 2][ZERO_POINT * 2];
		double[][] ptchDynm2dOnly = new double[ZERO_POINT * 2][ZERO_POINT * 2];
		for (int i = 0; i < ZERO_POINT * 2; i++)
			for (int j = 0; j < ZERO_POINT * 2; j++){
				ptchDynm2dOnly[i][j] = ptchDynmOnly[i]*ptchDynmOnly[j];
				tmp += ptchDynm2dOnly[i][j];
			}
		for (int i = 0; i < ZERO_POINT * 2; i++)
			for (int j = 0; j < ZERO_POINT * 2; j++){
				ptchDynm2dOnly[i][j] /= tmp;
				logPtchDynm2dOnly[i][j] = (float) Math.log(ptchDynm2dOnly[i][j]);
			}

		// 0 to *
		logPDynm0to0 = (float) Math.log(1-p0To1);
		logPDynm0to1 = (float) Math.log(p0To1/(maxDelay-32));

		// 1 to *
		logPDynm1to0 = (float) Math.log(p1To0);

		logPDynm1to1 = new float[maxDelay][maxDelay];
		for (int i = 32; i < maxDelay; i++)
			for (int j = 32; j < maxDelay; j++)
				logPDynm1to1[i][j] = (float) Math.log(ptchDynmOnly[ZERO_POINT + (j - i)]*(1 - p1To0 - p1To2));

		logPDynm1to2 = new float[maxDelay][maxDelay][maxDelay];
		for (int i = 32; i < maxDelay; i++)
			for (int j = 32; j < maxDelay; j++)
				for (int k = 32; k < maxDelay; k++)
					logPDynm1to2[i][j][k] = (float) Math.log(ptchDynmOnly[ZERO_POINT + (j - i)] * p1To2 / (maxDelay - 32));

		// 2 to *
		logPDynm2to1 = new float[maxDelay][maxDelay][maxDelay];
		for (int i = 32; i < maxDelay; i++)
			for (int j = 32; j < maxDelay; j++)
				for (int k = 32; k < maxDelay; k++)
					logPDynm2to1[i][j][k] = (float) Math.log(LocalMathMax(ptchDynmOnly[ZERO_POINT + (k - i)], ptchDynmOnly[ZERO_POINT + (k - j)]) * p2To1);	

		logPDynmPara = (float) Math.log(1 - p2To1);
	}

	public void PitchTrackingViterbi(){
		this.InitPitchDynm2();
		oldTrellis = new LinkedList<PitchStateSeq>();
		boolean[][] check = new boolean[maxDelay][maxDelay];
		PitchStateSeq tmpPSeq, maxSeq, candSeq;
		float tmpMax, transProb;
		boolean flag;

		int interimChange = 30;

		float transCoeff = 200; //Float.POSITIVE_INFINITY;

		float negInf = Float.NEGATIVE_INFINITY;

		oldTrellis.add(new PitchStateSeq());

		System.out.print("Viterbi searching...");
		for (int tf = 1; tf < numFrame; tf++){

			System.out.print(Math.round( (float) tf / numFrame * 100) + "%");

			newTrellis = new LinkedList<PitchStateSeq>();

			//TwoPitchHypothesis(tf);
			AllPitchHypothesis(tf);

			//current is 0
			tmpMax = negInf;
			maxSeq = null;
			for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
				tmpPSeq = iter.next();
				switch (tmpPSeq.seq.getLast().numP){
				case 0:
					transProb = tmpPSeq.prob + logPDynm0to0/transCoeff;
					if (tmpMax < transProb){
						tmpMax = transProb;
						maxSeq = tmpPSeq;
					}
					break;
				case 1:
					transProb = tmpPSeq.prob + logPDynm1to0/transCoeff;
					if (tmpMax < transProb){
						tmpMax = transProb;
						maxSeq = tmpPSeq;
					}
					break;
				}
			}
			if (maxSeq != null){
				candSeq = new PitchStateSeq(maxSeq);
				candSeq.seq.add(new PitchState(0,0,0));
				candSeq.prob = tmpMax + this.frameLevelZeroPitch;
				newTrellis.add(candSeq);
			}

			//current is 1
			for (int delay = 32 + 1; delay < maxDelay - 1; delay++){
				tmpMax = negInf;
				maxSeq = null;
				for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
					tmpPSeq = iter.next();
					switch (tmpPSeq.seq.getLast().numP){
					case 0:
						transProb = tmpPSeq.prob + logPDynm0to1/transCoeff;
						if (tmpMax < transProb){
							tmpMax = transProb;
							maxSeq = tmpPSeq;
						}
						break;
					case 1:
						if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay) < interimChange) {
							transProb = tmpPSeq.prob + logPDynm1to1[tmpPSeq.seq.getLast().p1][delay]/transCoeff;
							if (tmpMax < transProb) {
								tmpMax = transProb;
								maxSeq = tmpPSeq;
							}
						}
						break;
					case 2: 
						if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay) < interimChange || LocalMathAbs(tmpPSeq.seq.getLast().p2 - delay) < interimChange){
							transProb = tmpPSeq.prob + logPDynm2to1[tmpPSeq.seq.getLast().p1][tmpPSeq.seq.getLast().p2][delay]/transCoeff;
							if (tmpMax < transProb) {
								tmpMax = transProb;
								maxSeq = tmpPSeq;
							}
						}
						break;
					}
				}
				if (maxSeq != null){
					candSeq = new PitchStateSeq(maxSeq);
					candSeq.seq.add(new PitchState(1,delay,0));
					candSeq.prob = tmpMax + frameLevelOnePitch[delay];
					newTrellis.add(candSeq);
				}
			}

			//current is 2
			LinkedList<PeakLocation> peakList = new LinkedList<PeakLocation>();
			for (int delay1 = 32 + 1; delay1 < maxDelay - 2; delay1 ++)
				for (int delay2 = 32 + 1; delay2 < maxDelay - 2; delay2 ++)
					if (frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2+1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1][delay2+1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2+1])

						peakList.add(new PeakLocation(delay1,delay2,frameLevelTwoPitches[delay1][delay2]));

			Collections.sort(peakList);

			for (int idx = 0; idx < Math.min(peakList.size(), 100); idx++){
				PeakLocation pL = peakList.get(idx);

				check[pL.r][pL.c] = true;

				check[pL.r-1][pL.c-1] = true; check[pL.r][pL.c-1] = true;
				check[pL.r+1][pL.c-1] = true; check[pL.r-1][pL.c] = true;
				check[pL.r+1][pL.c] = true; check[pL.r-1][pL.c+1] = true;
				check[pL.r][pL.c+1] = true; check[pL.r+1][pL.c+1] = true;

				check[pL.r][pL.c+2] = true; check[pL.r][pL.c-2] = true; 
				check[pL.r+1][pL.c+2] = true; check[pL.r+1][pL.c-2] = true; 
				check[pL.r-1][pL.c+2] = true; check[pL.r-1][pL.c-2] = true;
				check[pL.r-2][pL.c] = true; check[pL.r+2][pL.c] = true; 
				check[pL.r-2][pL.c-1] = true; check[pL.r+2][pL.c-1] = true;
				check[pL.r-2][pL.c+1] = true; check[pL.r+2][pL.c+1] = true;

				check[pL.r+2][pL.c+2] = true; check[pL.r+2][pL.c-2] = true;
				check[pL.r-2][pL.c+2] = true; check[pL.r-2][pL.c-2] = true;
			}

			for (int delay1 = 32 + 1; delay1 < maxDelay - 2; delay1 ++)
				for (int delay2 = 32 + 1; delay2 < maxDelay - 2; delay2 ++)
					if (check[delay1][delay2]){
						check[delay1][delay2] = false;
						tmpMax = negInf;
						maxSeq = null;
						flag = false;
						for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
							tmpPSeq = iter.next();
							switch (tmpPSeq.seq.getLast().numP){
							/*case 0:
								transProb = tmpPSeq.prob;
								if (tmpMax < transProb){
									tmpMax = transProb;
									maxSeq = tmpPSeq;
									flag = true;
								}
								break;*/
							case 1:
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay1) < interimChange) {
									transProb = tmpPSeq.prob + logPDynm1to2[tmpPSeq.seq.getLast().p1][delay1][delay2]/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay2) < interimChange) {
									transProb = tmpPSeq.prob + logPDynm1to2[tmpPSeq.seq.getLast().p1][delay2][delay1]/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								break;
							case 2: 
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay1) < interimChange && LocalMathAbs(tmpPSeq.seq.getLast().p2 - delay2) < interimChange){
									transProb = tmpPSeq.prob + logPtchDynm2dOnly[ZERO_POINT + LocalMathAbs(tmpPSeq.seq.getLast().p1-delay1)][ZERO_POINT + LocalMathAbs(tmpPSeq.seq.getLast().p2-delay2)]/transCoeff + logPDynmPara/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								break;
							}
						}

						if (flag){
							candSeq = new PitchStateSeq(maxSeq);
							candSeq.seq.add(new PitchState(2,delay1,delay2));
							candSeq.prob = tmpMax + frameLevelTwoPitches[delay1][delay2];
							newTrellis.add(candSeq);
						}			
					}

			//prune
			if (newTrellis.size() > 200){
				Collections.sort(newTrellis);
				for (int i = 0; i < newTrellis.size() - 200; i++)
					newTrellis.removeLast();
			}

			//update
			oldTrellis = newTrellis;
			newTrellis = null;

			if (Math.round( (float) tf / numFrame * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");

		//output sequence
		pitchTrack = new int[2][numFrame];
		Collections.sort(oldTrellis);
		candSeq = oldTrellis.getFirst();
		int tIdx = 0;
		PitchState pstate;
		for (Iterator<PitchState> iter = candSeq.seq.iterator(); iter.hasNext(); ){
			pstate = iter.next();
			pitchTrack[0][tIdx] = pstate.p1;
			pitchTrack[1][tIdx] = pstate.p2;
			tIdx++;
		}
	}

	public void PitchTrackingViterbipass1(){
		this.InitPitchDynm();
		oldTrellis = new LinkedList<PitchStateSeq>();
		boolean[][] check = new boolean[maxDelay][maxDelay];
		PitchStateSeq tmpPSeq, maxSeq, candSeq;
		float tmpMax, transProb;
		boolean flag;

		int interimChange = 30;

		float transCoeff = 200; //Float.POSITIVE_INFINITY;

		float negInf = Float.NEGATIVE_INFINITY;

		oldTrellis.add(new PitchStateSeq());

		System.out.print("Viterbi searching...");
		for (int tf = 1; tf < numFrame; tf++){

			System.out.print(Math.round( (float) tf / numFrame * 100) + "%");

			newTrellis = new LinkedList<PitchStateSeq>();

			//TwoPitchHypothesis(tf);
			AllPitchHypothesis(tf);

			//current is 0
			tmpMax = negInf;
			maxSeq = null;
			for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
				tmpPSeq = iter.next();
				switch (tmpPSeq.seq.getLast().numP){
				case 0:
					transProb = tmpPSeq.prob + logPDynm0to0/transCoeff;
					if (tmpMax < transProb){
						tmpMax = transProb;
						maxSeq = tmpPSeq;
					}
					break;
				case 1:
					transProb = tmpPSeq.prob + logPDynm1to0/transCoeff;
					if (tmpMax < transProb){
						tmpMax = transProb;
						maxSeq = tmpPSeq;
					}
					break;
				}
			}
			if (maxSeq != null){
				candSeq = new PitchStateSeq(maxSeq);
				candSeq.seq.add(new PitchState(0,0,0));
				candSeq.prob = tmpMax + this.frameLevelZeroPitch;
				newTrellis.add(candSeq);
			}

			//current is 1
			for (int delay = 32 + 1; delay < maxDelay - 1; delay++){
				tmpMax = negInf;
				maxSeq = null;
				for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
					tmpPSeq = iter.next();
					switch (tmpPSeq.seq.getLast().numP){
					case 0:
						transProb = tmpPSeq.prob + logPDynm0to1/transCoeff;
						if (tmpMax < transProb){
							tmpMax = transProb;
							maxSeq = tmpPSeq;
						}
						break;
					case 1:
						if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay) < interimChange) {
							transProb = tmpPSeq.prob + logPDynm1to1[tmpPSeq.seq.getLast().p1][delay]/transCoeff;
							if (tmpMax < transProb) {
								tmpMax = transProb;
								maxSeq = tmpPSeq;
							}
						}
						break;
					case 2: 
						if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay) < interimChange || LocalMathAbs(tmpPSeq.seq.getLast().p2 - delay) < interimChange){
							transProb = tmpPSeq.prob + logPDynm2to1[tmpPSeq.seq.getLast().p1][tmpPSeq.seq.getLast().p2][delay]/transCoeff;
							if (tmpMax < transProb) {
								tmpMax = transProb;
								maxSeq = tmpPSeq;
							}
						}
						break;
					}
				}
				if (maxSeq != null){
					candSeq = new PitchStateSeq(maxSeq);
					candSeq.seq.add(new PitchState(1,delay,0));
					candSeq.prob = tmpMax + frameLevelOnePitch[delay];
					newTrellis.add(candSeq);
				}
			}

			//current is 2
			LinkedList<PeakLocation> peakList = new LinkedList<PeakLocation>();
			for (int delay1 = 32 + 1; delay1 < maxDelay - 2; delay1 ++)
				for (int delay2 = 32 + 1; delay2 < maxDelay - 2; delay2 ++)
					if (frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2+1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1][delay2+1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2+1])

						peakList.add(new PeakLocation(delay1,delay2,frameLevelTwoPitches[delay1][delay2]));

			Collections.sort(peakList);

			for (int idx = 0; idx < Math.min(peakList.size(), 100); idx++){
				PeakLocation pL = peakList.get(idx);

				check[pL.r][pL.c] = true;

				check[pL.r-1][pL.c-1] = true; check[pL.r][pL.c-1] = true;
				check[pL.r+1][pL.c-1] = true; check[pL.r-1][pL.c] = true;
				check[pL.r+1][pL.c] = true; check[pL.r-1][pL.c+1] = true;
				check[pL.r][pL.c+1] = true; check[pL.r+1][pL.c+1] = true;

				check[pL.r][pL.c+2] = true; check[pL.r][pL.c-2] = true; 
				check[pL.r+1][pL.c+2] = true; check[pL.r+1][pL.c-2] = true; 
				check[pL.r-1][pL.c+2] = true; check[pL.r-1][pL.c-2] = true;
				check[pL.r-2][pL.c] = true; check[pL.r+2][pL.c] = true; 
				check[pL.r-2][pL.c-1] = true; check[pL.r+2][pL.c-1] = true;
				check[pL.r-2][pL.c+1] = true; check[pL.r+2][pL.c+1] = true;

				check[pL.r+2][pL.c+2] = true; check[pL.r+2][pL.c-2] = true;
				check[pL.r-2][pL.c+2] = true; check[pL.r-2][pL.c-2] = true;
			}

			for (int delay1 = 32 + 1; delay1 < maxDelay - 2; delay1 ++)
				for (int delay2 = 32 + 1; delay2 < maxDelay - 2; delay2 ++)
					if (check[delay1][delay2]){
						check[delay1][delay2] = false;
						tmpMax = negInf;
						maxSeq = null;
						flag = false;
						for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
							tmpPSeq = iter.next();
							switch (tmpPSeq.seq.getLast().numP){
							/*case 0:
								transProb = tmpPSeq.prob;
								if (tmpMax < transProb){
									tmpMax = transProb;
									maxSeq = tmpPSeq;
									flag = true;
								}
								break;*/
							case 1:
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay1) < interimChange) {
									transProb = tmpPSeq.prob + logPDynm1to2[tmpPSeq.seq.getLast().p1][delay1][delay2]/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay2) < interimChange) {
									transProb = tmpPSeq.prob + logPDynm1to2[tmpPSeq.seq.getLast().p1][delay2][delay1]/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								break;
							case 2: 
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay1) < interimChange && LocalMathAbs(tmpPSeq.seq.getLast().p2 - delay2) < interimChange){
									transProb = tmpPSeq.prob + logPtchDynm2dOnly[ZERO_POINT + LocalMathAbs(tmpPSeq.seq.getLast().p1-delay1)][ZERO_POINT + LocalMathAbs(tmpPSeq.seq.getLast().p2-delay2)]/transCoeff + logPDynmPara/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								break;
							}
						}

						if (flag){
							candSeq = new PitchStateSeq(maxSeq);
							candSeq.seq.add(new PitchState(2,delay1,delay2));
							candSeq.prob = tmpMax + frameLevelTwoPitches[delay1][delay2];
							newTrellis.add(candSeq);
						}			
					}

			//prune
			if (newTrellis.size() > 200){
				Collections.sort(newTrellis);
				for (int i = 0; i < newTrellis.size() - 200; i++)
					newTrellis.removeLast();
			}

			//update
			oldTrellis = newTrellis;
			newTrellis = null;

			if (Math.round( (float) tf / numFrame * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");

		//output sequence
		pitchTrack = new int[2][numFrame];
		Collections.sort(oldTrellis);
		candSeq = oldTrellis.getFirst();
		int tIdx = 0, count2P = 0;
		PitchState pstate;
		for (Iterator<PitchState> iter = candSeq.seq.iterator(); iter.hasNext(); ){
			pstate = iter.next();
			pitchTrack[0][tIdx] = pstate.p1;
			pitchTrack[1][tIdx] = pstate.p2;
			tIdx++;
			if (pstate.numP==2)
				count2P++;
		}
		if (count2P > this.numFrame * 0.4){
			this.isWideBand = false;
			System.out.println("isWideBand = false(" + (float)count2P/this.numFrame + ")");
		}else{
			this.isWideBand = true;
			System.out.println("isWideBand = true(" + (float)count2P/this.numFrame + ")");
		}
	}

	public void PitchTrackingViterbipass2(){
		this.InitPitchDynm2();
		oldTrellis = new LinkedList<PitchStateSeq>();
		boolean[][] check = new boolean[maxDelay][maxDelay];
		PitchStateSeq tmpPSeq, maxSeq, candSeq;
		float tmpMax, transProb;
		boolean flag;

		int interimChange = 20;//100;

		float transCoeff = 50;//100;//Float.POSITIVE_INFINITY; //50;

		float negInf = Float.NEGATIVE_INFINITY;
		
		//System.out.print("Checking WB...");
		//this.checkWB();
		//System.out.print("is " + this.isWideBand + "\n");

		oldTrellis.add(new PitchStateSeq());

		System.out.print("Viterbi searching...");
		for (int tf = 1; tf < numFrame; tf++){

			System.out.print(Math.round( (float) tf / numFrame * 100) + "%");

			newTrellis = new LinkedList<PitchStateSeq>();

			//if (this.isWideBand)
			//	this.WBAllPitchHypothesis2(tf);
			//else
				this.TTAllPitchHypothesis2(tf);

			//current is 0
			tmpMax = negInf;
			maxSeq = null;
			for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
				tmpPSeq = iter.next();
				switch (tmpPSeq.seq.getLast().numP){
				case 0:
					transProb = tmpPSeq.prob + logPDynm0to0/transCoeff;
					if (tmpMax < transProb){
						tmpMax = transProb;
						maxSeq = tmpPSeq;
					}
					break;
				case 1:
					transProb = tmpPSeq.prob + logPDynm1to0/transCoeff;
					if (tmpMax < transProb){
						tmpMax = transProb;
						maxSeq = tmpPSeq;
					}
					break;
				/*case 2:
					transProb = tmpPSeq.prob;
					if (tmpMax < transProb){
						tmpMax = transProb;
						maxSeq = tmpPSeq;
						flag = true;
					}
					break;*/
				}
			}
			if (maxSeq != null){
				candSeq = new PitchStateSeq(maxSeq);
				candSeq.seq.add(new PitchState(0,0,0));
				candSeq.prob = tmpMax + this.frameLevelZeroPitch;
				newTrellis.add(candSeq);
			}

			//current is 1
			for (int delay = 32 + 1; delay < maxDelay - 1; delay++){
				tmpMax = negInf;
				maxSeq = null;
				for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
					tmpPSeq = iter.next();
					switch (tmpPSeq.seq.getLast().numP){
					case 0:
						transProb = tmpPSeq.prob + logPDynm0to1/transCoeff;
						if (tmpMax < transProb){
							tmpMax = transProb;
							maxSeq = tmpPSeq;
						}
						break;
					case 1:
						if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay) < interimChange) {
							transProb = tmpPSeq.prob + logPDynm1to1[tmpPSeq.seq.getLast().p1][delay]/transCoeff;
							if (tmpMax < transProb) {
								tmpMax = transProb;
								maxSeq = tmpPSeq;
							}
						}
						break;
					case 2: 
						if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay) < interimChange || LocalMathAbs(tmpPSeq.seq.getLast().p2 - delay) < interimChange){
							transProb = tmpPSeq.prob + logPDynm2to1[tmpPSeq.seq.getLast().p1][tmpPSeq.seq.getLast().p2][delay]/transCoeff;
							if (tmpMax < transProb) {
								tmpMax = transProb;
								maxSeq = tmpPSeq;
							}
						}
						break;
					}
				}
				if (maxSeq != null){
					candSeq = new PitchStateSeq(maxSeq);
					candSeq.seq.add(new PitchState(1,delay,0));
					candSeq.prob = tmpMax + frameLevelOnePitch[delay];
					newTrellis.add(candSeq);
				}
			}

			//current is 2
			LinkedList<PeakLocation> peakList = new LinkedList<PeakLocation>();
			for (int delay1 = 32 + 1; delay1 < maxDelay - 2; delay1 ++)
				for (int delay2 = 32 + 1; delay2 < maxDelay - 2; delay2 ++)
					if (frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2-1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2+1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1][delay2+1] &&
							frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2+1])

						peakList.add(new PeakLocation(delay1,delay2,frameLevelTwoPitches[delay1][delay2]));

			Collections.sort(peakList);

			for (int idx = 0; idx < Math.min(peakList.size(), 50); idx++){
				PeakLocation pL = peakList.get(idx);
				
				/*if (this.frameLevelTwoPitches[pL.r][pL.c]>Math.log(0.8) ||
				(this.frameLevelTwoPitches[pL.r][pL.c]>Math.log(0.7) && 
				(this.frameLevelTwoPitches[pL.r*2][pL.c]>Math.log(0.7) ||
				this.frameLevelTwoPitches[pL.r][pL.c*2]>Math.log(0.7)))){*/
				
				check[pL.r][pL.c] = true;

				check[pL.r-1][pL.c-1] = true; check[pL.r][pL.c-1] = true;
				check[pL.r+1][pL.c-1] = true; check[pL.r-1][pL.c] = true;
				check[pL.r+1][pL.c] = true; check[pL.r-1][pL.c+1] = true;
				check[pL.r][pL.c+1] = true; check[pL.r+1][pL.c+1] = true;

				check[pL.r][pL.c+2] = true; check[pL.r][pL.c-2] = true; 
				check[pL.r+1][pL.c+2] = true; check[pL.r+1][pL.c-2] = true; 
				check[pL.r-1][pL.c+2] = true; check[pL.r-1][pL.c-2] = true;
				check[pL.r-2][pL.c] = true; check[pL.r+2][pL.c] = true; 
				check[pL.r-2][pL.c-1] = true; check[pL.r+2][pL.c-1] = true;
				check[pL.r-2][pL.c+1] = true; check[pL.r+2][pL.c+1] = true;

				check[pL.r+2][pL.c+2] = true; check[pL.r+2][pL.c-2] = true;
				check[pL.r-2][pL.c+2] = true; check[pL.r-2][pL.c-2] = true;//}
			}

			for (int delay1 = 32 + 1; delay1 < maxDelay - 2; delay1 ++)
				for (int delay2 = 32 + 1; delay2 < maxDelay - 2; delay2 ++)
					if (check[delay1][delay2]){
						check[delay1][delay2] = false;
						tmpMax = negInf;
						maxSeq = null;
						flag = false;
						for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
							tmpPSeq = iter.next();
							switch (tmpPSeq.seq.getLast().numP){
							/*case 0:
								transProb = tmpPSeq.prob;
								if (tmpMax < transProb){
									tmpMax = transProb;
									maxSeq = tmpPSeq;
									flag = true;
								}
								break;*/
							case 1:
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay1) < interimChange) {
									transProb = tmpPSeq.prob + logPDynm1to2[tmpPSeq.seq.getLast().p1][delay1][delay2]/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay2) < interimChange) {
									transProb = tmpPSeq.prob + logPDynm1to2[tmpPSeq.seq.getLast().p1][delay2][delay1]/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								break;
							case 2: 
								if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay1) < interimChange && LocalMathAbs(tmpPSeq.seq.getLast().p2 - delay2) < interimChange){
									transProb = tmpPSeq.prob + logPtchDynm2dOnly[ZERO_POINT + LocalMathAbs(tmpPSeq.seq.getLast().p1-delay1)][ZERO_POINT + LocalMathAbs(tmpPSeq.seq.getLast().p2-delay2)]/transCoeff + logPDynmPara/transCoeff;
									if (tmpMax < transProb) {
										tmpMax = transProb;
										maxSeq = tmpPSeq;
										flag = true;
									}
								}
								break;
							}
						}

						if (flag){
							candSeq = new PitchStateSeq(maxSeq);
							candSeq.seq.add(new PitchState(2,delay1,delay2));
							candSeq.prob = tmpMax + frameLevelTwoPitches[delay1][delay2];
							newTrellis.add(candSeq);
						}			
					}

			//prune
			if (newTrellis.size() > 200){
				Collections.sort(newTrellis);
				for (int i = 0; i < newTrellis.size() - 200; i++)
					newTrellis.removeLast();
			}

			//update
			oldTrellis = newTrellis;
			newTrellis = null;

			if (Math.round( (float) tf / numFrame * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");

		//output sequence
		pitchTrack = new int[2][numFrame];
		Collections.sort(oldTrellis);
		candSeq = oldTrellis.getFirst();
		int tIdx = 0;
		PitchState pstate;
		for (Iterator<PitchState> iter = candSeq.seq.iterator(); iter.hasNext(); ){
			pstate = iter.next();
			pitchTrack[0][tIdx] = pstate.p1;
			pitchTrack[1][tIdx] = pstate.p2;
			tIdx++;
		}


	}

	public void PitchTrackingViterbipass2(int[] nPoly){
		this.InitPitchDynm2();
		oldTrellis = new LinkedList<PitchStateSeq>();
		boolean[][] check = new boolean[maxDelay][maxDelay];
		PitchStateSeq tmpPSeq, maxSeq, candSeq;
		float tmpMax, transProb;
		boolean flag;

		int interimChange = 30;

		float transCoeff = Float.POSITIVE_INFINITY; //50;

		float negInf = Float.NEGATIVE_INFINITY;

		oldTrellis.add(new PitchStateSeq());

		System.out.print("Viterbi searching...");
		for (int tf = 1; tf < numFrame; tf++){

			System.out.print(Math.round( (float) tf / numFrame * 100) + "%");

			newTrellis = new LinkedList<PitchStateSeq>();

			//TwoPitchHypothesis(tf);
			TTAllPitchHypothesis2(tf);


			if (nPoly[tf]==0){
				//current is 0
				tmpMax = negInf;
				maxSeq = null;
				for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
					tmpPSeq = iter.next();
					switch (tmpPSeq.seq.getLast().numP){
					case 0:
						transProb = tmpPSeq.prob + logPDynm0to0/transCoeff;
						if (tmpMax < transProb){
							tmpMax = transProb;
							maxSeq = tmpPSeq;
						}
						break;
					case 1:
						transProb = tmpPSeq.prob + logPDynm1to0/transCoeff;
						if (tmpMax < transProb){
							tmpMax = transProb;
							maxSeq = tmpPSeq;
						}
						break;
					case 2:
						transProb = tmpPSeq.prob;
						if (tmpMax < transProb){
							tmpMax = transProb;
							maxSeq = tmpPSeq;
							flag = true;
						}
						break;
					}
				}
				if (maxSeq != null){
					candSeq = new PitchStateSeq(maxSeq);
					candSeq.seq.add(new PitchState(0,0,0));
					candSeq.prob = tmpMax + this.frameLevelZeroPitch;
					newTrellis.add(candSeq);
				}
			}

			if (nPoly[tf]==1){
				//current is 1
				for (int delay = 32 + 1; delay < maxDelay - 1; delay++){
					tmpMax = negInf;
					maxSeq = null;
					for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
						tmpPSeq = iter.next();
						switch (tmpPSeq.seq.getLast().numP){
						case 0:
							transProb = tmpPSeq.prob + logPDynm0to1/transCoeff;
							if (tmpMax < transProb){
								tmpMax = transProb;
								maxSeq = tmpPSeq;
							}
							break;
						case 1:
							if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay) < interimChange) {
								transProb = tmpPSeq.prob + logPDynm1to1[tmpPSeq.seq.getLast().p1][delay]/transCoeff;
								if (tmpMax < transProb) {
									tmpMax = transProb;
									maxSeq = tmpPSeq;
								}
							}
							break;
						case 2: 
							if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay) < interimChange || LocalMathAbs(tmpPSeq.seq.getLast().p2 - delay) < interimChange){
								transProb = tmpPSeq.prob + logPDynm2to1[tmpPSeq.seq.getLast().p1][tmpPSeq.seq.getLast().p2][delay]/transCoeff;
								if (tmpMax < transProb) {
									tmpMax = transProb;
									maxSeq = tmpPSeq;
								}
							}
							break;
						}
					}
					if (maxSeq != null){
						candSeq = new PitchStateSeq(maxSeq);
						candSeq.seq.add(new PitchState(1,delay,0));
						candSeq.prob = tmpMax + frameLevelOnePitch[delay];
						newTrellis.add(candSeq);
					}
				}
			}

			if (nPoly[tf]==2){
				//current is 2
				LinkedList<PeakLocation> peakList = new LinkedList<PeakLocation>();
				for (int delay1 = 32 + 1; delay1 < maxDelay - 2; delay1 ++)
					for (int delay2 = 32 + 1; delay2 < maxDelay - 2; delay2 ++)
						if (frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2-1] &&
								frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1][delay2-1] &&
								frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2-1] &&
								frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2] &&
								frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2] &&
								frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1-1][delay2+1] &&
								frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1][delay2+1] &&
								frameLevelTwoPitches[delay1][delay2] > frameLevelTwoPitches[delay1+1][delay2+1])

							peakList.add(new PeakLocation(delay1,delay2,frameLevelTwoPitches[delay1][delay2]));

				Collections.sort(peakList);

				for (int idx = 0; idx < Math.min(peakList.size(), 100); idx++){
					PeakLocation pL = peakList.get(idx);

					check[pL.r][pL.c] = true;

					check[pL.r-1][pL.c-1] = true; check[pL.r][pL.c-1] = true;
					check[pL.r+1][pL.c-1] = true; check[pL.r-1][pL.c] = true;
					check[pL.r+1][pL.c] = true; check[pL.r-1][pL.c+1] = true;
					check[pL.r][pL.c+1] = true; check[pL.r+1][pL.c+1] = true;

					check[pL.r][pL.c+2] = true; check[pL.r][pL.c-2] = true; 
					check[pL.r+1][pL.c+2] = true; check[pL.r+1][pL.c-2] = true; 
					check[pL.r-1][pL.c+2] = true; check[pL.r-1][pL.c-2] = true;
					check[pL.r-2][pL.c] = true; check[pL.r+2][pL.c] = true; 
					check[pL.r-2][pL.c-1] = true; check[pL.r+2][pL.c-1] = true;
					check[pL.r-2][pL.c+1] = true; check[pL.r+2][pL.c+1] = true;

					check[pL.r+2][pL.c+2] = true; check[pL.r+2][pL.c-2] = true;
					check[pL.r-2][pL.c+2] = true; check[pL.r-2][pL.c-2] = true;
				}

				for (int delay1 = 32 + 1; delay1 < maxDelay - 2; delay1 ++)
					for (int delay2 = 32 + 1; delay2 < maxDelay - 2; delay2 ++)
						if (check[delay1][delay2]){
							check[delay1][delay2] = false;
							tmpMax = negInf;
							maxSeq = null;
							flag = false;
							for (Iterator<PitchStateSeq> iter = oldTrellis.iterator(); iter.hasNext(); ){
								tmpPSeq = iter.next();
								switch (tmpPSeq.seq.getLast().numP){
								case 0:
								transProb = tmpPSeq.prob;
								if (tmpMax < transProb){
									tmpMax = transProb;
									maxSeq = tmpPSeq;
									flag = true;
								}
								break;
								case 1:
									if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay1) < interimChange) {
										transProb = tmpPSeq.prob + logPDynm1to2[tmpPSeq.seq.getLast().p1][delay1][delay2]/transCoeff;
										if (tmpMax < transProb) {
											tmpMax = transProb;
											maxSeq = tmpPSeq;
											flag = true;
										}
									}
									if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay2) < interimChange) {
										transProb = tmpPSeq.prob + logPDynm1to2[tmpPSeq.seq.getLast().p1][delay2][delay1]/transCoeff;
										if (tmpMax < transProb) {
											tmpMax = transProb;
											maxSeq = tmpPSeq;
											flag = true;
										}
									}
									break;
								case 2: 
									if (LocalMathAbs(tmpPSeq.seq.getLast().p1 - delay1) < interimChange && LocalMathAbs(tmpPSeq.seq.getLast().p2 - delay2) < interimChange){
										transProb = tmpPSeq.prob + logPtchDynm2dOnly[ZERO_POINT + LocalMathAbs(tmpPSeq.seq.getLast().p1-delay1)][ZERO_POINT + LocalMathAbs(tmpPSeq.seq.getLast().p2-delay2)]/transCoeff + logPDynmPara/transCoeff;
										if (tmpMax < transProb) {
											tmpMax = transProb;
											maxSeq = tmpPSeq;
											flag = true;
										}
									}
									break;
								}
							}

							if (flag){
								candSeq = new PitchStateSeq(maxSeq);
								candSeq.seq.add(new PitchState(2,delay1,delay2));
								candSeq.prob = tmpMax + frameLevelTwoPitches[delay1][delay2];
								newTrellis.add(candSeq);
							}			
						}
			}

			//prune
			/*if (newTrellis.size() > 200){
				Collections.sort(newTrellis);
				for (int i = 0; i < newTrellis.size() - 200; i++)
					newTrellis.removeLast();
			}*/

			//update
			oldTrellis = newTrellis;
			newTrellis = null;

			if (Math.round( (float) tf / numFrame * 100) < 10)
				System.out.print("\b\b");
			else
				System.out.print("\b\b\b");
		}
		System.out.println("Done");

		//output sequence
		pitchTrack = new int[2][numFrame];
		Collections.sort(oldTrellis);
		candSeq = oldTrellis.getFirst();
		int tIdx = 0;
		PitchState pstate;
		for (Iterator<PitchState> iter = candSeq.seq.iterator(); iter.hasNext(); ){
			pstate = iter.next();
			pitchTrack[0][tIdx] = pstate.p1;
			pitchTrack[1][tIdx] = pstate.p2;
			tIdx++;
		}


	}

	public void WriteOutPitch(String s1, String s2) {
		try {
			FileWriter w = new FileWriter(s1, false);
			for (int tf = 0; tf < numFrame; tf++)
				w.write(pitchTrack[0][tf] + " ");

			w.flush();
			w.close();

			w = new FileWriter(s2, false);
			for (int tf = 0; tf < numFrame; tf++)
				w.write(pitchTrack[1][tf] + " ");

			w.flush();
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void BuildPitchModels(Gammatone gt){
		double q11 = 0.0164, q12 = 0.0161, q21 = 0.0626, q22 = 0.1080; // ICASSP
		a1 = new double[numChannel];
		a2 = new double[numChannel];
		double[] noSigProb = new double[numChannel];
		offPkLogProb1 = new float[numChannel][ZERO_POINT*2];
		offPkLogProb2 = new float[numChannel][ZERO_POINT*2];

		double prob_sum1, prob_sum2;
		double k = 1;
		for (int chan=0;chan<55;chan++) {
			a1[chan] = -0.0105*chan+1.1950; // ICASSP Training set only
			a2[chan] = -0.0178*chan+1.5433; // ICASSP

			noSigProb[chan] = 1.0/(gt.sampFreq/gt.cFreq[chan]);
			prob_sum1 = 0; prob_sum2 = 0;
			for (int i=0;i<ZERO_POINT*2;i++) {
				prob_sum1 += Math.pow(Math.exp(-(double)(LocalMathAbs(ZERO_POINT-i))/a1[chan]),k)/a1[chan];
				prob_sum2 += Math.pow(Math.exp(-(double)(LocalMathAbs(ZERO_POINT-i))/a2[chan]),k)/a2[chan];
			}
			for (int i=0;i<ZERO_POINT*2;i++) {
				offPkLogProb1[chan][i] = (float)Math.log((1.0-q11)*Math.pow(Math.exp(-(double)(LocalMathAbs(ZERO_POINT-i))/a1[chan]),k)/a1[chan]/prob_sum1 + q11*noSigProb[chan]);
				offPkLogProb2[chan][i] = (float)Math.log((1.0-q12)*Math.pow(Math.exp(-(double)(LocalMathAbs(ZERO_POINT-i))/a2[chan]),k)/a2[chan]/prob_sum2 + q12*noSigProb[chan]);
			}
		}

		for (int chan=55;chan<numChannel;chan++) {
			a1[chan] = -0.0084*(float)chan+2.5943; // ICASSP 
			a2[chan] = -0.0162*(float)chan+3.5661; // ICASSP

			noSigProb[chan] = 1.0/(double)(200-32);
			prob_sum1 = 0; prob_sum2 = 0;
			for (int i=0;i<ZERO_POINT*2;i++) {
				prob_sum1 += Math.pow(Math.exp(-(double)(LocalMathAbs(ZERO_POINT-i))/a1[chan]),k)/a1[chan];
				prob_sum2 += Math.pow(Math.exp(-(double)(LocalMathAbs(ZERO_POINT-i))/a2[chan]),k)/a2[chan];
			}
			for (int i=0;i<ZERO_POINT*2;i++) {
				offPkLogProb1[chan][i] = (float)Math.log((1.0-q21)*Math.pow(Math.exp(-(double)(LocalMathAbs(ZERO_POINT-i))/a1[chan]),k)/a1[chan]/prob_sum1 + q21*noSigProb[chan]);
				offPkLogProb2[chan][i] = (float)Math.log((1.0-q22)*Math.pow(Math.exp(-(double)(LocalMathAbs(ZERO_POINT-i))/a2[chan]),k)/a2[chan]/prob_sum2 + q22*noSigProb[chan]);
			}
		}
	}

	private void ZeroPitchHypothesis(){
		zeroPitch = new float[numFrame];
		for (int tf = 0; tf < numFrame; tf++)
			zeroPitch[tf] = (float) Math.log(0.5); //P_C0 / N_ROOT;
	}

	private void OnePitchHypothesis(){
		onePitch = new float[numFrame][maxDelay];
		int tmpDelta;
		for (int tf = 0; tf < numFrame; tf++)
			for (int delay = 0; delay < maxDelay; delay++)
				for (int chan = 0; chan < numChannel; chan++)
					if (chanSelMask[chan][tf]>0){
						tmpDelta = 500;
						for (int peakIdx = 0; peakIdx < chanSelMask[chan][tf]; peakIdx++)
							if (LocalMathAbs(tmpDelta)>LocalMathAbs(storedPeaks[chan][tf].get(peakIdx).intValue()-delay))
								tmpDelta = storedPeaks[chan][tf].get(peakIdx).intValue()-delay;
						onePitch[tf][delay] += offPkLogProb1[chan][ZERO_POINT+tmpDelta] + FACTOR;
					}else
						onePitch[tf][delay] += offPkLogProb1[chan][ZERO_POINT+180] + FACTOR;

		for (int tf = 0; tf < numFrame; tf++)
			for (int delay = 0; delay < maxDelay; delay++)
				onePitch[tf][delay] /= N_ROOT;
	}

	private void NewAllPitchHypothesis(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];

		double totalEn = 0;
		for (int chan = 0; chan < this.numChannel; chan++)
			totalEn += this.energy[chan][tf];

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double multiP = 1;
				for (int chan = 0; chan < this.numChannel; chan++)
					totalEn += this.energy[chan][tf];
				for (int chan = 0; chan < this.numChannel; chan++){
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						multiP *= this.acl[chan][tf][p1];
					}else{
						multiP *= this.acl[chan][tf][p2];
					}
				}
				this.frameLevelTwoPitches[p1][p2] = (float)Math.log(multiP);
				this.frameLevelTwoPitches[p2][p1] = this.frameLevelTwoPitches[p1][p2];
			}

		// zero pitch (incomplete: needs to add unvoiced/noise case)
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1-1][p1-1] && this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1+1][p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]>tmpMin)
				tmpMin = this.frameLevelTwoPitches[p1][p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = (float) Math.log(0.1) * 128;
		else
			this.frameLevelZeroPitch = (float) Math.log(0.1) * 128;

		// one pitch prob
		for (int p1 = 0; p1 < 200; p1++){
			int lcounts = 0;
			double multiP = 1;
			for (int chan = 0; chan < 55; chan++){
				if (this.chanSelMask[chan][tf] >= 0){
					lcounts++;
					multiP *= this.acl[chan][tf][p1];
				}
			}
			this.frameLevelOnePitch[p1] = (float)Math.log(multiP)/lcounts*128;
			//this.frameLevelOnePitch[p1] += 100000f;
		}

		// two pitch prob
		float maxP = 0;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > maxP)
					maxP = this.frameLevelTwoPitches[p1][p2];
		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = 0; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2]<=0)
					this.frameLevelTwoPitches[p1][p2] = this.frameLevelTwoPitches[p1][p2] + 5f; //Float.NEGATIVE_INFINITY;
				else
					this.frameLevelTwoPitches[p1][p2] = this.frameLevelTwoPitches[p1][p2];
		//this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP, 8) - 1 + maxP);// - 0.05f;
		//this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2], 6));

	}

	private void AllPitchHypothesis(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < 128; chan++){								//note the change
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE1 += this.energy[chan][tf];
					}else{
						sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
						sumE2 += this.energy[chan][tf];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
						this.frameLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
					}
			}




		// zero pitch (incomplete: needs to add unvoiced/noise case)
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1-1][p1-1] && this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1+1][p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]>tmpMin)
				tmpMin = this.frameLevelTwoPitches[p1][p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = (float) Math.log(1.5);
		else
			this.frameLevelZeroPitch = (float) Math.log(0.1) * 100;

		// one pitch prob
		/*for (int p1 = 0; p1 < 200; p1++){
				int lcounts = 0;
				double sumEA = 0, sumE = 0;
				for (int chan = 0; chan < 55; chan++){
					if (this.chanSelMask[chan][tf] > 0){
						lcounts++;
						sumEA += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE += this.energy[chan][tf];
					}
				}
				if (lcounts == 0)
					this.frameLevelOnePitch[p1] = Float.NEGATIVE_INFINITY;
				else
					this.frameLevelOnePitch[p1] = (float) Math.log(sumEA / sumE);// + 100000f;
		}*/
		for (int p1 = 0; p1 < 200; p1++){
			int lcounts = 0;
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < this.numChannel; chan++){
				if (this.chanSelMask[chan][tf] > 0 && chan < 55){
					lcounts++;
					sumEA += this.energy[chan][tf] * this.acl[chan][tf][p1];
					sumE += this.energy[chan][tf];
				}else{
					sumEA += this.energy[chan][tf] * 0.7;
					sumE += this.energy[chan][tf];
				}
			}
			if (lcounts == 0)
				this.frameLevelOnePitch[p1] = Float.NEGATIVE_INFINITY;
			else
				this.frameLevelOnePitch[p1] = (float)Math.log(sumEA / sumE); //log is just removed
		}

		//printOutProb
		/*try {
			FileWriter w = new FileWriter("outProb",true);
			for (int i = 0; i < 200; i++)
				w.write(this.frameLevelOnePitch[i]+" ");
			w.write("\n");
			w.flush();
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/

		// two pitch prob
		float maxP = 0;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > maxP)
					maxP = this.frameLevelTwoPitches[p1][p2];
		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = 0; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2]<=0)
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
				else
					this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP, 6) - 1 + maxP);

	}

	private void AllPitchHypothesis2(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE1 += this.energy[chan][tf];
					}else{
						sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
						sumE2 += this.energy[chan][tf];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2)); //(sumEA1 / sumE1 + sumEA2 / sumE2);
						this.frameLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2)); //(sumEA1 / sumE1 + sumEA2 / sumE2);
						//this.frameLevelTwoPitches[p1][p2] = (float) (sumEA1 / sumE1 + sumEA2 / sumE2) / 2f;
						//this.frameLevelTwoPitches[p2][p1] = (float) (sumEA1 / sumE1 + sumEA2 / sumE2) / 2f;
					}
			}

		// zero pitch (completed: silence, unvoiced, noise case)
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1-1][p1-1] && this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1+1][p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]>tmpMin)
				tmpMin = this.frameLevelTwoPitches[p1][p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = (float) Math.log(1.5);		// important to handle other conditions and unvoiced speech
		else
			this.frameLevelZeroPitch = (float) Math.log(0.5);		// important to handle wideband noise (0p vs. 1p)

		// one pitch prob
		if (!isWideBand){
			for (int p1 = 0; p1 < 200; p1++)
				this.frameLevelOnePitch[p1] = (float) Math.log(-this.frameLevelTwoPitches[p1][p1]);
		}else{
			for (int p1 = 0; p1 < 200; p1++){
				int lcounts = 0;
				double sumEA = 0, sumE = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					if (chan < 55){
						lcounts++;
						sumEA += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE += this.energy[chan][tf];
					}else{
						//sumEA += this.energy[chan][tf] * 0.7;
						//sumE += this.energy[chan][tf];
					}
				}
				if (lcounts == 0)
					this.frameLevelOnePitch[p1] = Float.NEGATIVE_INFINITY;
				else
					this.frameLevelOnePitch[p1] = (float) Math.log(sumEA / sumE);// + 2f;
			}
		}

		// two pitch prob
		if (!isWideBand){
			float maxP = 0;
			for (int p1 = 32; p1 < 200; p1++)
				for (int p2 = p1; p2 < 200; p2++)
					if (this.frameLevelTwoPitches[p1][p2] > maxP)
						maxP = this.frameLevelTwoPitches[p1][p2];
			for (int p1 = 0; p1 < 200; p1++)
				for (int p2 = 0; p2 < 200; p2++)
					if (this.frameLevelTwoPitches[p1][p2]<=0)
						this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
					else
						this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP, 6) - 1 + maxP);
		}else{
			for (int p1 = 0; p1 < 200; p1++)
				for (int p2 = 0; p2 < 200; p2++)
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
		}
		/*
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < this.numChannel; chan++){								//note the change
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE1 += this.energy[chan][tf];
					}else{
						sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
						sumE2 += this.energy[chan][tf];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
					}
			}

		// zero pitch (incomplete: needs to add unvoiced/noise case)
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1-1][p1-1] && this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1+1][p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]>tmpMin)
				tmpMin = this.frameLevelTwoPitches[p1][p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = (float) Math.log(1.5);
		else
			this.frameLevelZeroPitch = (float) Math.log(0.1) * 100;

		// one pitch prob
		if (this.pitchTrack[0][tf]>0 && this.pitchTrack[1][tf]>0){
			System.out.println(tf);
			for (int p1 = 0; p1 < 200; p1++)
				this.frameLevelOnePitch[p1] = (float) Math.log(-this.frameLevelTwoPitches[p1][p1]);
		}else{
			for (int p1 = 0; p1 < 200; p1++){
				int lcounts = 0;
				double sumEA = 0, sumE = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					if (this.chanSelMask[chan][tf] > 0 && chan < 55){
						lcounts++;
						sumEA += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE += this.energy[chan][tf];
					}else{
						//sumEA += this.energy[chan][tf] * 0.7;
						//sumE += this.energy[chan][tf];
					}
				}
				if (lcounts == 0)
					this.frameLevelOnePitch[p1] = Float.NEGATIVE_INFINITY;
				else
					this.frameLevelOnePitch[p1] = (float) Math.log(sumEA / sumE);// + 2f;
			}
		}

		// two pitch prob
		float maxP = 0;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > maxP)
					maxP = this.frameLevelTwoPitches[p1][p2];
		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = 0; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2]<=0)
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
				else
					this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP, 8) - 1 + maxP);
		 */
	}
	
	private void TTAllPitchHypothesis2(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];
		
		float[] logEn = new float[128];
		for (int chan = 0; chan < 128; chan++)
			logEn[chan] = (float) Math.log(this.energy[chan][tf] + 1);
		//	this.energy[chan][tf] = 1f;

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					if (this.cc[chan][tf]>.95)
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += logEn[chan] * this.acl[chan][tf][p1];
						sumE1 += logEn[chan];
					}else{
						sumEA2 += logEn[chan] * this.acl[chan][tf][p2];
						sumE2 += logEn[chan];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
						this.frameLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
						//this.frameLevelTwoPitches[p1][p2] = (float) Math.sqrt(sumEA1 / sumE1 * sumEA2 / sumE2);
						//this.frameLevelTwoPitches[p2][p1] = (float) Math.sqrt(sumEA1 / sumE1 * sumEA2 / sumE2);
					}
			}
		

		// zero pitch (completed: silence, unvoiced, noise case)
		float[] tmpP0 = new float[200];
		for (int p1 = 0; p1 < 200; p1++){
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < 128; chan++){
				sumEA += logEn[chan] * this.acl[chan][tf][p1];
				sumE += logEn[chan];
			}
			tmpP0[p1] = (float) (sumEA / sumE);
		}
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (tmpP0[p1]>tmpP0[p1-1] && tmpP0[p1]>tmpP0[p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = 1f;
		for (int p1 = 32; p1 < 200; p1++)
			if (tmpP0[p1]<tmpMin)
				tmpMin = tmpP0[p1];
		
		tmpP0 = new float[200];
		for (int p1 = 0; p1 < 200; p1++){
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < 55; chan++){
				if (this.cc[chan][tf] > 0.95)
					sumEA += logEn[chan] * this.acl[chan][tf][p1];
				sumE += logEn[chan];
			}
			if (sumE == 0)
				tmpP0[p1] = 0f;
			else
				tmpP0[p1] = (float) (sumEA / sumE);
		}
		
		double acMean = 0;
		for (int p1 = 32; p1 < 200; p1++)
			acMean += tmpP0[p1];
		acMean /= 168;
		double acVar = 0;
		for (int p1 = 32; p1 < 200; p1++)
			acVar += (tmpP0[p1]-acMean) * (tmpP0[p1]-acMean);
		acVar /= (168-1);
		
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = 1.0f;		// important to handle other conditions and unvoiced speech
		else if (acVar < 0.01)
			this.frameLevelZeroPitch = 0.6f;		// important to handle wideband noise (0p vs. 1p)
		else
			this.frameLevelZeroPitch = 0.0f;

		// one pitch prob
		for (int p1 = 0; p1 < 200; p1++){
			int lcounts = 0;
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < 128; chan++){
				//if (chan < 128){
				if (this.cc[chan][tf] > 0.95 && chan < 128){
					lcounts++;
					sumEA += logEn[chan] * this.acl[chan][tf][p1];
					sumE += logEn[chan];
				}else{
					//sumEA += this.energy[chan][tf] * 0.7;
					//sumE += this.energy[chan][tf];
				}
			}
			
			this.frameLevelOnePitch[p1] = (float) (sumEA / sumE);
		}
		
		
		float maxP1 = 0;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelOnePitch[p1] > maxP1)
				maxP1 = this.frameLevelOnePitch[p1];
		
		float maxP2 = 0;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > maxP2)
					maxP2 = this.frameLevelTwoPitches[p1][p2];
		
		this.maxPP[0][tf] = maxP1;
		this.maxPP[1][tf] = maxP2;
	
		
		this.frameLevelZeroPitch = (float) Math.log(this.frameLevelZeroPitch);
		for (int p1 = 0; p1 < 200; p1++)
			this.frameLevelOnePitch[p1] = (float) Math.log(this.frameLevelOnePitch[p1]);
		
		double ccEn = 0, totalEn = 0;
		for (int chan = 0; chan < 128; chan++){
			if (this.cc[chan][tf] > 0.95)
				ccEn += this.energy[chan][tf];
			totalEn += this.energy[chan][tf];
		}
		double enCover = ccEn/totalEn;
		
		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = 0; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > 0 && enCover > 0.65)
					this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP2, 6) - 1 + maxP2);
				else if (this.frameLevelTwoPitches[p1][p2] > 0)
					this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP2, 6) - 1 + maxP2 - 0.05);
				else
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
		
	}
	
	private void WBAllPitchHypothesis2(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];
		
		float[] logEn = new float[128];
		for (int chan = 0; chan < 128; chan++)
			logEn[chan] = (float) Math.log(this.energy[chan][tf] + 1);
		//	this.energy[chan][tf] = 1f;

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					if (this.cc[chan][tf]>.95)
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += logEn[chan] * this.acl[chan][tf][p1];
						sumE1 += logEn[chan];
					}else{
						sumEA2 += logEn[chan] * this.acl[chan][tf][p2];
						sumE2 += logEn[chan];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
						this.frameLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
						//this.frameLevelTwoPitches[p1][p2] = (float) Math.sqrt(sumEA1 / sumE1 * sumEA2 / sumE2);
						//this.frameLevelTwoPitches[p2][p1] = (float) Math.sqrt(sumEA1 / sumE1 * sumEA2 / sumE2);
					}
			}
		

		// zero pitch (completed: silence, unvoiced, noise case)
		float[] tmpP0 = new float[200];
		for (int p1 = 0; p1 < 200; p1++){
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < 128; chan++){
					sumEA += logEn[chan] * this.acl[chan][tf][p1];
					sumE += logEn[chan];
			}
			tmpP0[p1] = - (float) (sumEA / sumE);
		}
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (tmpP0[p1]<tmpP0[p1-1] && tmpP0[p1]<tmpP0[p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (tmpP0[p1]>tmpMin)
				tmpMin = tmpP0[p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = 1.5f;		// important to handle other conditions and unvoiced speech
		else
			this.frameLevelZeroPitch = 0.6f;		// important to handle wideband noise (0p vs. 1p)

		// one pitch prob
		for (int p1 = 0; p1 < 200; p1++){
			int lcounts = 0;
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < 128; chan++){
				//if (chan < 128){
				if (this.cc[chan][tf] > 0.95 && chan < 55){
					lcounts++;
					sumEA += logEn[chan] * this.acl[chan][tf][p1];
					sumE += logEn[chan];
				}else{
					//sumEA += this.energy[chan][tf] * 0.7;
					//sumE += this.energy[chan][tf];
				}
			}
			
			this.frameLevelOnePitch[p1] = (float) (sumEA / sumE);
		}
		
		
		float maxP1 = 0;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelOnePitch[p1] > maxP1)
				maxP1 = this.frameLevelOnePitch[p1];
		
		float maxP2 = 0;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > maxP2)
					maxP2 = this.frameLevelTwoPitches[p1][p2];
		
		this.maxPP[0][tf] = maxP1;
		this.maxPP[1][tf] = maxP2;
	
		
		this.frameLevelZeroPitch = (float) Math.log(this.frameLevelZeroPitch);
		for (int p1 = 0; p1 < 200; p1++)
			this.frameLevelOnePitch[p1] = (float) Math.log(this.frameLevelOnePitch[p1]);
		
		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = 0; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > 0)
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;//(float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP2, 6) - 1 + maxP2);
				else
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
		
	}
	
	private void TTWBAllPitchHypothesis2(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];
		
		for (int chan = 0; chan < 128; chan++)
			this.energy[chan][tf] = (float) Math.log(this.energy[chan][tf] + 1);

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					//if (this.cc[chan][tf]>.95)
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE1 += this.energy[chan][tf];
					}else{
						sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
						sumE2 += this.energy[chan][tf];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						//this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2)); //(sumEA1 / sumE1 + sumEA2 / sumE2);
						//this.frameLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2)); //(sumEA1 / sumE1 + sumEA2 / sumE2);
						this.frameLevelTwoPitches[p1][p2] = (float) Math.sqrt(sumEA1 / sumE1 * sumEA2 / sumE2);
						this.frameLevelTwoPitches[p2][p1] = (float) Math.sqrt(sumEA1 / sumE1 * sumEA2 / sumE2);
					}
			}
		
		/*float maxP2 = Float.NEGATIVE_INFINITY;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				float tmpval = this.frameLevelTwoPitches[p1][p2] > 0 ? this.frameLevelTwoPitches[p1][p2] : -this.frameLevelTwoPitches[p1][p2];
				if (tmpval > maxP2)
					maxP2 = tmpval;
			}
		this.maxPP[0][tf] = maxP2;
		System.out.println(this.frameLevelTwoPitches[32][130]);
		
		float maxP1 = Float.NEGATIVE_INFINITY;
		for (int p1 = 32; p1 < 200; p1++){
			float tmpval = -this.frameLevelTwoPitches[p1][p1];
			if (tmpval > maxP1)
				maxP1 = tmpval;
		}
		this.maxPP[1][tf] = maxP1;*/

		// zero pitch (completed: silence, unvoiced, noise case)
		float[] tmpP0 = new float[200];
		for (int p1 = 0; p1 < 200; p1++){
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < 128; chan++){
					sumEA += this.energy[chan][tf] * this.acl[chan][tf][p1];
					sumE += this.energy[chan][tf];
			}
			tmpP0[p1] = - (float) (sumEA / sumE);
		}
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (tmpP0[p1]<tmpP0[p1-1] && tmpP0[p1]<tmpP0[p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (tmpP0[p1]>tmpMin)
				tmpMin = tmpP0[p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = 1.5f;		// important to handle other conditions and unvoiced speech
		else
			this.frameLevelZeroPitch = 0.5f;		// important to handle wideband noise (0p vs. 1p)

		// one pitch prob
		float[] maxAc = new float[128];
		for (int chan = 0; chan < 128; chan++)
			for (int p1 = 32; p1 < 200; p1++)
				if (this.acl[chan][tf][p1] > maxAc[chan])
					maxAc[chan] = this.acl[chan][tf][p1];
		
		for (int p1 = 0; p1 < 200; p1++){
			int lcounts = 0;
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < 128; chan++){
				//if (chan < 128){
				if (this.cc[chan][tf] > 0.8 && chan < 128){
					lcounts++;
					sumEA += this.energy[chan][tf] * this.acl[chan][tf][p1];
					sumE += this.energy[chan][tf];
				}else{
					sumEA += this.energy[chan][tf] * maxAc[chan];
					sumE += this.energy[chan][tf];
				}
			}
			/*if (lcounts == 0)
				this.frameLevelOnePitch[p1] = Float.NEGATIVE_INFINITY;
			else
				this.frameLevelOnePitch[p1] = (float) Math.log(sumEA / sumE);// + 2f;*/
			
			this.frameLevelOnePitch[p1] = (float) (sumEA / sumE);
			//if (this.frameLevelOnePitch[p1] > 0.5 && maxP2 < 0.85)
				//this.frameLevelOnePitch[p1] += 0.1f;
		}
		
		/*try {
			FileWriter w = new FileWriter("prob.2p",true);
			for (int p1 = 0; p1 < 200; p1++){
				for (int p2 = 0; p2 < 200; p2++)
					w.write(this.frameLevelTwoPitches[p1][p2]+" ");
				w.write("\n");
			}
			w.flush();
			w.close();
			
			w = new FileWriter("prob095.1p", true);
			for (int p1 = 0; p1 < 200; p1++)
				w.write(this.frameLevelOnePitch[p1] + " ");
			w.write("\n");
			w.flush();
			w.close();

		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		float maxP1 = 0;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelOnePitch[p1] > maxP1)
				maxP1 = this.frameLevelOnePitch[p1];
		
		float maxP2 = 0;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > maxP2)
					maxP2 = this.frameLevelTwoPitches[p1][p2];
		
		this.maxPP[0][tf] = maxP1;
		this.maxPP[1][tf] = maxP2;
	
		
		this.frameLevelZeroPitch = (float) Math.log(this.frameLevelZeroPitch);
		for (int p1 = 0; p1 < 200; p1++)
			this.frameLevelOnePitch[p1] = (float) Math.log(this.frameLevelOnePitch[p1]);
		
		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = 0; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > 0)
					this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP2, 6) - 1 + maxP2);
				else
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
					


		// two pitch prob
		/*if (!isWideBand){
			float maxP = 0;
			for (int p1 = 32; p1 < 200; p1++)
				for (int p2 = p1; p2 < 200; p2++)
					if (this.frameLevelTwoPitches[p1][p2] > maxP)
						maxP = this.frameLevelTwoPitches[p1][p2];
			for (int p1 = 0; p1 < 200; p1++)
				for (int p2 = 0; p2 < 200; p2++)
					if (this.frameLevelTwoPitches[p1][p2]<=0)
						this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
					else
						this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP, 6) - 1 + maxP);
		}else{
			for (int p1 = 0; p1 < 200; p1++)
				for (int p2 = 0; p2 < 200; p2++)
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
		}*/
		
	}
	
	/*private void MAXAllPitchHypothesis2(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];
		this.frameLevelTwoPitchesRatios = new float[maxDelay][maxDelay];

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE1 += this.energy[chan][tf];
					}else{
						sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
						sumE2 += this.energy[chan][tf];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitchesRatios[p1][p2] = 0;
					this.frameLevelTwoPitchesRatios[p2][p1] = (float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitchesRatios[p2][p1] = 0;
						this.frameLevelTwoPitchesRatios[p1][p2] = (float) (sumEA1 / sumE1);
					}else{
						this.frameLevelTwoPitches[p1][p2] = (float) Math.sqrt(sumEA1 / sumE1 * sumEA2 / sumE2);
						this.frameLevelTwoPitches[p2][p1] = (float) Math.sqrt(sumEA1 / sumE1 * sumEA2 / sumE2);
						this.frameLevelTwoPitchesRatios[p1][p2] = (float) (sumEA1 / sumE1);
						this.frameLevelTwoPitchesRatios[p2][p1] = (float) (sumEA2 / sumE2);
					}
			}

		// zero pitch (completed: silence, unvoiced, noise case)
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1-1][p1-1] && this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1+1][p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]>tmpMin)
				tmpMin = this.frameLevelTwoPitches[p1][p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = (float) Math.log(1.5);		// important to handle other conditions and unvoiced speech
		else
			this.frameLevelZeroPitch = (float) Math.log(0.5);		// important to handle wideband noise (0p vs. 1p)

		// one pitch prob
		for (int p1 = 0; p1 < 200; p1++){
			float mP2 = Float.NEGATIVE_INFINITY;
			int maxInd = 0;
			for (int p2 = 32; p2 < 200; p2++)
				if (mP2 < this.frameLevelTwoPitchesRatios[p1][p2]){
					mP2 = this.frameLevelTwoPitchesRatios[p1][p2];
					maxInd = p2;
				}
			this.frameLevelOnePitch[p1] = this.frameLevelTwoPitchesRatios[p1][maxInd];
			this.frameLevelOnePitch[p1] = (float) Math.log(this.frameLevelOnePitch[p1]);
		}

		// two pitch prob
		if (!isWideBand){
			float maxP = 0;
			for (int p1 = 32; p1 < 200; p1++)
				for (int p2 = p1; p2 < 200; p2++)
					if (this.frameLevelTwoPitches[p1][p2] > maxP)
						maxP = this.frameLevelTwoPitches[p1][p2];
			for (int p1 = 0; p1 < 200; p1++)
				for (int p2 = 0; p2 < 200; p2++)
					if (this.frameLevelTwoPitches[p1][p2]<=0)
						this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
					else
						this.frameLevelTwoPitches[p1][p2] = (float) Math.log(this.frameLevelTwoPitches[p1][p2]);
		}else{
			for (int p1 = 0; p1 < 200; p1++)
				for (int p2 = 0; p2 < 200; p2++)
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
		}
		
	}*/

	private void TwoPitchHypothesis(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE1 += this.energy[chan][tf];
					}else{
						sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
						sumE2 += this.energy[chan][tf];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2)); //(sumEA1 / sumE1 + sumEA2 / sumE2);
						this.frameLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2)); //(sumEA1 / sumE1 + sumEA2 / sumE2);
						//this.frameLevelTwoPitches[p1][p2] = (float) (sumEA1 / sumE1 + sumEA2 / sumE2) / 2f;
						//this.frameLevelTwoPitches[p2][p1] = (float) (sumEA1 / sumE1 + sumEA2 / sumE2) / 2f;
					}
			}

		// zero pitch (incomplete: needs to add unvoiced/noise case)
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1-1][p1-1] && this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1+1][p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]>tmpMin)
				tmpMin = this.frameLevelTwoPitches[p1][p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = (float) Math.log(1.5);
		else
			this.frameLevelZeroPitch = (float) Math.log(0.1);

		// one pitch prob
		/*float minP = 0;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<minP)
				minP = this.frameLevelTwoPitches[p1][p1];*/
		for (int p1 = 0; p1 < 200; p1++){
			this.frameLevelOnePitch[p1] = (float) Math.log(-this.frameLevelTwoPitches[p1][p1]);
			//this.frameLevelOnePitch[p1] = (float) Math.log( Math.pow(-this.frameLevelTwoPitches[p1][p1] + 1 + minP, 1) - 1 - minP); // + (float) Math.log(1.1);
			//this.frameLevelOnePitch[p1] = (float) Math.log( Math.pow(-this.frameLevelTwoPitches[p1][p1], 5));
			/*if (minP < -0.8)
				this.frameLevelOnePitch[p1] += (float) Math.log(1.5);*/
		}

		// two pitch prob
		float maxP = 0;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > maxP)
					maxP = this.frameLevelTwoPitches[p1][p2];
		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = 0; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2]<=0)
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
				else
					//this.frameLevelTwoPitches[p1][p2] = (float) Math.log(this.frameLevelTwoPitches[p1][p2] / 1) * 1.5f;
					this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2] + 1 - maxP, 8) - 1 + maxP);
		//this.frameLevelTwoPitches[p1][p2] = (float) Math.log( Math.pow(this.frameLevelTwoPitches[p1][p2], 6));

		/*for (int p1 = 0; p1 < 200; p1++)
			if (-minP >= maxP)
				this.frameLevelOnePitch[p1] += (float) Math.log(2);*/

		/*//optional printout
		try {
			FileWriter w = new FileWriter("twoPitchProb.tmp",true);
			for (int p1 = 0; p1 < 200; p1++){
				for (int p2 = 0; p2 < 200; p2++)
					w.write(this.frameLevelTwoPitches[p1][p2]+" ");
				w.write("\n");
			}
			w.flush();
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}

	private void TwoPitchHypothesis(int tf, boolean isanAnchor){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < this.numChannel; chan++){
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE1 += this.energy[chan][tf];
					}else{
						sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
						sumE2 += this.energy[chan][tf];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						//this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2)); //(sumEA1 / sumE1 + sumEA2 / sumE2);
						//this.frameLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2)); //(sumEA1 / sumE1 + sumEA2 / sumE2);
						this.frameLevelTwoPitches[p1][p2] = (float) (sumEA1 / sumE1 + sumEA2 / sumE2);
						this.frameLevelTwoPitches[p2][p1] = (float) (sumEA1 / sumE1 + sumEA2 / sumE2);
					}
			}

		// zero pitch (incomplete: needs to add unvoiced/noise case)
		boolean havePeak = false;
		for (int p1 = 32 + 1; p1 < 200 - 1; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1-1][p1-1] && this.frameLevelTwoPitches[p1][p1]<this.frameLevelTwoPitches[p1+1][p1+1]){
				havePeak = true;
				break;
			}
		float tmpMin = -1;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]>tmpMin)
				tmpMin = this.frameLevelTwoPitches[p1][p1];
		tmpMin = -tmpMin;
		if (!havePeak || tmpMin > 0.5)
			this.frameLevelZeroPitch = (float) Math.log(1.5);
		else
			this.frameLevelZeroPitch = (float) Math.log(0.1);

		// one pitch prob
		/*float minP = 0;
		for (int p1 = 32; p1 < 200; p1++)
			if (this.frameLevelTwoPitches[p1][p1]<minP)
				minP = this.frameLevelTwoPitches[p1][p1];*/
		for (int p1 = 0; p1 < 200; p1++){
			this.frameLevelOnePitch[p1] = (float) Math.log(-this.frameLevelTwoPitches[p1][p1]);
			if (isanAnchor)
				this.frameLevelOnePitch[p1] += (float) Math.log(1.5);
			//if (minP < -0.8)
			//	this.frameLevelOnePitch[p1] += (float) Math.log(1.5);
		}

		// two pitch prob
		/*float maxP = 0;
		for (int p1 = 32; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2] > maxP)
					maxP = this.frameLevelTwoPitches[p1][p2];*/
		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = 0; p2 < 200; p2++)
				if (this.frameLevelTwoPitches[p1][p2]<=0)
					this.frameLevelTwoPitches[p1][p2] = Float.NEGATIVE_INFINITY;
				else
					this.frameLevelTwoPitches[p1][p2] = (float) Math.log(this.frameLevelTwoPitches[p1][p2] / 2);

		/*for (int p1 = 0; p1 < 200; p1++)
			if (-minP >= maxP)
				this.frameLevelOnePitch[p1] += (float) Math.log(2);

		//optional printout
		try {
			FileWriter w = new FileWriter("twoPitchProb.tmp",true);
			for (int p1 = 0; p1 < 200; p1++){
				for (int p2 = 0; p2 < 200; p2++)
					w.write(this.frameLevelTwoPitches[p1][p2]+" ");
				w.write("\n");
			}
			w.flush();
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}

	private float LocalMathMax(float in1, float in2){
		return (in1 > in2 ? in1 : in2);
	}

	private double LocalMathMax(double in1, double in2){
		return (in1 > in2 ? in1 : in2);
	}

	private float LocalMathMin(float in1, float in2){
		return (in1 < in2 ? in1 : in2);
	}

	private int LocalMathAbs(int in){
		return (in > 0 ? in : -in);
	}
	
	private void InspectPitchProb(int tf){
		this.frameLevelOnePitch = new float[maxDelay];
		this.frameLevelTwoPitches = new float[maxDelay][maxDelay];
		this.twoPitchLCounts = new int[maxDelay][maxDelay];

		for (int p1 = 0; p1 < 200; p1++)
			for (int p2 = p1; p2 < 200; p2++){
				int lcounts = 0;
				double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
				for (int chan = 0; chan < 128; chan++){								//note the change
					if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
						lcounts++;
						sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
						sumE1 += this.energy[chan][tf];
					}else{
						sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
						sumE2 += this.energy[chan][tf];
					}
				}
				if (lcounts == 0){
					this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
					this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
				}else
					if (lcounts == this.numChannel){
						this.frameLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
						this.frameLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
					}else{
						this.frameLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
						this.frameLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
					}
				this.twoPitchLCounts[p1][p2] = lcounts;
				this.twoPitchLCounts[p2][p1] = 128 - lcounts;
			}

		// one pitch prob
		for (int p1 = 0; p1 < 200; p1++){
			int lcounts = 0;
			double sumEA = 0, sumE = 0;
			for (int chan = 0; chan < 128; chan++){
				if (this.chanSelMask[chan][tf] > 0 && chan < 128){
					lcounts++;
					sumEA += this.energy[chan][tf] * this.acl[chan][tf][p1];
					sumE += this.energy[chan][tf];
				}else{
					sumEA += this.energy[chan][tf] * this.acl[chan][tf][p1];
					sumE += this.energy[chan][tf];
				}
			}
			if (lcounts == 0)
				this.frameLevelOnePitch[p1] = Float.NEGATIVE_INFINITY;
			else
				this.frameLevelOnePitch[p1] = (float) (sumEA / sumE); //log is just removed
			this.onePitchLCounts = lcounts;
		}

		//printOutProb
		try {
			FileWriter w = new FileWriter("oneP.full",true);
			for (int i = 0; i < 200; i++)
				w.write(this.frameLevelOnePitch[i]+" ");
			w.write("\n");
			w.flush();
			w.close();
			
			w = new FileWriter("oneC.full",true);
			w.write(this.onePitchLCounts+"\n");
			w.flush();
			w.close();
			
			w = new FileWriter("twoP.full",true);
			for (int i = 0; i < 200; i++){
				for (int j = 0; j < 200; j++)
					w.write(this.frameLevelTwoPitches[i][j]+" ");
				w.write("\n");
			}
			w.flush();
			w.close();
			
			w = new FileWriter("twoC.full",true);
			for (int i = 0; i < 200; i++){
				for (int j = 0; j < 200; j++)
					w.write(this.twoPitchLCounts[i][j]+" ");
				w.write("\n");
			}
			w.flush();
			w.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void BatchInspect(){
		System.out.print("Inspecting...");
		for (int tf = 0; tf < numFrame; tf++)
			this.InspectPitchProb(tf);
	}
	
	public void FixAC(){
		for (int chan = 0; chan < this.numChannel; chan++)
			for (int tf = 0; tf < this.numFrame; tf++){
				for (int p = 1; p < this.maxDelay; p++)
					if (this.acl[chan][tf][p] - this.acl[chan][tf][p-1] <= 0)
						this.acl[chan][tf][p-1] = 0;
					else
						break;
			}
	}
	
	public void WriteMask(){
		int[][] mask = new int[this.numChannel][this.numFrame];
		for (int chan = 0; chan < this.numChannel; chan++)
			for (int tf = 0; tf < this.numFrame; tf++)
				if (this.pitchTrack[0][tf]>0 && this.pitchTrack[1][tf]>0)
					if (this.acl[chan][tf][this.pitchTrack[0][tf]] > this.acl[chan][tf][this.pitchTrack[1][tf]])
						mask[chan][tf] = 1;
		try {
			FileWriter w = new FileWriter("msk",false);
			for (int chan = 0; chan < this.numChannel; chan++){
				for (int tf = 0; tf < this.numFrame; tf++)
					w.write(mask[chan][tf]+" ");
				w.write("\n");
			}
			w.flush();
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void WriteMaxPP(String s){
		
		try {
			FileWriter w = new FileWriter(s,false);
			for (int tf = 0; tf < this.numFrame; tf++){
				this.TTAllPitchHypothesis2(tf);
				w.write(maxPP[0][tf]+" "+maxPP[1][tf]+"\n");
			}
			w.flush();
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void checkWB(){
		float[] maxP2 = new float[this.numFrame];
		float[][] localframeLevelTwoPitches = new float[maxDelay][maxDelay];
		for (int tf = 0; tf < this.numFrame; tf++){
			for (int p1 = 0; p1 < 200; p1++)
				for (int p2 = p1; p2 < 200; p2++){
					int lcounts = 0;
					double sumEA1 = 0, sumEA2 = 0, sumE1 = 0, sumE2 = 0;
					for (int chan = 0; chan < 128; chan++){								//note the change
						if (this.acl[chan][tf][p1]>=this.acl[chan][tf][p2]){
							lcounts++;
							sumEA1 += this.energy[chan][tf] * this.acl[chan][tf][p1];
							sumE1 += this.energy[chan][tf];
						}else{
							sumEA2 += this.energy[chan][tf] * this.acl[chan][tf][p2];
							sumE2 += this.energy[chan][tf];
						}
					}
					if (lcounts == 0){
						localframeLevelTwoPitches[p1][p2] = -(float) (sumEA2 / sumE2);
						localframeLevelTwoPitches[p2][p1] = -(float) (sumEA2 / sumE2);
					}else
						if (lcounts == this.numChannel){
							localframeLevelTwoPitches[p1][p2] = -(float) (sumEA1 / sumE1);
							localframeLevelTwoPitches[p2][p1] = -(float) (sumEA1 / sumE1);
						}else{
							localframeLevelTwoPitches[p1][p2] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
							localframeLevelTwoPitches[p2][p1] = (float) ((sumEA1 + sumEA2) / (sumE1 + sumE2));
						}
				}
			for (int p1 = 32; p1 < 200; p1++)
				for (int p2 = p1; p2 < 200; p2++)
					if (maxP2[tf] < localframeLevelTwoPitches[p1][p2])
						maxP2[tf] = localframeLevelTwoPitches[p1][p2];
		}
		float lowCount = 0;
		for (int tf = 0; tf < this.numFrame; tf++)
			if (maxP2[tf] < 0.85)
				lowCount++;
		if (lowCount/this.numFrame > 0.6)
			this.isWideBand = true;
		else
			this.isWideBand = false;
	}
}
