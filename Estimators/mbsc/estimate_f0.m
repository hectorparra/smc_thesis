addpath('src'); % import yin code
addpath('jsonlab'); % import code to save data as json

% JSON save parameters
opt.ForceRootName = 0; % do not use root element
opt.Inf = '$1Infinity'; % use standard JS symbol
opt.NaN = 'NaN'; % use standard JS symbol

filenames = loadjson('../../MIR-1K/mir1k_micro.json');

folders = {'../../MIR-1K/Wavfile'};
reverb_folders = rdir('../../MIR-1K reverb/air_*', 'isdir==1');
for i = 1:length(reverb_folders)
  folders{length(folders)+1} = reverb_folders(i).name;
end
dereverb_folders = rdir('../../MIR-1K dereverb/**/air_*', 'isdir==1');
for i = 1:length(dereverb_folders)
  folders{length(folders)+1} = dereverb_folders(i).name;
end
wav_files = {};
for i = 1:length(folders)
  folder = folders{i};
  for j = 1:length(filenames)
    wav_files{length(wav_files)+1} = sprintf('%s/%s.wav', folder, filenames{j});
  end
end

progressbar = waitbar(0,'Initializing...'); tic;
for i = 1:length(wav_files)
  wav_file = wav_files{i};

  [x, fs, nbits] = wavread(wav_file); % 16kHz, 16bits
  %[f0, score] = mbsc(x, fs); % call estimator
  [f0, score] = fast_mbsc_fixedWinlen_tracking(x, fs); % fast estimator 
  R.f0 = f0(1:2:end)';
  R.score = score(1:2:end)';

  % Save results to JSON
  [path, name, ~] = fileparts(wav_file); % get rid off .wav extension
  opt.FileName = sprintf('%s/%s.mbsc.f0.json', path, name);
  savejson('', R, opt);  

  % progress bar
  time = toc;
  perc = i / length(wav_files);
  trem = time/perc-time; %Calculate the time remaining
  hrs = floor(trem/3600);
  min = floor((trem-hrs*3600)/60);
  waitbar(perc, progressbar, ...
    sprintf('%0.1f%% %03.0f:%02.0f:%02.0f ETA', perc*100, hrs, min, rem(trem,60)));    
end
close(progressbar); 
