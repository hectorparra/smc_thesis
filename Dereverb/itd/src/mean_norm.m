function [feat, meanfeat] = mean_norm(feat);

% Mean normalisation.
% input should be of size Dimension * Observations.

meanfeat = mean(feat,2);
feat = feat - repmat(meanfeat, 1, size(feat,2));

