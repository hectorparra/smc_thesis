% This script converts the stereo wav files from MIR-1K to mono files
% to only keep the voice channel

wav_files = dir('Wavfile/*.wav');

progressbar = waitbar(0,'Initializing...'); tic;

for i = 1:length(wav_files)
    filename = wav_files(i).name;
    soundfile = sprintf('Wavfile/%s', filename);
    [x, fs, nbits] = wavread(soundfile); % 16kHz, 16bits
    if size(x, 2) > 1
        x = x(:, 2); % Select only voice, i.e. Right channel
        wavwrite(x, fs, soundfile)
    end
    

  % progress bar
  time = toc;
  perc = i / length(wav_files);
  trem = time/perc-time; %Calculate the time remaining
  hrs = floor(trem/3600);
  min = floor((trem-hrs*3600)/60);
  waitbar(perc, progressbar, ...
    sprintf('%0.1f%% %03.0f:%02.0f:%02.0f ETA', perc*100, hrs, min, rem(trem,60)));    
    
end

close(progressbar);