# -*- coding: utf-8 -*-

from pylab import *
import glob
from fusha import FushaBar
import melodia
import imp
common = imp.load_source('common', '../../common/common.py')

mir_path = '../../MIR-1K/Wavfile'
mir_reverb_folders = glob.glob('../../MIR-1K reverb/air_*')
mir_dereverb_folders = glob.glob('../../MIR-1K dereverb/**/air_*')
# folders to evaluate
eval_folders = []
#eval_folders += [mir_path] 
#eval_folders += mir_reverb_folders
eval_folders += mir_dereverb_folders

wav_files = []
for eval_folder in eval_folders:
  wav_files += glob.glob('%s/*.wav' % eval_folder)

with FushaBar(interval=1, bar_len=100) as bar:
  i = 0
  for wav_file in wav_files:
    filename = wav_file[:-4] # remove .wav extension
    f0_file = '%s.mel.f0.json' % filename
    
    data = melodia.evaluate(wav_file, 'melodia_parameters.n3')
  
    common.save_json(data, f0_file)
    
    i += 1
    bar.update(i*100//len(wav_files))
    
print "Estimation finished!"    
  