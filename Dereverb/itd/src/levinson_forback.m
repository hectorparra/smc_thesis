function [X] = levinson_forback(Phi, Y)

% Solve for Y = M(Phi) * X, where M(.) is a Toeplitz matrix with Phi as its
% elements.

% Solve using forward-backward recursion.
% Obtain the forward and backward vectors
F{1} = 1/Phi(1); B{1} = 1/Phi(1);
N = length(Phi);

check = 0;
if(check); T = toeplitz(Phi); end
if(check); Y = Y(:);end

for i = 2:N
    % ef = Phi(i:-1:2) * F{i-1};
    eb = Phi(2:i) * B{i-1};
    ef = eb;
    
    sub_mul_efb = 1 - ef * eb;
    
    %F{i} = 1/sub_mul_efb * [F{i-1}; 0] - ef/sub_mul_efb * [0; B{i-1}];    
    B{i} = 1/sub_mul_efb * [0; B{i-1}] - eb/sub_mul_efb * [F{i-1}; 0];
    F{i} = B{i}(end:-1:1);    
    
    % check
    if(check); [T(1:i, 1:i) * F{i},  T(1:i, 1:i) * B{i}]; end
end

% Solve the linear equation using above backward vectors
X{1} = Y(1)/Phi(1);

for i = 2:N
    ef = Phi(i:-1:2) * X{i-1};
    X{i} = [X{i-1}; 0] + (Y(i)-ef) * B{i};
    
    if(check); [T(1:i, 1:i) * X{i} - Y(1:i)], end
end
X = X{end};