% This script generates reverberated files of MIR-1K voices using AIR
% database impulse responses
clear all;close all;clc

%air_files = dir('../AIR_1_4/*.mat'); % all files
air_files = {
  'air_binaural_booth_0_0_1.mat'    % booth 0.5m
  'air_binaural_booth_0_0_2.mat'    % booth 1m
  'air_binaural_booth_0_0_3.mat'    % booth 1.5m
  'air_binaural_lecture_0_0_1.mat'  % lecture 2.25m
  'air_binaural_lecture_0_0_2.mat'  % lecture 4m
  'air_binaural_lecture_0_0_3.mat'  % lecture 5.56m
  'air_binaural_lecture_0_0_4.mat'  % lecture 7.1m
  'air_binaural_lecture_0_0_5.mat'  % lecture 8.68m
  'air_binaural_lecture_0_0_6.mat'  % lecture 10.2m
  'air_binaural_meeting_0_0_1.mat'  % meeting 1.45m
  'air_binaural_meeting_0_0_2.mat'  % meeting 1.7m
  'air_binaural_meeting_0_0_3.mat'  % meeting 1.9m
  'air_binaural_meeting_0_0_4.mat'  % meeting 2.25m
  'air_binaural_meeting_0_0_5.mat'  % meeting 2.8m
  'air_binaural_office_0_0_1.mat'   % office 1m
  'air_binaural_office_0_0_2.mat'   % office 2m
  'air_binaural_office_0_0_3.mat'   % office 3m
  'air_phone_lecture_hfrp_0.mat'    % lecture hands-free phone
  'air_phone_lecture_hhp_0.mat'     % lecture hand held held
  'air_phone_meeting_hfrp_0.mat'    % meeting hands-free phone
  'air_phone_meeting_hhp_0.mat'     % meeting hand held held
  'air_phone_office_hfrp_0.mat'     % office hands-free phone
  'air_phone_office_hhp_0.mat'      % office hand held held
};

wav_dir = '../MIR-1K/Wavfile';
wav_files = dir(sprintf('%s/*.wav', wav_dir));
fs = 16000;

% create directories
for f = 1:length(air_files)
  [~, name, ~] = fileparts(air_files{f});
  mkdir(name);
end

progressbar = waitbar(0,'Initializing...'); tic;

for f = 1:length(air_files)
%   air_file = air_files(f).name;
  air_file = sprintf('%s/%s', '../AIR_1_4', air_files{f});
  [~, air_name, ~] = fileparts(air_file);
  load(air_file); % load rir
  % transpose
  if size(h_air,1)~=1
    h_air = h_air';
  end
  % resample
  if air_info.fs ~= fs
    h_air = resample(h_air, fs, air_info.fs);
    air_info.fs = fs;
  end  
  % Remove direct-path delay
  [~, locs] = findpeaks(h_air, 'MINPEAKHEIGHT', max(h_air)*0.6 );
  h_air = h_air(locs(1):end);  
  
  for i = 1:length(wav_files)
    wav_path = wav_files(i).name;
    [~, name, ~] = fileparts(wav_path);

    [x, fs, nbits] = wavread(sprintf('%s/%s', wav_dir, wav_path));
    % x = x(:, 2); % use only the voice channel

    y = conv(x, h_air); % convolve

    y = (0.95 / max(abs(y))) .* y; % normalize (avoid clipping)
    wavwrite(y, fs, sprintf('%s/%s.wav', air_name, name)); % save wav

    % progress bar
    time = toc;
    perc = (i + (f-1)*length(wav_files)) / (length(air_files) * length(wav_files));
    trem = time/perc-time; %Calculate the time remaining
    hrs = floor(trem/3600);
    min = floor((trem-hrs*3600)/60);
    waitbar(perc, progressbar, ...
    sprintf('%0.1f%% %03.0f:%02.0f:%02.0f ETA', perc*100, hrs, min, rem(trem,60)));         
  end   
  
end

close(progressbar); 
