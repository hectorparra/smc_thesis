///////////////////////////////
// size of the window in number of hop sizes, better to use 2 4 8 16
// 2=513 samples
// 4=1025 samples
// 8=2049 samples
// 16=4097 samples
/ for voice analysis ResWindowSize 11
/ for violin 8
//ResWindowSize 8
ResWindowSize 4
// 11
/
// using black-man harris 74dB ? --> 11
ResWindowType 10
// 10
//////////////////////////////
// SINE ANALYSIS
// SineModel 1 = HARMONIC
// SineModel 2 = INHARMONIC
SineModel 1
nSines 50 
///////////////////////////////
// threshold of peak detection (in dB)
MagThreshold -200
///////////////////////////////
// pitch parameters in Hz
//LowestPitch 60
LowestPitch 63.1883
/25
DefaultPitch 100
/60
//HighestPitch 1100
HighestPitch 706.3069
/
/
PitchDetection 1
///////////////////////////////
// frame rate 172 --> hop size 256 samples at 44100Hz, about 6 ms
/FrameRate 689.06
/FrameRate 172
FrameRate 50
/FrameRate 345
///////////////////////////////
MelAnalysis 1
ResZeroPaddingFactor 1
WindowsInFFT 2
KeepSinePhases 1
KeepSinePhaseAlignment 1
AttackReanalysis 0
nAttackReanalysisFrames 10
ResSubstractOffset 0
TryToUseWindowTables 0
SineUseResidualWindow 1
HighestFreq 22050
ResSubstractMode 0
ResAmpCorrec 0
ResModel 0
// 0
HarmonicFreqDeviation 1
HarmonicFreqDeviationSlope 0
PartialTrackMode 1
HarmonicSPPAnalysis 1
VFXSPPAnalysis 1
HarmonicCorrection 0
PitchContribution 1
MaxFreqEnergyForHarmonic 400000
MaxPitchError 50
MaxNewPitchError 50
MaxHarmonicAmplitudeOscillation 0
MaxHarmonicAmplitudeVariation 0
/
/ConsonantZeroCrossing 0
/UseHarmonicHighLowEnergyRatio 0
SilenceEnergy 0
SilenceZeroCrossing 0
/
AttributeFlags 4
RefinePitchWithHarmonics 0
RefinePeakWithPitchAdaptedWindow 0
/
/ use spectral amplitude correlation pitch detection method
PitchMode 4
/
forceFundamentalContinuity 1
