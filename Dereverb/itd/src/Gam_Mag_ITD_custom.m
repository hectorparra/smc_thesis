function [] = Gam_Mag_ITD_custom(inwavfile, outwavfile, gH, igH, analsize)
% Customized by Hector Parra to pass Gammatone transformation matrix (gH)
% and its inverse (igH) so they can be calculated just once outside the
% function

Spar = 0;    % Sparsity parameter
I = 20;      % Number of iterations of ITD processing (used for initialization)
I_ITD = 5;   % Number of iterations of ITD algorithm

hp = fix(analsize/4); % window hop length is 1/4 the analsize
Nframes = fix(10/hp * 256); % Filter length is 10-frames long for hp=256 (16 ms), thus a total of 160 ms
fs = 16000;  % Assuming 16 kHz

% Read speech data
input = wavread(inwavfile);

% mean and scale normalization [not essential to algorithm]
input = input - mean(input);
input = norm_wav(input, 16);

% ITD processing
[output] = derev_ITD(input', analsize, Nframes, Spar, I, hp, gH, igH, I_ITD);

% Normalize output
output = output - mean(output);
output = norm_wav(output, 16);

% Write output to file
wavwrite(output, fs, outwavfile);