# -*- coding: utf-8 -*-
# Grid search to find the best params for MELODIA algorithm.
# f0 boundaries are fixed for all algorithms, so we are tunning:
#   - minpeaksalience (monophonic voice filter): for monophonic recordings only (i.e. solo melody with no accompaniment).
#     Increase this value to filter out background noise (e.g. noise introduced by a laptop microphone). Always set to 0 for polyphonic recordings.
#     * min value: 0
#     * max value: 100
#     * quantized step: 1
#   - voicing: determine the tolerance of the voicing filter. Higher values mean more tolerance
#     (i.e. more pitch contours will be included in the melody even if they are less salient), 
#     lower values mean less tolerance (i.e. only the most salient pitch contours will be included in the melody).
#     * min value: -2.6
#     * max value: 3
#     * quantized step: 0.01
  
from numpy import arange  
from pystache import Renderer
from subprocess import call
from fusha import FushaBar
import os, imp
common = imp.load_source('common', os.path.join(os.path.dirname(__file__), '../../common/common.py'))
melodia = common.relative_import(__file__, 'melodia.py')
evaluation = common.relative_import(__file__, '../../Evaluation/evaluation.py')

context = {}
evaluations = []
results = []

voicing_values = arange(-2.6, 3.01, 0.4)
minpeaksalience_values = arange(0, 100.1, 10)

with FushaBar(interval=1, bar_len=100) as bar:
  i = 0  
  for voicing in voicing_values:
    context['voicing'] = voicing
    
    for minpeaksalience in minpeaksalience_values:
      context['minpeaksalience'] = minpeaksalience 
  
      # Create temporal transform file with parameters for melodia
      with open('grid_transform.n3', 'w') as file: 
        file.write(Renderer().render_path('transform_template.mustache', context))
      
      # Evaluate for the MIR-1K micro dataset
      for filename in common.mir1k_micro():
        wav_file = '../../MIR-1K/Wavfile/%s.wav' % filename
        f0 = melodia.evaluate(wav_file, 'grid_transform.n3')
        evalu = evaluation.mirex_evaluation(f0['f0'], evaluation.read_ground_truth(filename))
        evaluations.append(evalu)
      
      run_results = evaluation.compute_results(evaluations)
      run_results.update(context)
      results.append(run_results)
      common.save_json(results, 'gridsearch.json') # unnecesary, make it safe for unnexpected exceptions
      
      i += 1
      bar.update(i*100//(len(minpeaksalience_values)*len(voicing_values)))

common.save_json(results, 'gridsearch.json')

# Erase temporal transform file
call(['rm', 'grid_transform.n3'])
