# -*- coding: utf-8 -*-

from pylab import *
from scipy.io import wavfile
import glob, os
from fusha import FushaBar
import imp
common = imp.load_source('common', '../../common/common.py')
from subprocess import call

# D.Wang algorithm supposes 16KHz input files encoded in ASCII
# and uses a 320 samples (20ms) window size and 160 samples (10ms) hop size

def wav2ascii(wav_file, txt_file):
  '''Convert wav file into ascii file with an integer value per line'''
  _, data = wavfile.read(wav_file)
  savetxt(txt_file, data, fmt='%i')

mir_path = '../../MIR-1K/Wavfile'
mir_reverb_path = '../../MIR-1K reverb'
mir_reverb_folders = glob.glob('%s/air_*' % mir_reverb_path)
eval_folders = [mir_path] + mir_reverb_folders # folders to evaluate

# We use the micro dataset because this algorithm is extremely slow
filenames = common.mir1k_micro()

with FushaBar(interval=1, bar_len=100) as bar:
  i = 0
  for eval_folder in eval_folders:
    for filename in filenames:
      wav_file = '%s/%s.wav' % (eval_folder, filename)
      ascii_file = '%s/%s.wav.txt' % (eval_folder, filename) # wav encoded in ascii 
      pitch1_file = '%s/%s.wang.f0.csv' % (eval_folder, filename) # pitch1 output of jar
      pitch2_file = '%s/%s.wang.f0_2.csv' % (eval_folder, filename) # pitch2 output of jar
      f0_file = '%s/%s.wang.f0.json' % (eval_folder, filename) # the output we want

      wav2ascii(wav_file, ascii_file) # create ascii file from wav

      call(['java', '-Xmx1024m', '-jar', 'RevMultiPitch.jar', ascii_file, pitch1_file, pitch2_file])
      
      pitch1 = loadtxt(pitch1_file)
      pitch2 = loadtxt(pitch2_file)
      data = {  'f0': common.freq2pitch(pitch1[ : :2]), # take only odd samples (our hop size is the double)
                'f0_2': common.freq2pitch(pitch2[ : :2]),
                'pitch1': pitch1,
                'pitch2': pitch2
              }
      
      common.save_json(data, f0_file)
      # Erase temporary files
      os.remove(ascii_file)
      os.remove(pitch1_file)
      os.remove(pitch2_file)      
      
      i += 1
      bar.update(i*100//(len(filenames)*len(eval_folders)))
    
print "Estimation finished!" 