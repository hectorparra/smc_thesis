package casa;

public class FrontEnd {

  private int highFreqThreshold = 800;

  public double[] cFreq;
  public float[][] r;
  public int timeFrame = 160;
  public int sampFreq;

  private float[] fir_b;
  private float[] a = {
      2.8982e-3f, 8.6946e-3f, 8.6946e-3f, 2.8982e-3f};
  private float[] b = {
      1.0f, -2.3741f, 1.9294f, -0.5321f};
  public float[] filter; // GuoNing's low filter

  public int numChannel, numSample, numFrame;
  public int emptyInteval;

  public FrontEnd(float[][] gammaOut, Gammatone gt) {
    r = gammaOut;
    cFreq = gt.cFreq;
    sampFreq = gt.sampFreq;

    numChannel = r.length;
    numSample = r[0].length;
  }

  public void GuoNingFront() {
    numFrame = numSample / timeFrame - 1;
  }

  public void GuoNingFrontWithLowPass() {
    lowPass();
    numFrame = numSample / timeFrame - 1;
  }

  public void GuoNingFrontWithBandPass() {
    bandPass();
    numFrame = numSample / timeFrame - 1;
  }

  public void GuoNingFrontWithFocus() {
    cfBandPass();
    numFrame = numSample / timeFrame - 1;
  }

  public void MingFront() {
    fir_b = initFir();
    Process();
    numFrame = (numSample - emptyInteval) / timeFrame - 1;
  }

  public void MingFrontWithoutCompentation() {
    fir_b = initFir();
    GetEnvelope();
    numFrame = (numSample - emptyInteval) / timeFrame - 1;
  }

  private void Process() {
    GetEnvelope();
    CompensateDelays();
  }

  private void GetEnvelope() {
    float[] tmp = new float[numSample];
    System.out.print("Computing envelopes...");
    for (int chan = 0; chan < numChannel; chan++) {
      System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
      if (cFreq[chan] > highFreqThreshold) {
        tmp[0] = 0;
        tmp[numSample - 1] = 0;
        for (int t = 1; t < numSample - 1; t++)
          tmp[t] = r[chan][t] * r[chan][t] - r[chan][t - 1] * r[chan][t +
              1];
        Butterworth(tmp, chan);
      }
      r[chan] = FIR_Filter(r[chan]);
      if (Math.round( (float) chan / numChannel * 100) < 10)
        System.out.print("\b\b");
      else
        System.out.print("\b\b\b");
    }
    System.out.println("Done");
  }

  private void Butterworth(float[] x, int chan) {
    float[] yd = new float[x.length];
    int filterMinDelay;

    yd[0] = b[0] * x[0];
    r[chan][0] = yd[0];
    for (int i = 1; i < x.length; i++) {
      yd[i] = 0.0f;
      if (b.length > i + 1)
        filterMinDelay = i + 1;
      else
        filterMinDelay = b.length;
      for (int j = 0; j < filterMinDelay; j++)
        yd[i] += a[j] * x[i - j];
      for (int j = 1; j < filterMinDelay; j++)
        yd[i] -= b[j] * yd[i - j];
      r[chan][i] = yd[i];
    }
  }

  private float[] FIR_Filter(float[] x) {
    // y[n] = b[0]*x(n) + b[1]*x[n-1] + ... + b[nb-1]*x[n-nb+1]
    float tmp;
    float[] y = new float[x.length];
    int filterMinDelay;
    for (int i = 0; i < x.length; i++) {
      tmp = 0;
      if (fir_b.length > i + 1)
        filterMinDelay = i + 1;
      else
        filterMinDelay = fir_b.length;
      for (int j = 0; j < filterMinDelay; j++)
        tmp += fir_b[j] * x[i - j];
      y[i] = tmp;
    }
    return y;
  }

  private void CompensateDelays() {
    emptyInteval = 1200;
    int[] delays = {
        333, 400, 381, 364, 348, 334, 321, 309, 298, 288, 280, 320, 311, 302,
        294, 287, 280, 273, 266, 293, 286, 279, 273, 267, 262, 257, 253, 248,
        244, 239, 235, 231, 247, 243, 240, 236, 233, 229, 226, 222, 219, 217,
        214, 211, 209, 206, 204, 202, 211, 209, 207, 204, 202, 200, 198, 200,
        199, 197, 196, 194, 193, 192, 190, 189, 188, 187, 186, 185, 184, 183,
        181, 180, 179, 178, 178, 177, 176, 175, 174, 173, 172, 171, 171, 170,
        169, 168, 168, 167, 166, 166, 165, 164, 164, 163, 162, 162, 161, 161,
        160, 160, 159, 159, 158, 158, 157, 157, 156, 156, 156, 155, 155, 154,
        154, 154, 153, 153, 153, 152, 152, 152, 151, 151, 151, 150, 150, 150,
        149, 149
    };
    int minAlign = delays[0];
    for (int i = 1; i < delays.length; i++)
      if (delays[i] < minAlign)
        minAlign = delays[i];
    System.out.print("Compensating delays...");
    numSample = numSample + emptyInteval - minAlign;
    float[][] tmp = new float[numChannel][numSample];
    for (int chan = 0; chan < numChannel; chan++) {
      System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
      for (int t = emptyInteval - delays[chan];
           t < numSample + minAlign - delays[chan]; t++) {
        tmp[chan][t] = r[chan][t - emptyInteval + delays[chan]];
      }

      if (Math.round( (float) chan / numChannel * 100) < 10)
        System.out.print("\b\b");
      else
        System.out.print("\b\b\b");
    }
    System.out.println("Done");
    r = tmp;
  }

  private float[] initFir() {
    float[] fir_b = {
        0.153703f, -0.005802f, -0.005670f, -0.005606f, -0.005486f, -0.005440f,
        -0.005335f, -0.005278f, -0.005205f, -0.005180f, -0.005072f, -0.005061f,
        -0.004976f, -0.004975f, -0.004901f, -0.004918f, -0.004829f, -0.004834f,
        -0.004752f, -0.004817f, -0.004668f, -0.004759f, -0.004551f, -0.004697f,
        -0.004104f, -0.004549f, -0.005097f, -0.004651f, -0.004844f, -0.004707f,
        -0.004832f, -0.004756f, -0.004860f, -0.004816f, -0.004895f, -0.004868f,
        -0.004945f, -0.004931f, -0.004997f, -0.004988f, -0.005059f, -0.005042f,
        -0.005094f, -0.005080f, -0.005134f, -0.005107f, -0.005142f, -0.005104f,
        -0.005114f, -0.005037f, -0.005903f, -0.005447f, -0.005510f, -0.005546f,
        -0.005620f, -0.005654f, -0.005705f, -0.005818f, -0.005840f, -0.005853f,
        -0.005943f, -0.005984f, -0.006071f, -0.006094f, -0.006175f, -0.006175f,
        -0.006242f, -0.006260f, -0.006328f, -0.006295f, -0.006400f, -0.006357f,
        -0.006509f, -0.006462f, -0.006945f, -0.006861f, -0.006592f, -0.006868f,
        -0.006817f, -0.006948f, -0.006936f, -0.007043f, -0.007038f, -0.007118f,
        -0.007144f, -0.007218f, -0.007234f, -0.007303f, -0.007324f, -0.007382f,
        -0.007408f, -0.007470f, -0.007499f, -0.007546f, -0.007576f, -0.007631f,
        -0.007656f, -0.007704f, -0.007726f, -0.007782f, -0.007834f, -0.007856f,
        -0.007890f, -0.007929f, -0.007955f, -0.007994f, -0.008041f, -0.008010f,
        -0.008062f, -0.008108f, -0.008124f, -0.008145f, -0.008147f, -0.008174f,
        -0.008157f, -0.008183f, -0.008194f, -0.008216f, -0.008216f, -0.008252f,
        -0.008263f, -0.008312f, -0.008319f, -0.008384f, -0.008249f, -0.008246f,
        -0.008375f, -0.008292f, 0.991649f, -0.008292f, -0.008375f, -0.008246f,
        -0.008249f, -0.008384f, -0.008319f, -0.008312f, -0.008263f, -0.008252f,
        -0.008216f, -0.008216f, -0.008194f, -0.008183f, -0.008157f, -0.008174f,
        -0.008147f, -0.008145f, -0.008124f, -0.008108f, -0.008062f, -0.008010f,
        -0.008041f, -0.007994f, -0.007955f, -0.007929f, -0.007890f, -0.007856f,
        -0.007834f, -0.007782f, -0.007726f, -0.007704f, -0.007656f, -0.007631f,
        -0.007576f, -0.007546f, -0.007499f, -0.007470f, -0.007408f, -0.007382f,
        -0.007324f, -0.007303f, -0.007234f, -0.007218f, -0.007144f, -0.007118f,
        -0.007038f, -0.007043f, -0.006936f, -0.006948f, -0.006817f, -0.006868f,
        -0.006592f, -0.006861f, -0.006945f, -0.006462f, -0.006509f, -0.006357f,
        -0.006400f, -0.006295f, -0.006328f, -0.006260f, -0.006242f, -0.006175f,
        -0.006175f, -0.006094f, -0.006071f, -0.005984f, -0.005943f, -0.005853f,
        -0.005840f, -0.005818f, -0.005705f, -0.005654f, -0.005620f, -0.005546f,
        -0.005510f, -0.005447f, -0.005903f, -0.005037f, -0.005114f, -0.005104f,
        -0.005142f, -0.005107f, -0.005134f, -0.005080f, -0.005094f, -0.005042f,
        -0.005059f, -0.004988f, -0.004997f, -0.004931f, -0.004945f, -0.004868f,
        -0.004895f, -0.004816f, -0.004860f, -0.004756f, -0.004832f, -0.004707f,
        -0.004844f, -0.004651f, -0.005097f, -0.004549f, -0.004104f, -0.004697f,
        -0.004551f, -0.004759f, -0.004668f, -0.004817f, -0.004752f, -0.004834f,
        -0.004829f, -0.004918f, -0.004901f, -0.004975f, -0.004976f, -0.005061f,
        -0.005072f, -0.005180f, -0.005205f, -0.005278f, -0.005335f, -0.005440f,
        -0.005486f, -0.005606f, -0.005670f, -0.005802f, 0.153703f
    };
    return fir_b;
  }

  private void lowPass() {

    int PASSBAND = 1000;
    int STOPBAND = 1200;
    float RIPPLE = 0.01f;

    float[] filter;
    float beta;
    int fLength;
    int tim;

    float a = (float) ( -20 * Math.log(RIPPLE) / Math.log(10));
    if (a <= 21)
      beta = 0;
    else if (a <= 50)
      beta = (float) (0.5842 * Math.pow(a - 21, 0.4) + 0.07889 * (a - 21));
    else
      beta = (float) (0.1102 * (a - 8.7));

    float bw = (STOPBAND - PASSBAND) / (float) sampFreq;
    float len = (float) ( (a - 7.95) / 14.36 / bw);
    fLength = (int) len;
    if ( (len - fLength) < 0.5)
      fLength++;
    else
      fLength += 2;

    if (fLength % 2 != 0)
      fLength++;

    filter = kaiserLowPass(fLength, beta,
                           (STOPBAND + PASSBAND) / (float) sampFreq);

    int shift = fLength / 2;
    float[][] tmp = new float[numChannel][numSample];
    System.out.print("LowPassing for envelopes...");

    for (int chan = 0; chan < numChannel; chan++) {
      System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
      for (int n = 0; n < numSample; n++) {
        tmp[chan][n] = 0;
        for (int m = 0; m <= fLength; m++) {
          tim = n + shift - m;
          if ( (tim >= 0) && (tim < numSample))
            tmp[chan][n] += r[chan][tim] * filter[m];
        }
      }
      if (Math.round( (float) chan / numChannel * 100) < 10)
        System.out.print("\b\b");
      else
        System.out.print("\b\b\b");
    }
    System.out.println("Done");
    r = tmp;
  }

  private void bandPass() {

    int PASSBAND = 50;
    int STOPBAND = 550;
    float RIPPLE = 0.01f;

    float[] filter;
    float beta;
    int fLength;
    int tim;

    float a = (float) ( -20 * Math.log(RIPPLE) / Math.log(10));
    if (a <= 21)
      beta = 0;
    else if (a <= 50)
      beta = (float) (0.5842 * Math.pow(a - 21, 0.4) + 0.07889 * (a - 21));
    else
      beta = (float) (0.1102 * (a - 8.7));

    float bw = 50 / (float) sampFreq;
    float len = (float) ( (a - 7.95) / 14.36 / bw);
    fLength = (int) len;
    if ( (len - fLength) < 0.5)
      fLength++;
    else
      fLength += 2;

    if (fLength % 2 != 0)
      fLength++;

    filter = kaiserBandPass(fLength, beta,
                            (STOPBAND + PASSBAND) / (float) sampFreq,
                            (STOPBAND - PASSBAND) / (float) sampFreq);

    int shift = fLength / 2;
    float[][] tmp = new float[numChannel][numSample];
    System.out.print("BandPassing for envelopes...");

    for (int chan = 0; chan < numChannel; chan++) {
      System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
      for (int n = 0; n < numSample; n++) {
        tmp[chan][n] = 0;
        for (int m = 0; m <= fLength; m++) {
          tim = n + shift - m;
          if ( (tim >= 0) && (tim < numSample))
            tmp[chan][n] += r[chan][tim] * filter[m];
        }
      }
      if (Math.round( (float) chan / numChannel * 100) < 10)
        System.out.print("\b\b");
      else
        System.out.print("\b\b\b");
    }
    System.out.println("Done");
    r = tmp;
  }

  private void cfBandPass() {

    int[][] fRange = {
        {
        67, 95}
        , {
        73, 103}
        , {
        80, 110}
        , {
        87, 118}
        , {
        94, 125}
        , {
        101, 133}
        , {
        109, 142}
        , {
        116, 150}
        , {
        124, 158}
        , {
        132, 167}
        , {
        140, 176}
        , {
        148, 185}
        , {
        157, 194}
        , {
        165, 204}
        , {
        174, 214}
        , {
        183, 224}
        , {
        193, 234}
        , {
        202, 244}
        , {
        212, 255}
        , {
        222, 266}
        , {
        232, 277}
        , {
        242, 288}
        , {
        253, 300}
        , {
        264, 312}
        , {
        275, 324}
        , {
        286, 336}
        , {
        297, 349}
        , {
        309, 362}
        , {
        321, 375}
        , {
        334, 389}
        , {
        346, 403}
        , {
        359, 417}
        , {
        372, 432}
        , {
        386, 447}
        , {
        400, 462}
        , {
        414, 477}
        , {
        428, 493}
        , {
        443, 510}
        , {
        458, 526}
        , {
        474, 543}
        , {
        489, 561}
        , {
        506, 578}
        , {
        522, 596}
        , {
        539, 615}
        , {
        556, 634}
        , {
        574, 653}
        , {
        592, 673}
        , {
        610, 694}
        , {
        629, 714}
        , {
        649, 736}
        , {
        668, 757}
        , {
        689, 780}
        , {
        709, 802}
        , {
        730, 826}
        , {
        752, 849}
        , {
        774, 874}
        , {
        796, 898}
        , {
        820, 924}
        , {
        843, 950}
        , {
        867, 976}
        , {
        892, 1003}
        , {
        917, 1031}
        , {
        943, 1059}
        , {
        969, 1088}
        , {
        996, 1118}
        , {
        1024, 1148}
        , {
        1052, 1179}
        , {
        1081, 1211}
        , {
        1110, 1244}
        , {
        1140, 1277}
        , {
        1171, 1311}
        , {
        1203, 1345}
        , {
        1235, 1381}
        , {
        1268, 1417}
        , {
        1301, 1454}
        , {
        1336, 1492}
        , {
        1371, 1531}
        , {
        1407, 1570}
        , {
        1444, 1611}
        , {
        1482, 1652}
        , {
        1520, 1695}
        , {
        1559, 1738}
        , {
        1600, 1782}
        , {
        1641, 1828}
        , {
        1683, 1874}
        , {
        1726, 1921}
        , {
        1770, 1970}
        , {
        1815, 2019}
        , {
        1861, 2070}
        , {
        1908, 2122}
        , {
        1956, 2175}
        , {
        2005, 2229}
        , {
        2056, 2284}
        , {
        2107, 2341}
        , {
        2160, 2399}
        , {
        2213, 2458}
        , {
        2268, 2518}
        , {
        2325, 2580}
        , {
        2382, 2643}
        , {
        2441, 2708}
        , {
        2501, 2774}
        , {
        2562, 2842}
        , {
        2625, 2911}
        , {
        2690, 2982}
        , {
        2755, 3054}
        , {
        2822, 3128}
        , {
        2891, 3204}
        , {
        2961, 3281}
        , {
        3033, 3360}
        , {
        3107, 3441}
        , {
        3182, 3523}
        , {
        3259, 3608}
        , {
        3337, 3694}
        , {
        3417, 3783}
        , {
        3500, 3873}
        , {
        3583, 3965}
        , {
        3669, 4060}
        , {
        3757, 4157}
        , {
        3847, 4255}
        , {
        3939, 4356}
        , {
        4032, 4460}
        , {
        4128, 4565}
        , {
        4227, 4673}
        , {
        4327, 4784}
        , {
        4429, 4896}
        , {
        4534, 5012}
        , {
        4642, 5130}
        , {
        4751, 5251}
    };

    float RIPPLE = 0.01f;

    float[] filter;
    float beta;
    int fLength;
    int tim;

    float a = (float) ( -20 * Math.log(RIPPLE) / Math.log(10));
    if (a <= 21)
      beta = 0;
    else if (a <= 50)
      beta = (float) (0.5842 * Math.pow(a - 21, 0.4) + 0.07889 * (a - 21));
    else
      beta = (float) (0.1102 * (a - 8.7));

    float bw = 50 / (float) sampFreq;
    float len = (float) ( (a - 7.95) / 14.36 / bw);
    fLength = (int) len;
    if ( (len - fLength) < 0.5)
      fLength++;
    else
      fLength += 2;

    if (fLength % 2 != 0)
      fLength++;

    int shift = fLength / 2;
    float[][] tmp = new float[numChannel][numSample];
    System.out.print("BandPassing for envelopes...");

    for (int chan = 0; chan < numChannel; chan++) {
      System.out.print(Math.round( (float) chan / numChannel * 100) + "%");
      filter = kaiserBandPass(fLength, beta,
                              (fRange[chan][1] + fRange[chan][0]) /
                              (float) sampFreq,
                              (fRange[chan][1] - fRange[chan][0]) /
                              (float) sampFreq);
      /*for (int i = 0; i<fLength; i++){
        System.out.print(filter[i]+",");
      }
      System.out.println();*/

      for (int n = 0; n < numSample; n++) {
        tmp[chan][n] = 0;
        for (int m = 0; m <= fLength; m++) {
          tim = n + shift - m;
          if ( (tim >= 0) && (tim < numSample))
            tmp[chan][n] += r[chan][tim] * filter[m];
        }
      }
      if (Math.round( (float) chan / numChannel * 100) < 10)
        System.out.print("\b\b");
      else
        System.out.print("\b\b\b");
    }
    System.out.println("Done");
    r = tmp;
  }

  private float[] kaiserLowPass(int fLength, float beta, float wn) {

    float[] f = new float[fLength + 1];
    int step;
    float k;

    for (int tim = 0; tim <= fLength; tim++) {
      k = 2 * tim / (float) fLength - 1;
      f[tim] = bessi0( (float) (beta * Math.sqrt(1 - k * k))) / bessi0(beta);
    }

    for (int tim = 0; tim <= fLength; tim++) {
      step = tim - fLength / 2;
      if (step != 0)
        f[tim] *= Math.sin(wn * Math.PI * step) / Math.PI / step;
      else
        f[tim] *= wn;
    }
    return f;
  }

  private float[] kaiserBandPass(int fLength, float beta, float wc, float wn) {

    float[] f = kaiserLowPass(fLength, beta, wn);

    for (int tim = 0; tim <= fLength; tim++)
      f[tim] *= 2 * Math.cos( (tim - fLength / 2) * wc * Math.PI);
    return f;
  }

  private float bessi0(float x) {
    float ax, ans;
    float y;

    ax = Math.abs(x);
    if (ax < 3.75) {
      y = (float) (x / 3.75);
      y *= y;
      ans = (float) (1.0 +
                     y *
                     (3.5156229 +
                      y *
                      (3.0899424 +
                       y *
                       (1.2067492 +
                        y * (0.2659732 + y * (0.360768e-1 + y * 0.45813e-2))))));
    }
    else {
      y = (float) (3.75 / ax);
      ans = (float) ( (Math.exp(ax) / Math.sqrt(ax)) *
                     (0.39894228 +
                      y *
                      (0.1328592e-1 +
                       y *
                       (0.225319e-2 +
                        y *
                        ( -0.157565e-2 +
                         y *
                         (0.916281e-2 +
                          y *
                          ( -0.2057706e-1 +
                           y *
                           (0.2635537e-1 +
                            y * ( -0.1647633e-1 + y * 0.392377e-2)))))))));
    }
    return ans;
  }

}